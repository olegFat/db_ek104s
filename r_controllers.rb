class ResonatorsAnalyzesController < ApplicationController

    before_action :array_for_input_fields,
    only: [ :chart_1_1, :chart_1_2, :chart_1_3, :chart_1_4, :chart_1_5,
            :chart_3_1, :chart_3_2, :chart_3_3, :chart_3_4, :chart_3_5,
            :chart_5_0, :chart_7_0, :chart_9_0, :chart_11_0, :chart_13_0,
            :chart_15_0, :chart_17_0, :chart_19_0, :chart_21_0, :chart_23_0,
            :chart_25_0, :chart_27_0 ]

    before_action :resonators_select,
    only: [ :chart_1_1, :chart_1_2, :chart_1_3, :chart_1_4, :chart_1_5,
            :chart_3_1, :chart_3_2, :chart_3_3, :chart_3_4, :chart_3_5,
            :chart_5_0, :chart_7_0, :chart_9_0, :chart_11_0, :chart_13_0,
            :chart_15_0, :chart_17_0, :chart_19_0, :chart_21_0, :chart_23_0,
            :chart_25_0, :chart_27_0 ]

    def chart_1_1

        @title = 'В начале тренировки и в конце стабилизации оп.70'
        @legend_1 = 'В начале тренировки'
        @legend_2 = 'Оп.70'
        @download_name_1 = 'Распределение_1.1'
        @download_name_2 = 'Относительное распределение_2.1'
        @xtitle = 'Пороговый ток, мА'
        @ytitle = 'Число приборов, шт'


        # Распределение
        in_begin =
        @resonators
        .where.not(ev_threshold_current_at_the_beginning_of_the_workout: nil)
        .map { |e| e.ev_threshold_current_at_the_beginning_of_the_workout.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        in_end =
        @resonators
        .where.not(ev_threshold_current_at_the_end_of_stabilization: nil)
        .map { |e| e.ev_threshold_current_at_the_end_of_stabilization.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_two_charts in_begin, in_end, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_1_2

        @title = 'В конце стабилизации оп.70 и после холодоустойчивости оп.105'
        @legend_1 = 'оп.70'
        @legend_2 = 'оп.105'
        @download_name_1 = 'Распределение_1.2'
        @download_name_2 = 'Относительное распределение_2.2'
        @xtitle = 'Пороговый ток, мА'
        @ytitle = 'Число приборов, шт'

        in_begin =
        @resonators
        .where.not(ev_threshold_current_at_the_end_of_stabilization: nil)
        .map { |e| e.ev_threshold_current_at_the_end_of_stabilization.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        in_end =
        @resonators
        .where.not(cr_threshold_current_after_XY: nil)
        .map { |e| e.cr_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_two_charts in_begin, in_end, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_1_3

        @title = 'После холодоустойчивости оп.105 и после приработки оп.115'
        @legend_1 = 'оп.105'
        @legend_2 = 'оп.115'
        @download_name_1 = 'Распределение_1.3'
        @download_name_2 = 'Относительное распределение_2.3'
        @xtitle = 'Пороговый ток, мА'
        @ytitle = 'Число приборов, шт'

        in_begin =
        @resonators
        .where.not(cr_threshold_current_after_XY: nil)
        .map { |e| e.cr_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        in_end =
        @resonators
        .where.not(ri_threshold_current_after_XY: nil)
        .map { |e| e.ri_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_two_charts in_begin, in_end,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_1_4

        @title = 'После приработки оп.115 и проверочной оп.175'
        @legend_1 = 'оп.115'
        @legend_2 = 'оп.175'
        @download_name_1 = 'Распределение_1.4'
        @download_name_2 = 'Относительное распределение_2.4'
        @xtitle = 'Пороговый ток, мА'
        @ytitle = 'Число приборов, шт'

        in_begin =
        @resonators
        .where.not(ri_threshold_current_after_XY: nil)
        .map { |e| e.ri_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        in_end =
        @resonators
        .where.not(t9_threshold_current: nil)
        .map { |e| e.t9_threshold_current.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_two_charts in_begin, in_end, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_1_5

        @title = 'На всех стадиях'
        @legend_1 = 'В начале тренировки'
        @legend_2 = 'оп.70'
        @legend_3 = 'оп.105'
        @legend_4 = 'оп.115'
        @legend_5 = 'оп.175'

        @download_name_1 = 'Распределение_1.5'
        @download_name_2 = 'Относительное распределение_2.5'
        @xtitle = 'Пороговый ток, мА'
        @ytitle = 'Число приборов, шт'


        # В начале тренировки
        ch_1 = @resonators
        .where.not(ev_threshold_current_at_the_beginning_of_the_workout: nil)
        .map { |e| e.ev_threshold_current_at_the_beginning_of_the_workout.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        # В конце стабилизации оп.70
        ch_2 = @resonators
        .where.not(ev_threshold_current_at_the_end_of_stabilization: nil)
        .map { |e| e.ev_threshold_current_at_the_end_of_stabilization.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        # После холодоустойчивости оп.105
        ch_3 = @resonators
        .where.not(cr_threshold_current_after_XY: nil)
        .map { |e| e.cr_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        # После приработки оп.115
        ch_4 = @resonators
        .where.not(ri_threshold_current_after_XY: nil)
        .map { |e| e.ri_threshold_current_after_XY.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        # После проверочной оп.175
        ch_5 = @resonators
        .where.not(t9_threshold_current: nil)
        .map { |e| e.t9_threshold_current.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_five_charts ch_1, ch_2, ch_3, ch_4, ch_5,
        @resonators.count, params[:x_in], params[:x_out]


    end

    def chart_3_1

        @title = 'Интервалы разностей в начале тренировки и в конце стабилизации оп.70'

        ch_1 = @resonators
        .map { |e| (e.ev_threshold_current_at_the_end_of_stabilization.to_f -
         e.ev_threshold_current_at_the_beginning_of_the_workout.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }
        @chart_distribution =
        hash_for_one_charts ch_1, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_3_2

        @title = 'Интервалы разностей в конце стабилизации оп.70 и после холодоустойчивости оп.105'

        ch_1 = @resonators
        .map { |e| (e.cr_threshold_current_after_XY.to_f -
         e.ev_threshold_current_at_the_end_of_stabilization.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_one_charts ch_1, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_3_3

        @title = 'Интервалы разностей после холодоустойчивости оп.105 и после приработки оп.115'

        ch_1 = @resonators
        .map { |e| (e.ri_threshold_current_after_XY.to_f -
         e.cr_threshold_current_after_XY.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_one_charts ch_1, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_3_4

        @title = 'Интервалы разностей после приработки оп.115 и проверочной оп.175'

        ch_1 = @resonators
        .map { |e| (e.t9_threshold_current.to_f -
         e.ri_threshold_current_after_XY.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_one_charts ch_1, @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_3_5

        @title = 'Интервалы разностей на всех стадиях'
        @legend_1 = 'В начале тренировки и оп.70'
        @legend_2 = 'оп.70 и оп.105'
        @legend_3 = 'оп.105 и оп.115'
        @legend_4 = 'оп.115 и оп.175'

        ch_1 = @resonators
        .map { |e| (e.ev_threshold_current_at_the_end_of_stabilization.to_f -
         e.ev_threshold_current_at_the_beginning_of_the_workout.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_2 = @resonators
        .map { |e| (e.cr_threshold_current_after_XY.to_f -
         e.ev_threshold_current_at_the_end_of_stabilization.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_3 = @resonators
        .map { |e| (e.ri_threshold_current_after_XY.to_f -
         e.cr_threshold_current_after_XY.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_4 = @resonators
        .map { |e| (e.t9_threshold_current.to_f -
         e.ri_threshold_current_after_XY.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_four_charts ch_1, ch_2, ch_3, ch_4,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_5_0
        @title = 'По интервалам потерь после сборки и юстировки оп.065, проверочной оп.075, и проверочной оп.175'
        @legend_1 = 'оп.065'
        @legend_2 = 'оп.075'
        @legend_3 = 'оп.175'

        ch_1 = @resonators
        .where.not(al_TEM00_loss: nil)
        .map { |e| e.al_TEM00_loss.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_2 = @resonators
        .where.not(t3_losses: nil)
        .map { |e| e.t3_losses.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_3 = @resonators
        .where.not(t9_losses: nil)
        .map { |e| e.t9_losses.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_three_charts ch_1, ch_2, ch_3,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_7_0
        @title = 'По интервалам селективности после сборки и юстировки оп.065, проверочной оп.075, и проверочной оп.175'
        @legend_1 = 'оп.065'
        @legend_2 = 'оп.075'
        @legend_3 = 'оп.175'

        ch_1 = @resonators
        .where.not(al_selectivity: nil)
        .map { |e| e.al_selectivity.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        ch_2 = @resonators
        .where.not(t3_selectivity: nil)
        .map { |e| e.t3_selectivity.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        # Это хуйня какая-то
        ch_3 = @resonators
        .where.not(t9_selectivity: nil)
        .map { |e| e.t9_selectivity.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_three_charts ch_1, ch_2, ch_3,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_9_0
        @title = 'По интервалам разности потерь после сборки и юстировки оп.065 и проверочной оп.075'

        ch_1 = @resonators
        .map { |e| (e.t3_losses.to_f -
         e.al_TEM00_loss.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x,y| x<=>y }

        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_11_0
        @title = 'По интервалам разности потерь и разности селективности между проверочными оп.175 и оп.075'
        @legend_1 = 'Разность потерь'
        @legend_2 = 'Разность селективности'

        ch_1 = @resonators
        .map { |e| (e.t9_losses.to_f -
         e.t3_losses.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        ch_2 = @resonators
        .map { |e| (e.t9_selectivity.to_f -
         e.t3_selectivity.to_f).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        @chart_distribution =
        hash_for_two_charts ch_1, ch_2,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_13_0
        @title = 'По интервалам захвата(среднее по модам), измеренным на оп.105, 115, 175'
        @legend_1 = 'Оп.105'
        @legend_2 = 'Оп.115'
        @legend_3 = 'Оп.175'

        ch_1 = @resonators
        .map { |e|((e.cr_capture_in_fashion_1.to_f +
                    e.cr_capture_on_fashion_2.to_f +
                    e.cr_capture_in_fashion_3.to_f +
                    e.cr_capture_on_fashion_4.to_f) / 4 ).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }

        ch_2 = @resonators
        .map { |e|((e.ri_capture_in_fashion_1.to_f +
                    e.ri_capture_on_fashion_2.to_f +
                    e.ri_capture_in_fashion_3.to_f +
                    e.ri_capture_on_fashion_4.to_f) / 4 ).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }

        ch_3 = @resonators
        .where.not(t9_omega: nil)
        .map { |e| e.t9_omega.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }



        @chart_distribution =
        hash_for_three_charts ch_1, ch_2, ch_3,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_15_0
        @title = 'По интервалам разностей захвата(среднее по модам), измеренным на оп.115, 105'

        ch_1 = @resonators
        .map { |e|((e.ri_capture_in_fashion_1.to_f +
                    e.ri_capture_on_fashion_2.to_f +
                    e.ri_capture_in_fashion_3.to_f +
                    e.ri_capture_on_fashion_4.to_f) / 4 -
                    (e.cr_capture_in_fashion_1.to_f +
                    e.cr_capture_on_fashion_2.to_f +
                    e.cr_capture_in_fashion_3.to_f +
                    e.cr_capture_on_fashion_4.to_f) / 4).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}


        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_17_0
        @title = 'По интервалам разностей захвата(среднее по модам), измеренным на оп.175, 115'

        ch_1 = @resonators
        .map { |e|( e.t9_omega.to_f -
                    ( e.cr_capture_in_fashion_1.to_f +
                    e.cr_capture_on_fashion_2.to_f +
                    e.cr_capture_in_fashion_3.to_f +
                    e.cr_capture_on_fashion_4.to_f ) / 4 ).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_19_0
        @title = 'По интервалам захвата(максимальному), измеренным на оп.105, 115, 175'
        @legend_1 = 'Оп.105'
        @legend_2 = 'Оп.115'
        @legend_3 = 'Оп.175'

        ch_1 = @resonators
        .map { |e|[ e.cr_capture_in_fashion_1.to_f,
                    e.cr_capture_on_fashion_2.to_f,
                    e.cr_capture_in_fashion_3.to_f,
                    e.cr_capture_on_fashion_4.to_f ].max.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        ch_2 = @resonators
        .map { |e|[ e.ri_capture_in_fashion_1.to_f,
                    e.ri_capture_on_fashion_2.to_f,
                    e.ri_capture_in_fashion_3.to_f,
                    e.ri_capture_on_fashion_4.to_f ].max.round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        ch_3 = @resonators
        .where.not(t9_omega: nil)
        .group(:t9_omega)
        .count


        @chart_distribution =
        hash_for_three_charts ch_1, ch_2, ch_3,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_21_0
        @title = 'По интервалам разностей захвата(максимальному), измеренным на оп.115, 105'

        ch_1 = @resonators
        .map { |e|([e.ri_capture_in_fashion_1.to_f,
                    e.ri_capture_on_fashion_2.to_f,
                    e.ri_capture_in_fashion_3.to_f,
                    e.ri_capture_on_fashion_4.to_f].max -
                    [e.cr_capture_in_fashion_1.to_f,
                    e.cr_capture_on_fashion_2.to_f,
                    e.cr_capture_in_fashion_3.to_f,
                    e.cr_capture_on_fashion_4.to_f].max).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_23_0
        @title = 'По интервалам разностей захвата(максимальное по модам), измеренным на оп.175, 115'

        ch_1 = @resonators
        .map { |e|( e.t9_omega.to_f -
                    [e.cr_capture_in_fashion_1.to_f,
                    e.cr_capture_on_fashion_2.to_f,
                    e.cr_capture_in_fashion_3.to_f,
                    e.cr_capture_on_fashion_4.to_f].max).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]
    end

    def chart_25_0
        @title = 'По интервалам КНУ (захв.), замеренным на оп.105 и 115'
        @legend_1 = 'оп.105'
        @legend_2 = 'оп.115'

        ch_1 = @resonators
        .map { |e|((e.cr_output_frequency_in_mode_1.to_f +
                    e.cr_output_frequency_in_mode_2.to_f +
                    e.cr_output_frequency_in_mode_3.to_f +
                    e.cr_output_frequency_in_mode_4.to_f) / 4 ).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        ch_2 = @resonators
        .map { |e|((e.ri_output_frequency_in_mode_1.to_f +
                    e.ri_output_frequency_in_mode_1.to_f +
                    e.ri_output_frequency_in_mode_1.to_f +
                    e.ri_output_frequency_in_mode_1.to_f) / 4 ).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}

        @chart_distribution =
        hash_for_two_charts ch_1, ch_2,
        @resonators.count, params[:x_in], params[:x_out]

    end

    def chart_27_0

        @title = 'По интервалам разностей КНУ (захв.), замеренным на оп.115 и 105'

        ch_1 = @resonators
        .map { |e|((e.ri_output_frequency_in_mode_1.to_f +
                    e.ri_output_frequency_in_mode_2.to_f +
                    e.ri_output_frequency_in_mode_3.to_f +
                    e.ri_output_frequency_in_mode_4.to_f) / 4 -
                    (e.cr_output_frequency_in_mode_1.to_f +
                    e.cr_output_frequency_in_mode_2.to_f +
                    e.cr_output_frequency_in_mode_3.to_f +
                    e.cr_output_frequency_in_mode_4.to_f) / 4).round 1 }
        .group_by { |e| e }.map { |key, val| [key, val.count] }
        .sort{ |x, y| x <=> y}


        @chart_distribution =
        hash_for_one_charts ch_1,
        @resonators.count, params[:x_in], params[:x_out]

    end



    private
    def array_for_input_fields

        # Список резонаторов
        @resonators_array = Resonator.all
        .map { |e|  e.number }

        # Список изготовителей(поставщиков) катодов
        @manufacturer_array = Cathode.all
        .where.not(manufacturer_supplier: '')
        .select(:manufacturer_supplier).distinct
        .map { |e|  e.manufacturer_supplier }

        # Список режимов окисления катодов
        @oxidation_array = Cathode.all
        .where.not(oxidation_mode: '')
        .select(:oxidation_mode).distinct
        .map { |e|  e.oxidation_mode }

        # Список катодов
        @cathodes_array = Cathode.all
        .map { |e|  e.number}

        # Список ног
        @legs_array = Leg.all
        .map { |e|  e.disk_number}

        # Список зеркал, Только годные
        @mirrors_array = Mirror.all
        .where("result_of_polishing = ?
            OR result_of_polishing = ?
            OR appearance = ?",
            nil, "Годен", "Годен")
        .map { |e|  e.number_of_substrate}

    end


    def resonators_select

        # Выборка резонаторов для анализа
        @resonators = resonators_collect( Resonator.all, params[:number],
            params[:date_in], params[:date_out], params[:manufacturer],
            params[:oxidation], params[:cathodes],
            params[:legs], params[:mirrors])

    end

    def hash_for_one_charts c_1, resonator_count, x_in, x_out

        # Выбираем значения ключей по X
        ox = x_interval x_in, x_out
        c_1 = c_1
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        # Относительное распределение
        c_1_relative = c_1
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        hash_collect = [c_1, c_1_relative, resonator_count]

        return hash_collect

    end


    def hash_for_two_charts c_1, c_2, resonator_count, x_in, x_out


        # Выбираем значения ключей по X
        ox = x_interval x_in, x_out
        c_1 = c_1
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_2 = c_2
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        # Готовый хеш для графика распределения
        chart_distribution =
        adding_zero_values_to_the_chart c_1, c_2


        # Относительное распределение
        c_1 = c_1
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        c_2 = c_2
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        # Готовый хеш для графика относительного распределения
        chart_distribution_relative =
        adding_zero_values_to_the_chart c_1, c_2

        hash_collect =
        chart_distribution +
        chart_distribution_relative +
        [ resonator_count ]

        return hash_collect

    end

    def hash_for_three_charts c_1, c_2, c_3, resonator_count, x_in, x_out

        temp = adding_zero_values_to_the_chart c_3, c_2
        c_3 = temp.first
        c_2 = temp.last

        temp = adding_zero_values_to_the_chart c_2, c_1
        c_1 = temp.last

        # Выбираем значения ключей по X
        ox = x_interval x_in, x_out
        c_1 = c_1
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_2 = c_2
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_3 = c_3
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }


        # Распределение. Готовый хеш
        chart_distribution = [ c_1, c_2, c_3 ]

        # Относительное распределение
        ch_1 = c_1
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_2 = c_2
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_3 = c_3
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        hash_collect = chart_distribution +
        [ ch_1, ch_2, ch_3, resonator_count ]

        return hash_collect

    end

    def hash_for_four_charts c_1, c_2, c_3, c_4, resonator_count, x_in, x_out

        # Выбираем значения ключей по X
        ox = x_interval x_in, x_out
        c_1 = c_1
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_2 = c_2
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_3 = c_3
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_4 = c_4
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        temp = adding_zero_values_to_the_chart c_4, c_3
        c_4 = temp.first
        c_3 = temp.last

        temp = adding_zero_values_to_the_chart c_3, c_2
        c_2 = temp.last

        temp = adding_zero_values_to_the_chart c_2, c_1
        c_1 = temp.last

        # Распределение. Готовый хеш
        chart_distribution = [ c_1, c_2, c_3, c_4 ]


        # Относительное распределение
        ch_1 = c_1
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_2 = c_2
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_3 = c_3
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_4 = c_4
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        hash_collect = chart_distribution +
        [ ch_1, ch_2, ch_3, ch_4, resonator_count ]

        return hash_collect

    end

    def hash_for_five_charts c_1, c_2, c_3, c_4, c_5, resonator_count, x_in, x_out

        # Выбираем значения ключей по X
        ox = x_interval x_in, x_out
        c_1 = c_1
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_2 = c_2
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_3 = c_3
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_4 = c_4
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        c_5 = c_5
        .select{ |key, val| key >= ox.first.to_f && key <= ox.last.to_f }

        temp = adding_zero_values_to_the_chart c_5, c_4
        c_5 = temp.first
        c_4 = temp.last

        temp = adding_zero_values_to_the_chart c_4, c_3
        c_3 = temp.last

        temp = adding_zero_values_to_the_chart c_3, c_2
        c_2 = temp.last

        temp = adding_zero_values_to_the_chart c_2, c_1
        c_1 = temp.last

        chart_distribution = [ c_1, c_2, c_3, c_4, c_5 ]


        # Относительное распределение
        ch_1 = c_1
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_2 = c_2
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_3 = c_3
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_4 = c_4
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        ch_5 = c_5
        .collect{ |key, val| [key, val * 100 / resonator_count ]  }

        chart_distribution = chart_distribution +
        [ ch_1, ch_2, ch_3, ch_4, ch_5, resonator_count ]

        return chart_distribution


    end

    # Вернет два хеша с добавленными значениями друга, установив их в 0
    # Используется на графиках, чтобы не было разрывов
    def adding_zero_values_to_the_chart first_h, second_h

        first_key_array =
        first_h.collect{ |key, val|  key }

        second_key_array =
        second_h.collect{ |key, val|  key }

        first_zero_array = (first_key_array - second_key_array)
        .collect{ |item| [item, 0] }

        second_zero_array = (second_key_array - first_key_array)
        .collect{ |item| [item, 0] }

        first = (first_h.to_a + second_zero_array).sort{ |x,y| x <=> y }
        second = (second_h.to_a + first_zero_array).sort{ |x,y| x <=> y }

        return [ first.to_h, second.to_h ]

    end

    # Возвращает период с date_in до date_out
    def date_conversion_in_the_period date_in, date_out

        if date_in.length > 0 and date_out.length > 0
            year = date_in[0..3].to_i
            month = date_in[5..6].to_i
            date_in = Date.civil(year, month, 1)
            year = date_out[0..3].to_i
            month = date_out[5..6].to_i
            date_out = Date.civil(year, month, 1)

            return date_in..date_out
        else
            return Date.current - 10.year..Date.current

        end

    end

    # Возвращает список номеров резонаторов
    def resonators_in_the_list resonators

        if resonators
            return Resonator.where(number: resonators).select(:number)
        else
            return Resonator.all.select(:number)
        end

    end

    # Возвращает список резонаторов, по производителю катода
    def cathodes_to_the_list_by_manufacturer cathode_manufacturer

        if cathode_manufacturer
            return Cathode.where(manufacturer_supplier: cathode_manufacturer).ids
        else
            return Cathode.all.ids
        end

    end

    # Возвращает список резонаторов, по режиму окисления
    def list_of_cathodes_by_oxidation_regime oxidation

        if oxidation
            return Cathode.where(oxidation_mode: oxidation).ids
        else
            return Cathode.all.ids
        end

    end

    # Возвращает список резонаторов, по установленным катодам
    def cathodes_in_the_list cathodes

        if cathodes
            return Cathode.where(number: cathodes).ids
        else
            return Cathode.all.ids
        end

    end

    # Возвращает список резонаторов, по установленным ногам
    def legs_in_the_list legs

        if legs
            return Leg.where(disk_number: legs).ids
        else
            return Leg.all.ids
        end

    end

    # Возвращает список резонаторов, по установленным зеркалам
    def mirrors_in_the_list mirrors

        if mirrors
            return Mirror.where(number_of_substrate: mirrors).ids
        else
            return Mirror.all.ids
        end

    end

    # Возвращает интервал значений по оси Х
    def x_interval x_in, x_out

        if x_in.length > 0 && x_out.length > 0
            return [ x_in, x_out ]
        else
            return [ -9999999, 9999999 ]
        end

    end

    # Вернет список резонаторов, по всем условиям запросов
    def resonators_collect( model_list,
                            resonators = 0,
                            date_in = 0,
                            date_out = 0,
                            manufacturer_ids = 0,
                            oxidation_ids = 0,
                            cathodes = 0,
                            legs = 0,
                            mirrors = 0 )

        if model_list.exists?

            date_interval =
            date_conversion_in_the_period date_in, date_out

            resonators_list =
            resonators_in_the_list resonators

            manufacturer_list_ids =
            cathodes_to_the_list_by_manufacturer manufacturer_ids

            oxidation_list_ids =
            list_of_cathodes_by_oxidation_regime oxidation_ids

            cathode_list =
            cathodes_in_the_list cathodes

            legs_list =
            legs_in_the_list legs

            mirrors_list =
            mirrors_in_the_list mirrors

            return Resonator.all
            .where(al_date: date_interval)
            .where(number: resonators_list)
            .where(cathode_id: manufacturer_list_ids)
            .where(cathode_id: oxidation_list_ids)
            .where(cathode_id: cathode_list)
            .where(cathode_id: legs_list)
            .where(cathode_id: mirrors_list)

        end
    end
end


# Возвращает хеш, в заданном по x интервале
def axis_h(chart, with, to)
  if chart
    axis_i = with && to ? [with, to] : [-999999, 999999]
    return chart.select{ |key, val| key >= axis_i.first.to_f && key <= axis_i.last.to_f }
  else return nil
  end
end

# Вернет два хеша с добавленными значениями друга, установив их в 0
# Используется на графиках, чтобы не было разрывов
def add_zero chart1, chart2
  chart1_key = chart1.select{ |key, val| key }
  chart2_key = chart2.select{ |key, val| key }
  chart1_add = (chart2_key - chart1_key).collect{ |item| [item, 0] }
  chart2_add = (chart1_key - chart2_key).collect{ |item| [item, 0] }
  return [
    (chart1.to_a + chart1_add).sort{ |x,y| x<=>y },
    (chart2.to_a + chart2_add).sort{ |x,y| x<=>y }
  ]
end

def hash_charts(from, to, count, chart1, chart2 = nil, chart3 = nil, chart4 = nil, chart5 = nil)

  chart1 = axis_h(chart1, from, to)
  chart2 = axis_h(chart2, from, to)
  chart3 = axis_h(chart3, from, to)
  chart4 = axis_h(chart4, from, to)
  chart5 = axis_h(chart5, from, to)

  temp = add_zero(chart1, chart2)
  chart1 = temp.first
  chart2 = temp.last

  temp = add_zero(chart2, chart3)
  chart2 = temp.first
  chart3 = temp.last

  temp = add_zero(chart3, chart4)
  chart3 = temp.first
  chart4 = temp.last

  temp = add_zero(chart4, chart5)
  chart4 = temp.first
  chart5 = temp.last

  return [ chart1, chart2, chart3, chart4, chart5 ].compact

end
