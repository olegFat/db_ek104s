i = 0
Sensor.all.each do |sensor|
    count = 0

    if sensor.align_prisms_and_fpu == 'Завершена'
        count = count + 1
    end
    if sensor.checking_the_build_quality_in_screens == 'Завершена'
        count = count + 1
    end
    if sensor.check_for_resistance_to_sinusoidal_vibration == 'Завершена'
        count = count + 1
    end
    if sensor.check_by_pm == 'Завершена'
        count = count + 1
    end
    if sensor.psi == 'Завершена'
        count = count + 1
    end
    sensor.update end_count: count

    if count < 5
        sensor.update suitable: false
        i = i.next
    else
        sensor.update suitable: true
    end

    puts "#{count}"

end
puts "#{i}"
