class CreateHistories < ActiveRecord::Migration[5.0]
  def change
  	drop_table :histories
    create_table :histories do |t|
    	t.string :dev
    	t.integer :dev_id
    	t.string :operation
    	t.string :operator
      t.timestamps
    end
  end
end
