class AddRemarkResoSens < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.string	:remark
  	end

  	change_table :bad_resonators do |t|
  		t.string	:remark
  	end

  	change_table :sensors do |t|
  		t.string	:remark
  	end
  end
end
