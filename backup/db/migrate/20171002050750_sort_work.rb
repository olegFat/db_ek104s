class SortWork < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.integer	:end_count
  	end

  	change_table :bad_resonators do |t|
  		t.integer	:end_count
  	end
  end
end
