class ResonatorSuitable < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.boolean	:suitable
  	end
  end
end
