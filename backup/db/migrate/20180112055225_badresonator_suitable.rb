class BadresonatorSuitable < ActiveRecord::Migration[5.0]
  def change
    change_table :bad_resonators do |t|
	t.boolean :suitable
    end
  end
end
