class RemoveFromResonator < ActiveRecord::Migration[5.0]
  def change
    change_table :sensors do |t|
        t.remove :сoil_winding
        t.remove :cw_date_of_coil_winding
        t.remove :cw_date_of_pouring_compound
        t.remove :cw_date_of_installation_of_piezoelectric_blocks
        t.remove :cw_the_result_of_winding
        t.remove :cw_return_date
        t.remove :cw_reason_for_return
        t.remove :cw_for_the_return_list
        t.remove :cw_refund_description
    end
  end
end
