class ChangeNovember < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.float :se_threshold_voltage_1GRP
  		t.float :se_threshold_voltage_2GRP
  	end

  	change_table :bad_resonators do |t|
  		t.float :se_threshold_voltage_1GRP
  		t.float :se_threshold_voltage_2GRP
  	end

    change_table :legs do |t|
      t.string :xray_file
    end

  end
end
