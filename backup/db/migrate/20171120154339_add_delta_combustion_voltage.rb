class AddDeltaCombustionVoltage < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.float :cr_combustion_voltage_delta
  		t.float :cr_capture_in_fashion_delta
  		t.float :cr_output_frequency_delta
  		t.float :ri_capture_in_fashion_delta
  		t.float :ri_output_frequency_delta
  	end

  	change_table :bad_resonators do |t|
  		t.float :cr_combustion_voltage_delta
  		t.float :cr_capture_in_fashion_delta
  		t.float :cr_output_frequency_delta
  		t.float :ri_capture_in_fashion_delta
  		t.float :ri_output_frequency_delta
  	end
  end
end