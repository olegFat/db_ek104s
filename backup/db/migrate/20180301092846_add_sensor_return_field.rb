class AddSensorReturnField < ActiveRecord::Migration[5.0]
  def change
    change_table :sensors do |t|

      # По служебной . Раздел проверка по ПМ
      t.string   :check_to_pm
      t.string   :cp_date
      t.string   :cp_result
      t.date     :cp_return_date
      t.string   :cp_reason_for_return
      t.string   :cp_for_the_return_list
      t.string   :cp_refund_description

      t.boolean  :suitable
      t.integer  :end_count

      t.rename   :ap_the_result_of_the_alignment_of_the_prism_and_the_fpu, :ap_result
      t.rename   :cb_the_result_of_checking_the_build_quality_in_screens, :cb_result
      t.rename   :cf_the_result_of_the_vibration_test, :cf_result
      t.rename   :ps_the_result_of_checking_the_build_quality_in_screens, :ps_result

      t.rename   :ap_date_of_alignment_of_the_prism_and_fpu, :ap_date
      t.rename   :cb_date_of_the_assembly_quality_assurance_in_the_screens, :cb_date
      t.rename   :cf_date_of_testing_for_resistance_to_sinusoidal_vibration, :cf_date
      t.rename   :ps_date_of_psi, :ps_date

      t.rename   :check_to_pm, :check_by_pm

      t.remove   :assembly_and_wiring
      t.remove   :aw_date_of_assembly_and_electromagnet

    end

  end
end
