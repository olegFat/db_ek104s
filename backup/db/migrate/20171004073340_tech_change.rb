class TechChange < ActiveRecord::Migration[5.0]
  def change
  	change_table :resonators do |t|
  		t.float	:cr_nitrogen_level
  		t.float	:cr_nitrogen_level_rate
  	end

  	change_table :bad_resonators do |t|
  		t.float	:cr_nitrogen_level
  		t.float	:cr_nitrogen_level_rate
  	end
  end
end
