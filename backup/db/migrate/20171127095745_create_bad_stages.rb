class CreateBadStages < ActiveRecord::Migration[5.0]
  def change
    create_table :bad_stages do |t|
      t.string 		:number
      t.string 		:bad_stage
      t.string 		:ev_vacuum_post_number
      t.datetime 	:created_at
    end
  end
end
