class CreateLossOfResonators < ActiveRecord::Migration[5.0]
  def change
    create_table :loss_of_resonators do |t|
		t.string :number
		t.float  :diffractions_before
		t.float  :difference_in_losses
		t.string :ev_vacuum_post_number
		t.date   :created_at
    end
  end
end
