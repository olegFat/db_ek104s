class CreateBadSensors < ActiveRecord::Migration[5.0]
  def change

    drop_table :bad_sensors

    create_table :bad_sensors do |t|
        t.string   :number
        t.integer  :resonator_id
        t.date     :date_of_manufacture_of_the_resonator
        t.string   :remark

        t.string   :align_prisms_and_fpu
        t.date     :ap_date
        t.float    :ap_the_combustion_voltage_in_2_sec
        t.float    :ap_the_combustion_voltage_after_35_sec
        t.float    :ap_the_combustion_voltage_after_10_min
        t.float    :ap_the_transmission_coefficient_pb1
        t.float    :ap_the_transmission_coefficient_pb2
        t.string   :ap_result
        t.date     :ap_return_date
        t.string   :ap_reason_for_return
        t.string   :ap_for_the_return_list
        t.string   :ap_refund_description

        t.string   :checking_the_build_quality_in_screens
        t.date     :cb_date
        t.float    :cb_the_combustion_voltage_in_2_sec
        t.float    :cb_the_combustion_voltage_after_35_sec
        t.float    :cb_the_combustion_voltage_after_10_min
        t.float    :cb_the_amplitude_of_the_output_signal_1_in_the_mode_plus
        t.float    :cb_the_amplitude_of_the_output_signal_2_in_the_mode_plus
        t.float    :cb_the_amplitude_of_the_output_signal_1_in_the_mode_minus
        t.float    :cb_the_amplitude_of_the_output_signal_2_in_the_mode_minus
        t.float    :cb_the_phase_difference_of_the_output_signals
        t.float    :cb_the_transmission_coefficient_of_the_modes_in_the_mode_plus
        t.float    :cb_the_transmission_coefficient_of_the_modes_in_the_mode_minus
        t.float    :cb_the_transmission_coefficient
        t.float    :cb_the_amplitude_of_the_perimeter_signal_of_the_mode_plus
        t.float    :cb_the_amplitude_of_the_perimeter_signal_of_the_mode_minus
        t.string   :cb_result
        t.date     :cb_return_date
        t.string   :cb_reason_for_return
        t.string   :cb_for_the_return_list
        t.string   :cb_refund_description

        t.string   :check_for_resistance_to_sinusoidal_vibration
        t.date     :cf_date
        t.string   :cf_result
        t.date     :cf_return_date
        t.string   :cf_reason_for_return
        t.string   :cf_for_the_return_list
        t.string   :cf_refund_description

        t.string   :check_by_pm
        t.string   :cp_date
        t.string   :cp_result
        t.date     :cp_return_date
        t.string   :cp_reason_for_return
        t.string   :cp_for_the_return_list
        t.string   :cp_refund_description

        t.string   :psi
        t.date     :ps_date
        t.float    :ps_the_amplitude_of_the_output_signal_1_in_the_working_mode
        t.float    :ps_the_amplitude_of_the_output_signal_2_in_the_operating_mode
        t.float    :ps_the_transmission_coefficient_of_the_ou_in_the_working_mode
        t.float    :ps_combustion_voltage
        t.string   :ps_result
        t.date     :ps_return_date
        t.string   :ps_reason_for_return
        t.string   :ps_for_the_return_list
        t.string   :ps_refund_description

        t.integer  :end_count
        t.boolean  :suitable

        t.timestamps
    end
  end
end
