class AddReturnFieldTest < ActiveRecord::Migration[5.0]
  def change
    drop_table :list_element_cols
    drop_table :list_elements
    drop_table :parameter_list_cols
    drop_table :resonator_cols
    drop_table :returns_list_cols

    change_table :resonators do |t|

      # Убираю все лишнее
      t.remove   :varnish_coating
      t.remove   :vc_date

      t.remove   :metallization_of_mirrors
      t.remove   :mm_date
      t.remove   :tinning_mirrors
      t.remove   :tm_date

      t.remove   :tm_verification_operation_after_ts
      t.remove   :tm_optical_contact_monitoring
      t.remove   :tm_description_of_optical_contact
      t.remove   :tm_burning_voltage_after_2_sec
      t.remove   :tm_burning_voltage_after_35_sec
      t.remove   :tm_burning_voltage_after_10_min
      t.remove   :tm_threshold_current_after_XY
      t.remove   :tm_result
      t.remove   :tm_return_date
      t.remove   :tm_reason_for_return
      t.remove   :tm_for_the_return_list
      t.remove   :tm_refund_description

      t.remove  :cn_fit

      t.remove  :test_9
      t.remove  :t9_appearance
      t.remove  :t9_optical_contact
      t.remove  :t9_im_date
      t.remove  :t9_im_fit

      # по служебной СКТО от 21.12.2017 № 8/4-353
      t.remove    :al_optical_contact_descripttion
      t.remove    :ev_optical_contact_monitoring
      t.remove    :ev_description_of_optical_contact
      t.remove    :t3_appearance
      t.remove    :t3_optical_contact
      t.remove    :ts_description_of_optical_contact
      t.remove    :se_burning_voltage_after_2_sec
      t.remove    :se_burning_voltage_after_35_sec
      t.remove    :se_burning_voltage_after_10_min
      t.remove    :se_optical_contact
      t.remove    :ht_description_of_optical_contact

      # по просьбе Банникова
      t.float     :al_leg_clamp



      t.string  :t3_result
      t.date    :t3_return_date
      t.string  :t3_reason_for_return
      t.string  :t3_for_the_return_list
      t.string  :t3_refund_description

      t.string  :t9_result
      t.date    :t9_return_date
      t.string  :t9_reason_for_return
      t.string  :t9_for_the_return_list
      t.string  :t9_refund_description


      t.rename :date_control, :cn_date
      t.rename :test_10, :test_9
      t.rename :t10_appearance, :t9_appearance
      t.rename :t10_optical_contact, :t9_optical_contact
      t.rename :t10_im_date, :t9_im_date
      t.rename :t10_im_fit, :t9_im_fit
      t.rename :t10_combustion_voltage_2s, :t9_combustion_voltage_2s
      t.rename :t10_combustion_voltage_35s, :t9_combustion_voltage_35s
      t.rename :t10_combustion_voltage_10m, :t9_combustion_voltage_10m
      t.rename :t10_combustion_voltage_delta, :t9_combustion_voltage_delta
      t.rename :t10_threshold_current, :t9_threshold_current
      t.rename :t10_omega, :t9_omega
      t.rename :t10_nitrogen_level, :t9_nitrogen_level
      t.rename :t10_nitrogen_level_rate, :t9_nitrogen_level_rate

      t.rename :al_alignment_result, :al_result
      t.rename :ev_the_result_of_the_final_TVE, :ev_result
      t.rename :se_the_result_of_clamping, :se_result

      t.string  :cn_result
      t.date    :cn_return_date
      t.string  :cn_reason_for_return
      t.string  :cn_for_the_return_list
      t.string  :cn_refund_description

    end

    change_table :bad_resonators do |t|

              # Убираю все лишнее
      t.remove   :varnish_coating
      t.remove   :vc_date

      t.remove   :metallization_of_mirrors
      t.remove   :mm_date
      t.remove   :tinning_mirrors
      t.remove   :tm_date

      t.remove   :tm_verification_operation_after_ts
      t.remove   :tm_optical_contact_monitoring
      t.remove   :tm_description_of_optical_contact
      t.remove   :tm_burning_voltage_after_2_sec
      t.remove   :tm_burning_voltage_after_35_sec
      t.remove   :tm_burning_voltage_after_10_min
      t.remove   :tm_threshold_current_after_XY
      t.remove   :tm_result
      t.remove   :tm_return_date
      t.remove   :tm_reason_for_return
      t.remove   :tm_for_the_return_list
      t.remove   :tm_refund_description

      t.remove  :cn_fit

      t.remove  :test_9
      t.remove  :t9_appearance
      t.remove  :t9_optical_contact
      t.remove  :t9_im_date
      t.remove  :t9_im_fit

      # по служебной СКТО от 21.12.2017 № 8/4-353
      t.remove    :al_optical_contact_descripttion
      t.remove    :ev_optical_contact_monitoring
      t.remove    :ev_description_of_optical_contact
      t.remove    :t3_appearance
      t.remove    :t3_optical_contact
      t.remove    :ts_description_of_optical_contact
      t.remove    :se_burning_voltage_after_2_sec
      t.remove    :se_burning_voltage_after_35_sec
      t.remove    :se_burning_voltage_after_10_min
      t.remove    :se_optical_contact
      t.remove    :ht_description_of_optical_contact

      # по просьбе Банникова
      t.float     :al_leg_clamp



      t.string  :t3_result
      t.date    :t3_return_date
      t.string  :t3_reason_for_return
      t.string  :t3_for_the_return_list
      t.string  :t3_refund_description

      t.string  :t9_result
      t.date    :t9_return_date
      t.string  :t9_reason_for_return
      t.string  :t9_for_the_return_list
      t.string  :t9_refund_description


      t.rename :date_control, :cn_date
      t.rename :test_10, :test_9
      t.rename :t10_appearance, :t9_appearance
      t.rename :t10_optical_contact, :t9_optical_contact
      t.rename :t10_im_date, :t9_im_date
      t.rename :t10_im_fit, :t9_im_fit
      t.rename :t10_combustion_voltage_2s, :t9_combustion_voltage_2s
      t.rename :t10_combustion_voltage_35s, :t9_combustion_voltage_35s
      t.rename :t10_combustion_voltage_10m, :t9_combustion_voltage_10m
      t.rename :t10_combustion_voltage_delta, :t9_combustion_voltage_delta
      t.rename :t10_threshold_current, :t9_threshold_current
      t.rename :t10_omega, :t9_omega
      t.rename :t10_nitrogen_level, :t9_nitrogen_level
      t.rename :t10_nitrogen_level_rate, :t9_nitrogen_level_rate

      t.rename :al_alignment_result, :al_result
      t.rename :ev_the_result_of_the_final_TVE, :ev_result
      t.rename :se_the_result_of_clamping, :se_result

      t.string  :cn_result
      t.date    :cn_return_date
      t.string  :cn_reason_for_return
      t.string  :cn_for_the_return_list
      t.string  :cn_refund_description
    end
  end
end
