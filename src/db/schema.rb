# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180329130814) do

  create_table "bad_resonators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.string   "alignment"
    t.date     "al_date"
    t.date     "al_cathode_date_of_manufacture"
    t.date     "al_leg_date_of_manufacture"
    t.date     "al_fmirror_date_of_manufacture"
    t.date     "al_piezo_mirrors_1_date_of_manufacture"
    t.date     "al_piezo_mirrors_2_date_of_manufacture"
    t.date     "al_smirror_date_of_manufacture"
    t.float    "al_TEM00_loss",                                               limit: 24
    t.float    "al_TEM01_loss",                                               limit: 24
    t.string   "al_result"
    t.date     "al_return_date"
    t.string   "al_reason_for_return"
    t.string   "al_refund_description"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "al_for_the_return_list"
    t.string   "evo"
    t.date     "ev_date"
    t.string   "ev_vacuum_post_number"
    t.float    "ev_threshold_current_at_the_beginning_of_the_workout",        limit: 24
    t.float    "ev_burning_stress_at_the_beginning_of_the_workout",           limit: 24
    t.float    "ev_burning_voltage_after_5_hours_of_training",                limit: 24
    t.float    "ev_combustion_voltage_after_refilling",                       limit: 24
    t.float    "ev_the_burning_voltage_after_10_hours_of_training",           limit: 24
    t.float    "ev_threshold_current_at_the_end_of_the_workout",              limit: 24
    t.float    "ev_the_combustion_voltage_at_the_beginning_of_stabilization", limit: 24
    t.float    "ev_the_combustion_voltage_after_1_hour_of_stabilization",     limit: 24
    t.float    "ev_the_combustion_voltage_after_2_hours_of_stabilization",    limit: 24
    t.float    "ev_the_combustion_voltage_after_3_hours_of_stabilization",    limit: 24
    t.float    "ev_threshold_current_at_the_end_of_stabilization",            limit: 24
    t.string   "ev_result"
    t.date     "ev_return_date"
    t.string   "ev_reason_for_return"
    t.string   "ev_for_the_return_list"
    t.string   "ev_refund_description"
    t.string   "thermostabilization"
    t.date     "ts_date"
    t.date     "ts_verification_operation_after_ts"
    t.string   "ts_optical_contact_monitoring"
    t.float    "ts_measurement_of_combustion_voltage",                        limit: 24
    t.date     "ts_date_of_preheating_of_getter"
    t.string   "ts_result"
    t.date     "ts_return_date"
    t.string   "ts_reason_for_return"
    t.string   "ts_for_the_return_list"
    t.string   "ts_refund_description"
    t.string   "cold_resistance"
    t.date     "cr_date"
    t.date     "cr_verification_operation_after_ts"
    t.string   "cr_optical_contact_monitoring"
    t.string   "cr_description_of_optical_contact"
    t.float    "cr_burning_voltage_after_2_sec",                              limit: 24
    t.float    "cr_burning_voltage_after_35_sec",                             limit: 24
    t.float    "cr_burning_voltage_after_10_min",                             limit: 24
    t.float    "cr_threshold_current_after_XY",                               limit: 24
    t.float    "cr_output_frequency_in_mode_1",                               limit: 24
    t.float    "cr_capture_in_fashion_1",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_2",                               limit: 24
    t.float    "cr_capture_on_fashion_2",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_3",                               limit: 24
    t.float    "cr_capture_in_fashion_3",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_4",                               limit: 24
    t.float    "cr_capture_on_fashion_4",                                     limit: 24
    t.string   "cr_result"
    t.date     "cr_return_date"
    t.string   "cr_reason_for_return"
    t.string   "cr_for_the_return_list"
    t.string   "cr_refund_description"
    t.string   "running_in"
    t.date     "ri_date"
    t.date     "ri_verification_operation_after_ts"
    t.string   "ri_optical_contact_monitoring"
    t.string   "ri_description_of_optical_contact"
    t.float    "ri_threshold_current_after_XY",                               limit: 24
    t.float    "ri_output_frequency_in_mode_1",                               limit: 24
    t.float    "ri_capture_in_fashion_1",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_2",                               limit: 24
    t.float    "ri_capture_on_fashion_2",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_3",                               limit: 24
    t.float    "ri_capture_in_fashion_3",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_4",                               limit: 24
    t.float    "ri_capture_on_fashion_4",                                     limit: 24
    t.string   "ri_result"
    t.date     "ri_return_date"
    t.string   "ri_reason_for_return"
    t.string   "ri_for_the_return_list"
    t.string   "ri_refund_description"
    t.string   "sealing"
    t.date     "se_date"
    t.date     "se_verification_operation_after_ts"
    t.string   "se_result"
    t.date     "se_return_date"
    t.string   "se_reason_for_return"
    t.string   "se_for_the_return_list"
    t.string   "se_refund_description"
    t.string   "heat_treatment_in_an_argon_chamber"
    t.date     "ht_date"
    t.date     "ht_verification_operation_after_ts"
    t.string   "ht_optical_contact_monitoring"
    t.float    "ht_burning_voltage_after_2_sec",                              limit: 24
    t.float    "ht_burning_voltage_after_35_sec",                             limit: 24
    t.float    "ht_burning_voltage_after_10_min",                             limit: 24
    t.float    "ht_threshold_current_after_XY",                               limit: 24
    t.float    "ht_the_change_in_the_argon_pressure_in_the_resonator",        limit: 24
    t.string   "ht_result"
    t.date     "ht_return_date"
    t.string   "ht_reason_for_return"
    t.string   "ht_for_the_return_list"
    t.string   "ht_refund_description"
    t.integer  "cathode_id"
    t.integer  "leg_id"
    t.integer  "fmirror_id"
    t.integer  "pmirror_1_id"
    t.integer  "pmirror_2_id"
    t.integer  "smirror_id"
    t.integer  "sensor_id"
    t.float    "al_selectivity",                                              limit: 24
    t.float    "ts_measurement_of_combustion_voltage_35",                     limit: 24
    t.string   "test_3"
    t.float    "t3_selectivity",                                              limit: 24
    t.float    "t3_losses",                                                   limit: 24
    t.date     "t3_im_date"
    t.string   "t3_im_fit"
    t.date     "cr_im_date"
    t.string   "cr_im_fit"
    t.string   "cr_appearance"
    t.float    "cr_combustion_voltage",                                       limit: 24
    t.float    "cr_losses",                                                   limit: 24
    t.float    "cr_selectivity",                                              limit: 24
    t.float    "ri_combustion_voltage_2s",                                    limit: 24
    t.float    "ri_combustion_voltage_35s",                                   limit: 24
    t.float    "ri_combustion_voltage_10m",                                   limit: 24
    t.string   "se_appearance"
    t.date     "se_im_date"
    t.string   "se_im_fit"
    t.string   "test_9"
    t.string   "t9_appearance"
    t.string   "t9_optical_contact"
    t.date     "t9_im_date"
    t.string   "t9_im_fit"
    t.float    "t9_combustion_voltage_2s",                                    limit: 24
    t.float    "t9_combustion_voltage_35s",                                   limit: 24
    t.float    "t9_combustion_voltage_10m",                                   limit: 24
    t.float    "t9_combustion_voltage_delta",                                 limit: 24
    t.float    "t9_threshold_current",                                        limit: 24
    t.float    "t9_omega",                                                    limit: 24
    t.string   "control"
    t.date     "cn_date"
    t.string   "ri_appearance"
    t.float    "t3_nitrogen_level",                                           limit: 24
    t.integer  "t3_nitrogen_level_rate"
    t.float    "t9_nitrogen_level",                                           limit: 24
    t.integer  "t9_nitrogen_level_rate"
    t.float    "cr_nitrogen_level",                                           limit: 24
    t.float    "cr_nitrogen_level_rate",                                      limit: 24
    t.integer  "end_count"
    t.float    "se_threshold_voltage_1GRP",                                   limit: 24
    t.float    "se_threshold_voltage_2GRP",                                   limit: 24
    t.string   "remark"
    t.float    "cr_combustion_voltage_delta",                                 limit: 24
    t.float    "cr_capture_in_fashion_delta",                                 limit: 24
    t.float    "cr_output_frequency_delta",                                   limit: 24
    t.float    "ri_capture_in_fashion_delta",                                 limit: 24
    t.float    "ri_output_frequency_delta",                                   limit: 24
    t.boolean  "suitable"
    t.float    "al_leg_clamp",                                                limit: 24
    t.string   "t3_result"
    t.date     "t3_return_date"
    t.string   "t3_reason_for_return"
    t.string   "t3_for_the_return_list"
    t.string   "t3_refund_description"
    t.string   "t9_result"
    t.date     "t9_return_date"
    t.string   "t9_reason_for_return"
    t.string   "t9_for_the_return_list"
    t.string   "t9_refund_description"
    t.string   "cn_result"
    t.date     "cn_return_date"
    t.string   "cn_reason_for_return"
    t.string   "cn_for_the_return_list"
    t.string   "cn_refund_description"
    t.float    "t9_losses",                                                   limit: 24
    t.float    "t9_selectivity",                                              limit: 24
  end

  create_table "bad_sensors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.integer  "resonator_id"
    t.date     "date_of_manufacture_of_the_resonator"
    t.string   "remark"
    t.string   "align_prisms_and_fpu"
    t.date     "ap_date"
    t.float    "ap_the_combustion_voltage_in_2_sec",                             limit: 24
    t.float    "ap_the_combustion_voltage_after_35_sec",                         limit: 24
    t.float    "ap_the_combustion_voltage_after_10_min",                         limit: 24
    t.float    "ap_the_transmission_coefficient_pb1",                            limit: 24
    t.float    "ap_the_transmission_coefficient_pb2",                            limit: 24
    t.string   "ap_result"
    t.date     "ap_return_date"
    t.string   "ap_reason_for_return"
    t.string   "ap_for_the_return_list"
    t.string   "ap_refund_description"
    t.string   "checking_the_build_quality_in_screens"
    t.date     "cb_date"
    t.float    "cb_the_combustion_voltage_in_2_sec",                             limit: 24
    t.float    "cb_the_combustion_voltage_after_35_sec",                         limit: 24
    t.float    "cb_the_combustion_voltage_after_10_min",                         limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_1_in_the_mode_plus",       limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_2_in_the_mode_plus",       limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_1_in_the_mode_minus",      limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_2_in_the_mode_minus",      limit: 24
    t.float    "cb_the_phase_difference_of_the_output_signals",                  limit: 24
    t.float    "cb_the_transmission_coefficient_of_the_modes_in_the_mode_plus",  limit: 24
    t.float    "cb_the_transmission_coefficient_of_the_modes_in_the_mode_minus", limit: 24
    t.float    "cb_the_transmission_coefficient",                                limit: 24
    t.float    "cb_the_amplitude_of_the_perimeter_signal_of_the_mode_plus",      limit: 24
    t.float    "cb_the_amplitude_of_the_perimeter_signal_of_the_mode_minus",     limit: 24
    t.string   "cb_result"
    t.date     "cb_return_date"
    t.string   "cb_reason_for_return"
    t.string   "cb_for_the_return_list"
    t.string   "cb_refund_description"
    t.string   "check_for_resistance_to_sinusoidal_vibration"
    t.date     "cf_date"
    t.string   "cf_result"
    t.date     "cf_return_date"
    t.string   "cf_reason_for_return"
    t.string   "cf_for_the_return_list"
    t.string   "cf_refund_description"
    t.string   "check_by_pm"
    t.string   "cp_date"
    t.string   "cp_result"
    t.date     "cp_return_date"
    t.string   "cp_reason_for_return"
    t.string   "cp_for_the_return_list"
    t.string   "cp_refund_description"
    t.string   "psi"
    t.date     "ps_date"
    t.float    "ps_the_amplitude_of_the_output_signal_1_in_the_working_mode",    limit: 24
    t.float    "ps_the_amplitude_of_the_output_signal_2_in_the_operating_mode",  limit: 24
    t.float    "ps_the_transmission_coefficient_of_the_ou_in_the_working_mode",  limit: 24
    t.float    "ps_combustion_voltage",                                          limit: 24
    t.string   "ps_result"
    t.date     "ps_return_date"
    t.string   "ps_reason_for_return"
    t.string   "ps_for_the_return_list"
    t.string   "ps_refund_description"
    t.integer  "end_count"
    t.boolean  "suitable"
    t.datetime "created_at",                                                                null: false
    t.datetime "updated_at",                                                                null: false
  end

  create_table "bad_stages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.string   "bad_stage"
    t.string   "ev_vacuum_post_number"
    t.datetime "created_at"
  end

  create_table "cathodes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date     "date_of_manufacture"
    t.string   "manufacturer_supplier"
    t.string   "description_of_appearance"
    t.string   "oxidation_mode"
    t.date     "date_of_transfer_to_assembly"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "resonator_id"
    t.string   "number"
    t.string   "result_of_manufacturing_the_workpiece"
    t.date     "oxidation_date"
    t.float    "breakdown_voltage",                     limit: 24
    t.float    "combustion_voltage",                    limit: 24
    t.string   "tb_first_inclusion"
    t.string   "tb_subsequent_inclusions"
    t.date     "date_of_transfer_to_assembly_otk"
    t.string   "the_result_of_cathode_manufacturing"
    t.string   "reason_for_rejection"
    t.string   "deviations"
    t.date     "date_of_installation_in_the_resonator"
  end

  create_table "fmirrors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resonator_id"
    t.integer  "resonator_location"
    t.integer  "cycle"
    t.date     "completion_date"
    t.string   "number"
    t.float    "integral_scattering",         limit: 24
    t.float    "phase_anisotropy",            limit: 24
    t.string   "manufacturer"
    t.string   "name_of_the_employee"
    t.string   "the_rejection_sign_landing"
    t.string   "the_rejection_sign_spraying"
    t.text     "comment",                     limit: 65535
    t.string   "employee_name"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.boolean  "defect"
  end

  create_table "fsubstrates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "fmirror_id"
    t.integer  "cycle"
    t.date     "completion_date"
    t.string   "number"
    t.float    "roughness",       limit: 24
    t.string   "manufacturer"
    t.string   "name_polisher"
    t.text     "comment",         limit: 65535
    t.string   "employee_name"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "dev"
    t.integer  "dev_id"
    t.string   "operation"
    t.string   "operator"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "legs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "disk_number"
    t.date    "ds_date_of_manufacture"
    t.float   "ds_the_nonplaneity_of_the_disc_n",                  limit: 24
    t.float   "ds_the_discontinuity_of_the_disk_n",                limit: 24
    t.string  "ds_appearance_ok"
    t.string  "ds_description_of_appearance_ok"
    t.date    "ds_date_control_ok"
    t.date    "sd_soldering_date"
    t.float   "sd_non_flatness_of_the_foot_n",                     limit: 24
    t.float   "sd_the_non_flatness_of_the_foot_n",                 limit: 24
    t.string  "sd_germ_checking_for_leaks"
    t.date    "sd_germ_date_control"
    t.string  "sd_appearance_ok"
    t.string  "sd_description_of_appearance_ok"
    t.date    "sd_date_control_ok"
    t.date    "date_of_transfer_to_the_assembly_in_the_resonator"
    t.integer "resonator_id"
    t.date    "return_date"
    t.string  "reason_for_return"
    t.string  "for_the_return_list"
    t.integer "number_of_assemblies"
    t.date    "date_of_installation_in_the_resonator"
    t.string  "return"
    t.string  "deviations"
    t.string  "xray_file"
  end

  create_table "loss_of_resonators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "number"
    t.float  "diffractions_before",   limit: 24
    t.float  "difference_in_losses",  limit: 24
    t.string "ev_vacuum_post_number"
    t.date   "created_at"
  end

  create_table "mirrors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number_of_substrate"
    t.date     "date_of_manufacture_of_the_substrate"
    t.date     "date_of_deposition"
    t.string   "spray_installation"
    t.string   "appearance"
    t.string   "description_of_appearance"
    t.date     "date_control"
    t.float    "tl_total_losses",                                   limit: 24
    t.string   "tl_measurement"
    t.date     "tl_date_of_measurement"
    t.float    "ci_coefficient_of_integral_scattering",             limit: 24
    t.string   "ci_measurement"
    t.date     "ci_date_of_measurement"
    t.float    "pa_phase_anisotropy",                               limit: 24
    t.date     "pa_ci_date_of_measurement"
    t.float    "tr_transmittance",                                  limit: 24
    t.date     "tr_date_of_measurement"
    t.date     "date_of_transfer_to_the_assembly_in_the_resonator"
    t.integer  "resonator_id"
    t.date     "return_date"
    t.string   "reason_for_return"
    t.string   "for_the_return_list"
    t.integer  "number_of_assemblies"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "mirror_type"
    t.string   "name_of_polisher"
    t.float    "roughness_of_the_substrate",                        limit: 24
    t.float    "radius_of_curvature_is_nominal",                    limit: 24
    t.float    "radius_of_curvature_is_actual",                     limit: 24
    t.string   "description_of_appearance_substrate"
    t.string   "manufacturer"
    t.string   "return"
    t.string   "deviations"
    t.date     "date_of_installation_in_the_resonator"
    t.date     "date_of_measurement_of_the_radius_of_curvature"
    t.string   "result_of_polishing"
  end

  create_table "resonators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.string   "alignment"
    t.date     "al_date"
    t.date     "al_cathode_date_of_manufacture"
    t.date     "al_leg_date_of_manufacture"
    t.date     "al_fmirror_date_of_manufacture"
    t.date     "al_piezo_mirrors_1_date_of_manufacture"
    t.date     "al_piezo_mirrors_2_date_of_manufacture"
    t.date     "al_smirror_date_of_manufacture"
    t.float    "al_TEM00_loss",                                               limit: 24
    t.float    "al_TEM01_loss",                                               limit: 24
    t.string   "al_result"
    t.date     "al_return_date"
    t.string   "al_reason_for_return"
    t.string   "al_refund_description"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "al_for_the_return_list"
    t.string   "evo"
    t.date     "ev_date"
    t.string   "ev_vacuum_post_number"
    t.float    "ev_threshold_current_at_the_beginning_of_the_workout",        limit: 24
    t.float    "ev_burning_stress_at_the_beginning_of_the_workout",           limit: 24
    t.float    "ev_burning_voltage_after_5_hours_of_training",                limit: 24
    t.float    "ev_combustion_voltage_after_refilling",                       limit: 24
    t.float    "ev_the_burning_voltage_after_10_hours_of_training",           limit: 24
    t.float    "ev_threshold_current_at_the_end_of_the_workout",              limit: 24
    t.float    "ev_the_combustion_voltage_at_the_beginning_of_stabilization", limit: 24
    t.float    "ev_the_combustion_voltage_after_1_hour_of_stabilization",     limit: 24
    t.float    "ev_the_combustion_voltage_after_2_hours_of_stabilization",    limit: 24
    t.float    "ev_the_combustion_voltage_after_3_hours_of_stabilization",    limit: 24
    t.float    "ev_threshold_current_at_the_end_of_stabilization",            limit: 24
    t.string   "ev_result"
    t.date     "ev_return_date"
    t.string   "ev_reason_for_return"
    t.string   "ev_for_the_return_list"
    t.string   "ev_refund_description"
    t.string   "thermostabilization"
    t.date     "ts_date"
    t.date     "ts_verification_operation_after_ts"
    t.string   "ts_optical_contact_monitoring"
    t.float    "ts_measurement_of_combustion_voltage",                        limit: 24
    t.date     "ts_date_of_preheating_of_getter"
    t.string   "ts_result"
    t.date     "ts_return_date"
    t.string   "ts_reason_for_return"
    t.string   "ts_for_the_return_list"
    t.string   "ts_refund_description"
    t.string   "cold_resistance"
    t.date     "cr_date"
    t.date     "cr_verification_operation_after_ts"
    t.string   "cr_optical_contact_monitoring"
    t.string   "cr_description_of_optical_contact"
    t.float    "cr_burning_voltage_after_2_sec",                              limit: 24
    t.float    "cr_burning_voltage_after_35_sec",                             limit: 24
    t.float    "cr_burning_voltage_after_10_min",                             limit: 24
    t.float    "cr_threshold_current_after_XY",                               limit: 24
    t.float    "cr_output_frequency_in_mode_1",                               limit: 24
    t.float    "cr_capture_in_fashion_1",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_2",                               limit: 24
    t.float    "cr_capture_on_fashion_2",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_3",                               limit: 24
    t.float    "cr_capture_in_fashion_3",                                     limit: 24
    t.float    "cr_output_frequency_in_mode_4",                               limit: 24
    t.float    "cr_capture_on_fashion_4",                                     limit: 24
    t.string   "cr_result"
    t.date     "cr_return_date"
    t.string   "cr_reason_for_return"
    t.string   "cr_for_the_return_list"
    t.string   "cr_refund_description"
    t.string   "running_in"
    t.date     "ri_date"
    t.date     "ri_verification_operation_after_ts"
    t.string   "ri_optical_contact_monitoring"
    t.string   "ri_description_of_optical_contact"
    t.float    "ri_threshold_current_after_XY",                               limit: 24
    t.float    "ri_output_frequency_in_mode_1",                               limit: 24
    t.float    "ri_capture_in_fashion_1",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_2",                               limit: 24
    t.float    "ri_capture_on_fashion_2",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_3",                               limit: 24
    t.float    "ri_capture_in_fashion_3",                                     limit: 24
    t.float    "ri_output_frequency_in_mode_4",                               limit: 24
    t.float    "ri_capture_on_fashion_4",                                     limit: 24
    t.string   "ri_result"
    t.date     "ri_return_date"
    t.string   "ri_reason_for_return"
    t.string   "ri_for_the_return_list"
    t.string   "ri_refund_description"
    t.string   "sealing"
    t.date     "se_date"
    t.date     "se_verification_operation_after_ts"
    t.string   "se_result"
    t.date     "se_return_date"
    t.string   "se_reason_for_return"
    t.string   "se_for_the_return_list"
    t.string   "se_refund_description"
    t.string   "heat_treatment_in_an_argon_chamber"
    t.date     "ht_date"
    t.date     "ht_verification_operation_after_ts"
    t.string   "ht_optical_contact_monitoring"
    t.float    "ht_burning_voltage_after_2_sec",                              limit: 24
    t.float    "ht_burning_voltage_after_35_sec",                             limit: 24
    t.float    "ht_burning_voltage_after_10_min",                             limit: 24
    t.float    "ht_threshold_current_after_XY",                               limit: 24
    t.float    "ht_the_change_in_the_argon_pressure_in_the_resonator",        limit: 24
    t.string   "ht_result"
    t.date     "ht_return_date"
    t.string   "ht_reason_for_return"
    t.string   "ht_for_the_return_list"
    t.string   "ht_refund_description"
    t.integer  "cathode_id"
    t.integer  "leg_id"
    t.integer  "fmirror_id"
    t.integer  "pmirror_1_id"
    t.integer  "pmirror_2_id"
    t.integer  "smirror_id"
    t.integer  "sensor_id"
    t.float    "al_selectivity",                                              limit: 24
    t.float    "ts_measurement_of_combustion_voltage_35",                     limit: 24
    t.string   "test_3"
    t.float    "t3_selectivity",                                              limit: 24
    t.float    "t3_losses",                                                   limit: 24
    t.date     "t3_im_date"
    t.string   "t3_im_fit"
    t.date     "cr_im_date"
    t.string   "cr_im_fit"
    t.string   "cr_appearance"
    t.float    "cr_combustion_voltage",                                       limit: 24
    t.float    "cr_losses",                                                   limit: 24
    t.float    "cr_selectivity",                                              limit: 24
    t.float    "ri_combustion_voltage_2s",                                    limit: 24
    t.float    "ri_combustion_voltage_35s",                                   limit: 24
    t.float    "ri_combustion_voltage_10m",                                   limit: 24
    t.string   "se_appearance"
    t.date     "se_im_date"
    t.string   "se_im_fit"
    t.string   "test_9"
    t.string   "t9_appearance"
    t.string   "t9_optical_contact"
    t.date     "t9_im_date"
    t.string   "t9_im_fit"
    t.float    "t9_combustion_voltage_2s",                                    limit: 24
    t.float    "t9_combustion_voltage_35s",                                   limit: 24
    t.float    "t9_combustion_voltage_10m",                                   limit: 24
    t.float    "t9_combustion_voltage_delta",                                 limit: 24
    t.float    "t9_threshold_current",                                        limit: 24
    t.float    "t9_omega",                                                    limit: 24
    t.string   "control"
    t.date     "cn_date"
    t.string   "ri_appearance"
    t.float    "t3_nitrogen_level",                                           limit: 24
    t.integer  "t3_nitrogen_level_rate"
    t.float    "t9_nitrogen_level",                                           limit: 24
    t.integer  "t9_nitrogen_level_rate"
    t.float    "cr_nitrogen_level",                                           limit: 24
    t.float    "cr_nitrogen_level_rate",                                      limit: 24
    t.integer  "end_count"
    t.float    "se_threshold_voltage_1GRP",                                   limit: 24
    t.float    "se_threshold_voltage_2GRP",                                   limit: 24
    t.string   "remark"
    t.float    "cr_combustion_voltage_delta",                                 limit: 24
    t.float    "cr_capture_in_fashion_delta",                                 limit: 24
    t.float    "cr_output_frequency_delta",                                   limit: 24
    t.float    "ri_capture_in_fashion_delta",                                 limit: 24
    t.float    "ri_output_frequency_delta",                                   limit: 24
    t.boolean  "suitable"
    t.float    "al_leg_clamp",                                                limit: 24
    t.string   "t3_result"
    t.date     "t3_return_date"
    t.string   "t3_reason_for_return"
    t.string   "t3_for_the_return_list"
    t.string   "t3_refund_description"
    t.string   "t9_result"
    t.date     "t9_return_date"
    t.string   "t9_reason_for_return"
    t.string   "t9_for_the_return_list"
    t.string   "t9_refund_description"
    t.string   "cn_result"
    t.date     "cn_return_date"
    t.string   "cn_reason_for_return"
    t.string   "cn_for_the_return_list"
    t.string   "cn_refund_description"
    t.float    "t9_losses",                                                   limit: 24
    t.float    "t9_selectivity",                                              limit: 24
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "sensors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "number"
    t.integer  "resonator_id"
    t.date     "date_of_manufacture_of_the_resonator"
    t.date     "ap_date"
    t.float    "ap_the_combustion_voltage_in_2_sec",                             limit: 24
    t.float    "ap_the_combustion_voltage_after_35_sec",                         limit: 24
    t.float    "ap_the_combustion_voltage_after_10_min",                         limit: 24
    t.float    "ap_the_transmission_coefficient_pb1",                            limit: 24
    t.float    "ap_the_transmission_coefficient_pb2",                            limit: 24
    t.string   "ap_result"
    t.date     "ap_return_date"
    t.string   "ap_reason_for_return"
    t.string   "ap_for_the_return_list"
    t.string   "ap_refund_description"
    t.string   "checking_the_build_quality_in_screens"
    t.date     "cb_date"
    t.float    "cb_the_combustion_voltage_in_2_sec",                             limit: 24
    t.float    "cb_the_combustion_voltage_after_35_sec",                         limit: 24
    t.float    "cb_the_combustion_voltage_after_10_min",                         limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_1_in_the_mode_plus",       limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_2_in_the_mode_plus",       limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_1_in_the_mode_minus",      limit: 24
    t.float    "cb_the_amplitude_of_the_output_signal_2_in_the_mode_minus",      limit: 24
    t.float    "cb_the_phase_difference_of_the_output_signals",                  limit: 24
    t.float    "cb_the_transmission_coefficient_of_the_modes_in_the_mode_plus",  limit: 24
    t.float    "cb_the_transmission_coefficient_of_the_modes_in_the_mode_minus", limit: 24
    t.float    "cb_the_transmission_coefficient",                                limit: 24
    t.float    "cb_the_amplitude_of_the_perimeter_signal_of_the_mode_plus",      limit: 24
    t.float    "cb_the_amplitude_of_the_perimeter_signal_of_the_mode_minus",     limit: 24
    t.string   "cb_result"
    t.date     "cb_return_date"
    t.string   "cb_reason_for_return"
    t.string   "cb_for_the_return_list"
    t.string   "cb_refund_description"
    t.string   "check_for_resistance_to_sinusoidal_vibration"
    t.date     "cf_date"
    t.string   "cf_result"
    t.date     "cf_return_date"
    t.string   "cf_reason_for_return"
    t.string   "cf_for_the_return_list"
    t.string   "cf_refund_description"
    t.string   "psi"
    t.date     "ps_date"
    t.float    "ps_the_amplitude_of_the_output_signal_2_in_the_operating_mode",  limit: 24
    t.float    "ps_the_amplitude_of_the_output_signal_1_in_the_working_mode",    limit: 24
    t.float    "ps_the_transmission_coefficient_of_the_ou_in_the_working_mode",  limit: 24
    t.float    "ps_combustion_voltage",                                          limit: 24
    t.string   "ps_result"
    t.date     "ps_return_date"
    t.string   "ps_reason_for_return"
    t.string   "ps_for_the_return_list"
    t.string   "ps_refund_description"
    t.datetime "created_at",                                                                null: false
    t.datetime "updated_at",                                                                null: false
    t.string   "align_prisms_and_fpu"
    t.string   "remark"
    t.string   "check_by_pm"
    t.string   "cp_date"
    t.string   "cp_result"
    t.date     "cp_return_date"
    t.string   "cp_reason_for_return"
    t.string   "cp_for_the_return_list"
    t.string   "cp_refund_description"
    t.boolean  "suitable"
    t.integer  "end_count"
  end

  create_table "smirrors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resonator_id"
    t.integer  "resonator_location"
    t.integer  "cycle"
    t.date     "completion_date"
    t.string   "number"
    t.float    "the_radius_of_curvature_of_the_substrates", limit: 24
    t.float    "integral_scattering",                       limit: 24
    t.float    "phase_anisotropy",                          limit: 24
    t.string   "manufacturer"
    t.string   "name_of_the_employee"
    t.text     "comment",                                   limit: 65535
    t.string   "employee_name"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.boolean  "defect"
    t.string   "the_rejection_sign_landing"
    t.string   "the_rejection_sign_spraying"
  end

  create_table "ssubstrates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "smirror_id"
    t.integer  "cycle"
    t.date     "completion_date"
    t.string   "number"
    t.float    "radius_of_curvature", limit: 24
    t.float    "roughness",           limit: 24
    t.float    "alignment",           limit: 24
    t.string   "manufacturer"
    t.string   "name_polisher"
    t.text     "comment",             limit: 65535
    t.string   "employee_name"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username",               default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "full_name"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

end
