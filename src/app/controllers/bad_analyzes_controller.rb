class BadAnalyzesController < ApplicationController

    before_action :resonator_list_cols,
    only: [ :bad_stage, :return_stage ]

    before_action :sensor_list_cols,
    only: [ :bad_stage, :return_stage ]

    before_action :returns_list_cols,
    only: [ :bad_evo, :return_evo ]

    def bad_total

        @reject = true

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.civil(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.civil(year, month, 1)

            date_interval = date_in..date_out

        end

        reject_in_post = BadResonator.all
                          .where(al_date: date_interval)
                          .where.not(number: Resonator.select(:number))
                          .where(ev_result: 'Возврат')
                          .select(:number).count.to_f

        reject_after_post = BadResonator.all
                            .where(al_date: date_interval)
                            .where.not(number: Resonator.select(:number))
                            .where.not(al_result: 'Возврат', ev_result: 'Возврат') # Тут что-то не так
                            .select(:number).count.to_f

        total = reject_in_post + reject_after_post

        @reject_in_post = reject_in_post / total * 100
        @reject_after_post = reject_after_post / total * 100

        @total = Resonator.where(al_date: date_interval)
        @total = @total.group_by_month(:al_date).count
        .map{ |date, count|
            {
                # Год / Месяц
                date: date,

                # Количество запущенных резонаторов
                # ---------------------------------------------------
                # Количество резонаторов сгрупированных по :al_date
                # плюс количество уникальных записей из BadResonator
                # за тот же период

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct.count.map.sum{ |c| c.last },

                # Количество переборок
                # ---------------------------------------------
                # Выбираются все резонаторы из BadResonator,
                # :number которых встречается более одного раза
                # все значения сумируются по месяцам
                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                # Брак на посту, рассматриваются только последние записи
                in_post:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_result: 'Возврат')
                .group(:number).count.count,

                # Брак после поста
                after_post:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where.not(al_result: 'Возврат', ev_result: 'Возврат')
                .group(:number)
                .count.count,

                # Годных
                good: Resonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(end_count: 10, suitable: true)
                .count,

                # Не годных
                not_good: BadResonator.where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .select(:number).distinct.count

            }
        }

    end

    def bad_evo
        @reject = true

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.civil(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.civil(year, month, 1)

            date_interval = date_in..date_out

        end

        @evo_1 = Resonator.where(al_date: date_interval)
        @evo_1 = @evo_1.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                depressurization:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_reason_for_return: 'group_0')
                .select(:number)
                .distinct
                .count,

                no_generate:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_for_the_return_list: 'Отсутствие генерации')
                .select(:number)
                .distinct
                .count,

                threshold_current:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_for_the_return_list: 'Пороговый ток')
                .select(:number)
                .distinct
                .count,

                combustion_voltage:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_for_the_return_list: 'Напряжение горения')
                .select(:number)
                .distinct
                .count,

                loss:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(al_reason_for_return: 'group_1')
                .select(:number)
                .distinct
                .count,

                view:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(t3_reason_for_return: 'group_0')
                .select(:number)
                .distinct
                .count,

                other_1:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(al_reason_for_return: 'group_0')
                .select(:number)
                .distinct
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(al_reason_for_return: 'other')
                .select(:number)
                .distinct
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_reason_for_return: 'other')
                .select(:number)
                .distinct
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(t3_reason_for_return: 'other')
                .select(:number)
                .distinct
                .count

            }
        }

        @evo_2 = Resonator.where(al_date: date_interval)
        @evo_2 = @evo_2.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                leg:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Оптический контакт, ножка", "Оптический контакт по ножке")
                .select(:number)
                .distinct
                .count,

                mirror:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Оптический контакт, зеркало", "Оптический контакт по зеркалам")
                .select(:number)
                .distinct
                .count,

                glass:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Трещина в стекле", "Трещина в стекле")
                .select(:number)
                .distinct
                .count,

                electrodes:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Течь по пайке электродов ножки", "Пайка ножки")
                .select(:number)
                .distinct
                .count,

                place:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_for_the_return_list: 'Течь по соседнему месту')
                .select(:number)
                .distinct
                .count,

                other_2:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_reason_for_return: 'group_0')
                .where.not("ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ?",
                'Оптический контакт, ножка','Оптический контакт, зеркало','Трещина в стекле','Течь по пайке электродов ножки','Течь по соседнему месту')
                .select(:number)
                .distinct
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("t3_for_the_return_list = ? OR t3_for_the_return_list = ?",
                'Газоотделение (водород)','Не определено')
                .select(:number)
                .distinct
                .count

            }
        }

    end

    def bad_after_evo

        @reject = true

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.new(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.new(year, month, 1)

            date_interval = date_in..date_out

        end

        @evo_1 = Resonator.where(al_date: date_interval)
        @evo_1 = @evo_1.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                loss:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR cr_for_the_return_list = ?",
                    "Потери",
                    "Селективность")
                .select(:number)
                .distinct
                .count,

                gas:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR ht_reason_for_return = ?
                    OR t9_reason_for_return = ?",
                    "group_1",
                    "group_2",
                    "group_0",
                    "group_0")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_reason_for_return = ?
                    OR cb_reason_for_return = ?
                    OR cf_reason_for_return = ?
                    OR ps_reason_for_return = ?
                    OR cp_reason_for_return = ?",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0")
                .select(:number)
                .distinct
                .count,

                capture:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR ri_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Зона захвата",
                    "Зона захвата",
                    "Зона захвата")
                .select(:number)
                .distinct
                .count,

                amp:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала")
                .select(:number)
                .distinct
                .count,

                view:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ts_reason_for_return = ?
                    OR cr_reason_for_return = ?
                    OR ri_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR t9_reason_for_return = ?
                    OR cn_reason_for_return = ?",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_1",
                    "group_1")
                .select(:number)
                .distinct
                .count,

                threshold_voltage:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(se_for_the_return_list: 'Пороговое напряжение пробоя')
                .select(:number)
                .distinct
                .count,

                moda:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                OR cb_for_the_return_list = ? ",
                "Поперечная мода",
                "Поперечная мода")
                .select(:number)
                .distinct
                .count,

                current:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ? ",
                    "Пороговый ток",
                    "Пороговый ток")
                .select(:number)
                .distinct
                .count,

                no_generate:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации")
                .select(:number)
                .distinct
                .count,

                combustion_voltage:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения")
                .select(:number)
                .distinct
                .count,

                no_burn:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(cp_for_the_return_list: 'Неподжиг')
                .select(:number)
                .distinct
                .count,

                drift:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("cp_for_the_return_list = ?
                    OR ps_for_the_return_list = ?",
                    "Точностные параметры (дрейф, СКО)",
                    "Точностные параметры (дрейф, СКО)")
                .select(:number)
                .distinct
                .count,

                other_1:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("ts_reason_for_return = ?
                    OR cr_reason_for_return = ?
                    OR ri_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR ht_reason_for_return = ?
                    OR t9_reason_for_return = ?
                    OR cn_reason_for_return = ?",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_reason_for_return = ?
                    OR cb_reason_for_return = ?
                    OR cf_reason_for_return = ?
                    OR ps_reason_for_return = ?
                    OR cp_reason_for_return = ?",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other")
                .select(:number)
                .distinct
                .count,


            }
        }

        @evo_2 = Resonator.where(al_date: date_interval)
        @evo_2 = @evo_2.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                brazing:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки")
                .select(:number)
                .distinct
                .count,

                leg:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке")
                .select(:number)
                .distinct
                .count,

                glass:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам")
                .select(:number)
                .distinct
                .count,

                crack:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(cr_for_the_return_list: 'Трещина в стекле')
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .distinct
                .count,

                gas:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .distinct
                .count,


                undefined:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено")
                .select(:number)
                .distinct
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено")
                .select(:number)
                .distinct
                .count,

                other_2:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_reason_for_return: 'group_0')
                .where.not("ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ?",
                'Оптический контакт, ножка','Оптический контакт, зеркало','Трещина в стекле','Течь по пайке электродов ножки','Течь по соседнему месту')
                .select(:number)
                .distinct
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where("t3_for_the_return_list = ? OR t3_for_the_return_list = ?",
                'Газоотделение (водород)','Не определено')
                .select(:number)
                .distinct
                .count

            }
        }

    end

    def bad_stage

        @reject = true

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.new(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.new(year, month, 1)

            date_interval = date_in..date_out

        end

        @stage = Resonator.where(al_date: date_interval)
        @stage = @stage.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .distinct
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                alignment:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(al_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                evo:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ev_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                test_3:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(t3_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                thermostabilization:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ts_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                cold_resistance:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(cr_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                running_in:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ri_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                sealing:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(se_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                heat_treatment_in_an_argon_chamber:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(ht_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                test_9:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(t9_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                control:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .where(cn_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                align_prisms_and_fpu:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(ap_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                checking_the_build_quality_in_screens:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(cb_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                check_for_resistance_to_sinusoidal_vibration:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(cf_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                check_by_pm:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(cp_result: 'Возврат')
                .select(:number)
                .distinct
                .count,

                psi:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where.not(number: Sensor.select(:number))
                .where(ps_result: 'Возврат')
                .select(:number)
                .distinct
                .count
            }
        }

    end

    def return_total

        @reject = false

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.civil(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.civil(year, month, 1)

            date_interval = date_in..date_out

        end

        reject_in_post = BadResonator.all
                          .where(al_date: date_interval)
                          .where(ev_result: 'Возврат')
                          .select(:number).count.to_f

        reject_after_post = BadResonator.all
                            .where(al_date: date_interval)
                            .where.not(al_result: 'Возврат', ev_result: 'Возврат')
                            .select(:number).count.to_f

        total = reject_in_post + reject_after_post

        @reject_in_post = reject_in_post / total * 100
        @reject_after_post = reject_after_post / total * 100

        @total = Resonator.where(al_date: date_interval)
        @total = @total.group_by_month(:al_date).count
        .map{ |date, count|
            {
                # Год / Месяц
                date: date,

                # Количество запущенных резонаторов
                # ---------------------------------------------------
                # Количество резонаторов сгрупированных по :al_date
                # плюс количество записей из BadResonator
                # за тот же период
                run_resonators:
                count + BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .group(:number)
                .count.map.sum{ |c| c.last },


                # Брак на посту
                in_post:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_result: 'Возврат')
                .group(:number)
                .count.count,

                # Брак после поста
                after_post:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where.not(al_result: 'Возврат', ev_result: 'Возврат')
                .group(:number)
                .count.count,

                # Годных
                good: Resonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(end_count: 10, suitable: true)
                .count,

                # Не годных
                not_good: BadResonator.where(al_date: date..date + 1.month - 1.day)
                .where.not(number: Resonator.select(:number))
                .select(:number).distinct.count
            }
        }
    end

    def return_evo

        @reject = false

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.new(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.new(year, month, 1)

            date_interval = date_in..date_out

        end

        @evo_1 = Resonator.where(al_date: date_interval)
        @evo_1 = @evo_1.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                depressurization:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_reason_for_return: 'group_0')
                .select(:number)
                .count,

                no_generate:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_for_the_return_list: 'Отсутствие генерации')
                .select(:number)
                .count,

                threshold_current:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_for_the_return_list: 'Пороговый ток')
                .select(:number)
                .count,

                combustion_voltage:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_for_the_return_list: 'Напряжение горения')
                .select(:number)
                .count,

                loss:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(al_reason_for_return: 'group_1')
                .select(:number)
                .count,

                view:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(t3_reason_for_return: 'group_0')
                .select(:number)
                .count,

                other_1:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(al_reason_for_return: 'group_0')
                .select(:number)
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(al_reason_for_return: 'other')
                .select(:number)
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_reason_for_return: 'other')
                .select(:number)
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(t3_reason_for_return: 'other')
                .select(:number)
                .count

            }
        }

        @evo_2 = Resonator.where(al_date: date_interval)
        @evo_2 = @evo_2.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                leg:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Оптический контакт, ножка", "Оптический контакт по ножке")
                .select(:number)
                .count,

                mirror:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Оптический контакт, зеркало", "Оптический контакт по зеркалам")
                .select(:number)
                .count,

                glass:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Трещина в стекле", "Трещина в стекле")
                .select(:number)
                .count,

                electrodes:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ev_for_the_return_list = ? OR t3_for_the_return_list = ?", "Течь по пайке электродов ножки", "Пайка ножки")
                .select(:number)
                .count,

                place:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_for_the_return_list: 'Течь по соседнему месту')
                .select(:number)
                .count,

                other_2:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_reason_for_return: 'group_0')
                .where.not("ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ?",
                'Оптический контакт, ножка','Оптический контакт, зеркало','Трещина в стекле','Течь по пайке электродов ножки','Течь по соседнему месту')
                .select(:number)
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("t3_for_the_return_list = ? OR t3_for_the_return_list = ?",
                'Газоотделение (водород)','Не определено')
                .select(:number)
                .count

            }
        }

    end

    def return_after_evo

        @reject = false

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.new(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.new(year, month, 1)

            date_interval = date_in..date_out

        end

        @evo_1 = Resonator.where(al_date: date_interval)
        @evo_1 = @evo_1.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                loss:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR cr_for_the_return_list = ?",
                    "Потери",
                    "Селективность")
                .select(:number)
                .count,

                gas:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR ht_reason_for_return = ?
                    OR t9_reason_for_return = ?",
                    "group_1",
                    "group_2",
                    "group_0",
                    "group_0")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_reason_for_return = ?
                    OR cb_reason_for_return = ?
                    OR cf_reason_for_return = ?
                    OR ps_reason_for_return = ?
                    OR cp_reason_for_return = ?",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0")
                .select(:number)
                .count,

                capture:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR ri_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Зона захвата",
                    "Зона захвата",
                    "Зона захвата")
                .select(:number)
                .count,

                amp:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала",
                    "Амплитуда сигнала")
                .select(:number)
                .count,

                view:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ts_reason_for_return = ?
                    OR cr_reason_for_return = ?
                    OR ri_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR t9_reason_for_return = ?
                    OR cn_reason_for_return = ?",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_0",
                    "group_1",
                    "group_1")
                .select(:number)
                .count,

                threshold_voltage:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(se_for_the_return_list: 'Пороговое напряжение пробоя')
                .select(:number)
                .count,

                moda:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                OR cb_for_the_return_list = ? ",
                "Поперечная мода",
                "Поперечная мода")
                .select(:number)
                .count,

                current:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ? ",
                    "Пороговый ток",
                    "Пороговый ток")
                .select(:number)
                .count,

                no_generate:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации",
                    "Нет генерации")
                .select(:number)
                .count,

                combustion_voltage:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения",
                    "Напряжение горения")
                .select(:number)
                .count,

                no_burn:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(cp_for_the_return_list: 'Неподжиг')
                .select(:number)
                .count,

                drift:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("cp_for_the_return_list = ?
                    OR ps_for_the_return_list = ?",
                    "Точностные параметры (дрейф, СКО)",
                    "Точностные параметры (дрейф, СКО)")
                .select(:number)
                .count,

                other_1:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("ts_reason_for_return = ?
                    OR cr_reason_for_return = ?
                    OR ri_reason_for_return = ?
                    OR se_reason_for_return = ?
                    OR ht_reason_for_return = ?
                    OR t9_reason_for_return = ?
                    OR cn_reason_for_return = ?",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_reason_for_return = ?
                    OR cb_reason_for_return = ?
                    OR cf_reason_for_return = ?
                    OR ps_reason_for_return = ?
                    OR cp_reason_for_return = ?",
                    "other",
                    "other",
                    "other",
                    "other",
                    "other")
                .select(:number)
                .count,

            }
        }

        @evo_2 = Resonator.where(al_date: date_interval)
        @evo_2 = @evo_2.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                brazing:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки",
                    "Пайка ножки")
                .select(:number)
                .count,

                leg:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке",
                    "Оптический контакт по ножке")
                .select(:number)
                .count,

                glass:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам",
                    "Оптический контакт по зеркалам")
                .select(:number)
                .count,

                crack:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(cr_for_the_return_list: 'Трещина в стекле')
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .count,

                gas:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)",
                    "Газоотделение (водород)")
                .select(:number)
                .count,


                undefined:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("cr_for_the_return_list = ?
                    OR se_for_the_return_list = ?
                    OR ht_for_the_return_list = ?
                    OR t9_for_the_return_list = ?",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено")
                .select(:number)
                .count +
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where("ap_for_the_return_list = ?
                    OR cb_for_the_return_list = ?
                    OR cf_for_the_return_list = ?
                    OR ps_for_the_return_list = ?
                    OR cp_for_the_return_list = ?",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено",
                    "Не определено")
                .select(:number)
                .count,

                other_2:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_reason_for_return: 'group_0')
                .where.not("ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ? OR ev_for_the_return_list = ?",
                'Оптический контакт, ножка','Оптический контакт, зеркало','Трещина в стекле','Течь по пайке электродов ножки','Течь по соседнему месту')
                .select(:number)
                .count +
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where("t3_for_the_return_list = ? OR t3_for_the_return_list = ?",
                'Газоотделение (водород)','Не определено')
                .select(:number)
                .count

            }
        }

    end

    def return_stage

        @reject = false

        if params[:date_in] and params[:date_out]

            date_i = params[:date_in]
            year = date_i[0..3].to_i
            month = date_i[5..6].to_i
            date_in = Date.new(year, month, 1)

            date_o = params[:date_out]
            year = date_o[0..3].to_i
            month = date_o[5..6].to_i
            date_out = Date.new(year, month, 1)

            date_interval = date_in..date_out

        end

        @stage = Resonator.where(al_date: date_interval)
        @stage = @stage.group_by_month(:al_date).count
        .map{ |date, count|
            {
                date: date,

                run_resonators:
                count + BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .select(:number)
                .count.map.sum{ |c| c.last },

                bust:
                BadResonator.all
                .group(:number)
                .where(al_date: date..date + 1.month - 1.day)
                .count.map.sum{ |t| t.last-1 },

                alignment:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(al_result: 'Возврат')
                .select(:number)
                .count,

                evo:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ev_result: 'Возврат')
                .select(:number)
                .count,

                test_3:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(t3_result: 'Возврат')
                .select(:number)
                .count,

                thermostabilization:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ts_result: 'Возврат')
                .select(:number)
                .count,

                cold_resistance:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(cr_result: 'Возврат')
                .select(:number)
                .count,

                running_in:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ri_result: 'Возврат')
                .select(:number)
                .count,

                sealing:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(se_result: 'Возврат')
                .select(:number)
                .count,

                heat_treatment_in_an_argon_chamber:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(ht_result: 'Возврат')
                .select(:number)
                .count,

                test_9:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(t9_result: 'Возврат')
                .select(:number)
                .count,

                control:
                BadResonator.all
                .where(al_date: date..date + 1.month - 1.day)
                .where(cn_result: 'Возврат')
                .select(:number)
                .count,

                align_prisms_and_fpu:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(ap_result: 'Возврат')
                .select(:number)
                .count,

                checking_the_build_quality_in_screens:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(cb_result: 'Возврат')
                .select(:number)
                .count,

                check_for_resistance_to_sinusoidal_vibration:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(cf_result: 'Возврат')
                .select(:number)
                .count,

                check_by_pm:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(cp_result: 'Возврат')
                .select(:number)
                .count,

                psi:
                BadSensor.all
                .where(ap_date: date..date + 1.month - 1.day)
                .where(ps_result: 'Возврат')
                .select(:number)
                .count
            }
        }

    end

end
