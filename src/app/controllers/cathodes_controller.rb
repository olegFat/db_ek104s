class CathodesController < ApplicationController

	before_action :authenticate_user!
	skip_before_action :authenticate_user!, only:[:index, :search]

    before_action :cathode_list_cols, 
    only: [ :index, :new, :edit, :show, :search ]

    before_action :returns_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :element_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :parameter_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :manufacturer_list_cols,
    only: [ :new, :edit, :search]

    before_action :set_cathode,
    only: [ :edit, :update, :destroy, :show]

    before_action :mode_list_cols,
    only: [ :new, :edit, :search]

	def index
        # Текущая страница
        if $cc_page == nil
            $cc_page = 1
        end

        if params[:page]
            $cc_page = params[:page]
        end

        @cathodes = Cathode.order(created_at: :desc).page($cc_page).per_page(25)
	end

    def search
        @search_val = params[:search_val]
        if @search_val
            @cathodes = Cathode.where("number LIKE ?", "%#{@search_val}%").order(number: :desc).page(params[:page]).per_page(25)
        end
    end

	def new
		@cathode = Cathode.new
	end

	def create
		@cathode = Cathode.new(cathode_params)
		if @cathode.save
			redirect_to cathodes_path,
			notice: "Добавлена запись #{@cathode.number}"
		else
			redirect_to new_cathode_path,
			notice: @cathode.errors.first[1]
		end
	end

	def edit
	end

	def update
		update_params = cathode_params 
		
		if update_params[:the_result_of_cathode_manufacturing] == 'Годен'
			update_params[:reason_for_rejection] = nil
		end

		if @cathode.update(update_params)
			redirect_to cathodes_path,
			notice: "Запись #{@cathode.number} изменена"
		else
			redirect_to edit_cathode_path,
			notice: @cathode.errors.first[1]
		end
	end

	def destroy
		del_cathode_from_resonator(@cathode.resonator_id)
		if @cathode.destroy
			redirect_to cathodes_path,
			notice: "Удалена запись #{@cathode.number}"
		else
			redirect_to cathodes_path,
			notice: "Не удалось удалить запись #{@cathode.number}"
		end
	end

	def show
	end

    private

    def set_cathode
        @cathode = Cathode.find(params[:id])
    end

    def del_cathode_from_resonator(id)
    	if resonator_id = @cathode.resonator_id
			resonator = Resonator.find(resonator_id)
			resonator.update cathode_id: nil
		end
    end


    def cathode_params
        params.require(:cathode).permit(
			:number,
			:date_of_manufacture,
			:manufacturer_supplier,
			:result_of_manufacturing_the_workpiece,
			:description_of_appearance,
			:oxidation_date,
			:oxidation_mode,
			:breakdown_voltage,
			:combustion_voltage,
			:tb_first_inclusion,
			:tb_subsequent_inclusions,
			:date_of_transfer_to_assembly_otk,
			:the_result_of_cathode_manufacturing,
			:reason_for_rejection,
			:date_of_transfer_to_assembly,
			:deviations,
			:resonator_id,
			:date_of_installation_in_the_resonator)





    end
end
