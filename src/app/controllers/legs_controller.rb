class LegsController < ApplicationController

    before_action :authenticate_user!
    skip_before_action :authenticate_user!, only:[:index, :search]

    before_action :set_step,
    only: [:new, :edit, :search]

    before_action :leg_list_cols, 
    only: [ :index, :new, :edit, :show, :search ]

    before_action :returns_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :element_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :parameter_list_cols,
    only: [ :index, :new, :edit, :search ]

    before_action :set_leg,
    only: [ :edit, :update, :destroy, :show]

    before_action :mode_list_cols,
    only: [ :new, :edit, :search]

    def index
        # Текущая страница
        if $lc_page == nil
            $lc_page = 1
        end

        if params[:page]
            $lc_page = params[:page]
        end

        @legs = Leg.order(disk_number: :desc).page($lc_page).per_page(25)
    end

    def new
        @leg = Leg.new
    end

    def search
        @search_val = params[:search_val]
        if @search_val
            @legs = Leg.where("disk_number LIKE ?", "%#{@search_val}%").order(disk_number: :desc).page(params[:page]).per_page(25)
        end
    end

    def create
        @leg = Leg.new(leg_params)
        @leg.xray_file = params[:file]
        if @leg.save
            redirect_to legs_path,
            notice: "Добавлена запись #{@leg.disk_number}"
        else
            redirect_to new_leg_path,
            notice: @leg.errors.first[1]
        end
    end

    def edit
    end

    def update
        if @leg.update(leg_params)
            redirect_to legs_path,
            notice: "Запись #{@leg.disk_number} изменена"
        else
            redirect_to edit_leg_path,
            notice: @leg.errors.first[1]
        end
    end

    def show
    end

    def destroy
        del_leg_from_resonator(@leg.resonator_id)
        if @leg.destroy
            redirect_to legs_path,
            notice: "Удалена запись #{@leg.disk_number}"
        else
            redirect_to legs_path,
            notice: "Не удалось удалить запись #{@leg.disk_number}"
        end
    end

    private

        def set_leg
            @leg = Leg.find(params[:id])
        end

        def del_leg_from_resonator(id)
            if resonator_id = @leg.resonator_id
                resonator = Resonator.find(resonator_id)
                resonator.update leg_id: nil
            end
        end

        def leg_params
            params.require(:leg).permit(
                :disk_number,
                :ds_date_of_manufacture,
                :ds_the_nonplaneity_of_the_disc_n,
                :ds_the_discontinuity_of_the_disk_n,
                :ds_appearance_ok,
                :ds_description_of_appearance_ok,
                :ds_date_control_ok,
                :sd_soldering_date,
                :sd_non_flatness_of_the_foot_n,
                :sd_the_non_flatness_of_the_foot_n,
                :sd_germ_checking_for_leaks,
                :sd_germ_date_control,
                :sd_appearance_ok,
                :sd_description_of_appearance_ok,
                :sd_date_control_ok,
                :date_of_transfer_to_the_assembly_in_the_resonator,
                :return_date,
                :reason_for_return,
                :for_the_return_list,
                :deviations,
                :number_of_assemblies,
                :resonator_id,
                :xray_file)
        end
end