class BadSensorsController < ApplicationController

    before_action :set_step,
    only: [ :show ]

    before_action :sensor_list_cols,
    only: [ :show ]

    before_action :returns_list_cols,
    only: [ :show ]

    before_action :element_list_cols,
    only: [ :show ]

    before_action :parameter_list_cols,
    only: [ :show ]

    def index
        @sensors = BadSensor.all
    end

    def show
        @sensor = BadSensor.find(params[:id])
        @histories = History.where(dev_id: @sensor.id)
    end

    def update
        @sensor = BadSensor.find(params[:id])
        redirect_to @sensor,
        notice: 'Нельзя редактировать брак'
    end

end
