class ResonatorsAnalyzesController < ApplicationController

  before_action :array_for_input_fields,
  only: [:chart_1_1, :chart_1_2, :chart_1_3, :chart_1_4, :chart_1_5,
          :chart_3_1, :chart_3_2, :chart_3_3, :chart_3_4, :chart_3_5,
          :chart_5_0, :chart_7_0, :chart_9_0, :chart_11_0, :chart_13_0,
          :chart_15_0, :chart_17_0, :chart_19_0, :chart_21_0, :chart_23_0,
          :chart_25_0, :chart_27_0]

  before_action :resonators_select,
  only: [:chart_1_1, :chart_1_2, :chart_1_3, :chart_1_4, :chart_1_5,
          :chart_3_1, :chart_3_2, :chart_3_3, :chart_3_4, :chart_3_5,
          :chart_5_0, :chart_7_0, :chart_9_0, :chart_11_0, :chart_13_0,
          :chart_15_0, :chart_17_0, :chart_19_0, :chart_21_0, :chart_23_0,
          :chart_25_0, :chart_27_0]

  def chart_1_1
    @title = 'В начале тренировки и в конце стабилизации оп.70'
    @legend_1 = 'В начале тренировки'
    @legend_2 = 'Оп.70'
    @download_name_1 = 'Распределение_1.1'
    @download_name_2 = 'Относительное распределение_2.1'
    @xtitle = 'Пороговый ток, мА'
    @ytitle = 'Число приборов, шт'

    ch_1 =
    @resonators.
    where.not(ev_threshold_current_at_the_beginning_of_the_workout: nil).
    map{ |e| e.ev_threshold_current_at_the_beginning_of_the_workout.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 =
    @resonators.
    where.not(ev_threshold_current_at_the_end_of_stabilization: nil).
    map{ |e| e.ev_threshold_current_at_the_end_of_stabilization.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_1_2
    @title = 'В конце стабилизации оп.70 и после холодоустойчивости оп.105'
    @legend_1 = 'оп.70'
    @legend_2 = 'оп.105'
    @download_name_1 = 'Распределение_1.2'
    @download_name_2 = 'Относительное распределение_2.2'
    @xtitle = 'Пороговый ток, мА'
    @ytitle = 'Число приборов, шт'

    ch_1 =
    @resonators.
    where.not(ev_threshold_current_at_the_end_of_stabilization: nil).
    map{ |e| e.ev_threshold_current_at_the_end_of_stabilization.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 =
    @resonators.
    where.not(cr_threshold_current_after_XY: nil).
    map{ |e| e.cr_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_1_3
    @title = 'После холодоустойчивости оп.105 и после приработки оп.115'
    @legend_1 = 'оп.105'
    @legend_2 = 'оп.115'
    @download_name_1 = 'Распределение_1.3'
    @download_name_2 = 'Относительное распределение_2.3'
    @xtitle = 'Пороговый ток, мА'
    @ytitle = 'Число приборов, шт'

    ch_1 =
    @resonators.
    where.not(cr_threshold_current_after_XY: nil).
    map{ |e| e.cr_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 =
    @resonators.
    where.not(ri_threshold_current_after_XY: nil).
    map{ |e| e.ri_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_1_4
    @title = 'После приработки оп.115 и проверочной оп.175'
    @legend_1 = 'оп.115'
    @legend_2 = 'оп.175'
    @download_name_1 = 'Распределение_1.4'
    @download_name_2 = 'Относительное распределение_2.4'
    @xtitle = 'Пороговый ток, мА'
    @ytitle = 'Число приборов, шт'

    ch_1 =
    @resonators.
    where.not(ri_threshold_current_after_XY: nil).
    map{ |e| e.ri_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 =
    @resonators.
    where.not(t9_threshold_current: nil).
    map{ |e| e.t9_threshold_current.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_1_5
    @title = 'На всех стадиях'
    @legend_1 = 'В начале тренировки'
    @legend_2 = 'оп.70'
    @legend_3 = 'оп.105'
    @legend_4 = 'оп.115'
    @legend_5 = 'оп.175'

    @download_name_1 = 'Распределение_1.5'
    @download_name_2 = 'Относительное распределение_2.5'
    @xtitle = 'Пороговый ток, мА'
    @ytitle = 'Число приборов, шт'

    # В начале тренировки
    ch_1 = @resonators.
    where.not(ev_threshold_current_at_the_beginning_of_the_workout: nil).
    map{ |e| e.ev_threshold_current_at_the_beginning_of_the_workout.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    # В конце стабилизации оп.70
    ch_2 = @resonators.
    where.not(ev_threshold_current_at_the_end_of_stabilization: nil).
    map{ |e| e.ev_threshold_current_at_the_end_of_stabilization.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    # После холодоустойчивости оп.105
    ch_3 = @resonators.
    where.not(cr_threshold_current_after_XY: nil).
    map{ |e| e.cr_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    # После приработки оп.115
    ch_4 = @resonators.
    where.not(ri_threshold_current_after_XY: nil).
    map{ |e| e.ri_threshold_current_after_XY.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    # После проверочной оп.175
    ch_5 = @resonators.
    where.not(t9_threshold_current: nil).
    map{ |e| e.t9_threshold_current.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3, ch_4, ch_5)
  end

  def chart_3_1
    @title = 'Интервалы разностей в начале тренировки и в конце стабилизации оп.70'

    ch_1 = @resonators.
    map{ |e| (e.ev_threshold_current_at_the_end_of_stabilization.to_f -
              e.ev_threshold_current_at_the_beginning_of_the_workout.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_3_2
    @title = 'Интервалы разностей в конце стабилизации оп.70 и после холодоустойчивости оп.105'

    ch_1 = @resonators.
    map{ |e| (e.cr_threshold_current_after_XY.to_f -
              e.ev_threshold_current_at_the_end_of_stabilization.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_3_3
    @title = 'Интервалы разностей после холодоустойчивости оп.105 и после приработки оп.115'

    ch_1 = @resonators.
    map{ |e| (e.ri_threshold_current_after_XY.to_f -
              e.cr_threshold_current_after_XY.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_3_4
    @title = 'Интервалы разностей после приработки оп.115 и проверочной оп.175'

    ch_1 = @resonators.
    map{ |e| (e.t9_threshold_current.to_f -
              e.ri_threshold_current_after_XY.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_3_5
    @title = 'Интервалы разностей на всех стадиях'
    @legend_1 = 'В начале тренировки и оп.70'
    @legend_2 = 'оп.70 и оп.105'
    @legend_3 = 'оп.105 и оп.115'
    @legend_4 = 'оп.115 и оп.175'

    ch_1 = @resonators.
    map{ |e| (e.ev_threshold_current_at_the_end_of_stabilization.to_f -
              e.ev_threshold_current_at_the_beginning_of_the_workout.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    map{ |e| (e.cr_threshold_current_after_XY.to_f -
     e.ev_threshold_current_at_the_end_of_stabilization.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_3 = @resonators.
    map{ |e| (e.ri_threshold_current_after_XY.to_f -
     e.cr_threshold_current_after_XY.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_4 = @resonators.
    map{ |e| (e.t9_threshold_current.to_f -
              e.ri_threshold_current_after_XY.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3, ch_4)
  end

  def chart_5_0
    @title = 'По интервалам потерь после сборки и юстировки оп.065, проверочной оп.075, и проверочной оп.175'
    @legend_1 = 'оп.065'
    @legend_2 = 'оп.075'
    @legend_3 = 'оп.175'

    ch_1 = @resonators.
    where.not(al_TEM00_loss: nil).
    map{ |e| e.al_TEM00_loss.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    where.not(t3_losses: nil).
    map{ |e| e.t3_losses.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_3 = @resonators.
    where.not(t9_losses: nil).
    map{ |e| e.t9_losses.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3)
  end

  def chart_7_0
    @title = 'По интервалам селективности после сборки и юстировки оп.065, проверочной оп.075, и проверочной оп.175'
    @legend_1 = 'оп.065'
    @legend_2 = 'оп.075'
    @legend_3 = 'оп.175'

    ch_1 = @resonators.
    where.not(al_selectivity: nil).
    map{ |e| e.al_selectivity.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    where.not(t3_selectivity: nil).
    map{ |e| e.t3_selectivity.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_3 = @resonators.
    where.not(t9_selectivity: nil).
    map{ |e| e.t9_selectivity.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3)
  end

  def chart_9_0
    @title = 'По интервалам разности потерь после сборки и юстировки оп.065 и проверочной оп.075'

    ch_1 = @resonators.
    map{ |e| (e.t3_losses.to_f -
              e.al_TEM00_loss.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_11_0
    @title = 'По интервалам разности потерь и разности селективности между проверочными оп.175 и оп.075'
    @legend_1 = 'Разность потерь'
    @legend_2 = 'Разность селективности'

    ch_1 = @resonators.
    map{ |e| (e.t9_losses.to_f -
              e.t3_losses.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    map{ |e| (e.t9_selectivity.to_f -
              e.t3_selectivity.to_f).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_13_0
    @title = 'По интервалам захвата(среднее по модам), измеренным на оп.105, 115, 175'
    @legend_1 = 'Оп.105'
    @legend_2 = 'Оп.115'
    @legend_3 = 'Оп.175'

    ch_1 = @resonators.
    map{ |e|((e.cr_capture_in_fashion_1.to_f +
              e.cr_capture_on_fashion_2.to_f +
              e.cr_capture_in_fashion_3.to_f +
              e.cr_capture_on_fashion_4.to_f) / 4 ).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    map{ |e|((e.ri_capture_in_fashion_1.to_f +
              e.ri_capture_on_fashion_2.to_f +
              e.ri_capture_in_fashion_3.to_f +
              e.ri_capture_on_fashion_4.to_f) / 4 ).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_3 = @resonators.
    where.not(t9_omega: nil).
    map{ |e| e.t9_omega.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3)
  end

  def chart_15_0
    @title = 'По интервалам разностей захвата(среднее по модам), измеренным на оп.115, 105'

    ch_1 = @resonators.
    map{ |e|((e.ri_capture_in_fashion_1.to_f +
              e.ri_capture_on_fashion_2.to_f +
              e.ri_capture_in_fashion_3.to_f +
              e.ri_capture_on_fashion_4.to_f) / 4 -
             (e.cr_capture_in_fashion_1.to_f +
              e.cr_capture_on_fashion_2.to_f +
              e.cr_capture_in_fashion_3.to_f +
              e.cr_capture_on_fashion_4.to_f) / 4).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_17_0
    @title = 'По интервалам разностей захвата(среднее по модам), измеренным на оп.175, 115'

    ch_1 = @resonators.
    map{ |e|( e.t9_omega.to_f -
            ( e.cr_capture_in_fashion_1.to_f +
              e.cr_capture_on_fashion_2.to_f +
              e.cr_capture_in_fashion_3.to_f +
              e.cr_capture_on_fashion_4.to_f ) / 4 ).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_19_0
    @title = 'По интервалам захвата(максимальному), измеренным на оп.105, 115, 175'
    @legend_1 = 'Оп.105'
    @legend_2 = 'Оп.115'
    @legend_3 = 'Оп.175'

    ch_1 = @resonators.
    map{ |e| [e.cr_capture_in_fashion_1.to_f,
              e.cr_capture_on_fashion_2.to_f,
              e.cr_capture_in_fashion_3.to_f,
              e.cr_capture_on_fashion_4.to_f].max.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    map{ |e| [e.ri_capture_in_fashion_1.to_f,
              e.ri_capture_on_fashion_2.to_f,
              e.ri_capture_in_fashion_3.to_f,
              e.ri_capture_on_fashion_4.to_f].max.round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_3 = @resonators.
    where.not(t9_omega: nil).
    group(:t9_omega).
    count.to_a

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2, ch_3)
  end

  def chart_21_0
    @title = 'По интервалам разностей захвата(максимальному), измеренным на оп.115, 105'

    ch_1 = @resonators.
    map{ |e|([e.ri_capture_in_fashion_1.to_f,
              e.ri_capture_on_fashion_2.to_f,
              e.ri_capture_in_fashion_3.to_f,
              e.ri_capture_on_fashion_4.to_f].max -
             [e.cr_capture_in_fashion_1.to_f,
              e.cr_capture_on_fashion_2.to_f,
              e.cr_capture_in_fashion_3.to_f,
              e.cr_capture_on_fashion_4.to_f].max).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_23_0
    @title = 'По интервалам разностей захвата(максимальное по модам), измеренным на оп.175, 115'

    ch_1 = @resonators.
    map{ |e|( e.t9_omega.to_f -
            [ e.cr_capture_in_fashion_1.to_f,
              e.cr_capture_on_fashion_2.to_f,
              e.cr_capture_in_fashion_3.to_f,
              e.cr_capture_on_fashion_4.to_f].max).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  def chart_25_0
    @title = 'По интервалам КНУ (захв.), замеренным на оп.105 и 115'
    @legend_1 = 'оп.105'
    @legend_2 = 'оп.115'

    ch_1 = @resonators.
    map{ |e|((e.cr_output_frequency_in_mode_1.to_f +
              e.cr_output_frequency_in_mode_2.to_f +
              e.cr_output_frequency_in_mode_3.to_f +
              e.cr_output_frequency_in_mode_4.to_f) / 4 ).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    ch_2 = @resonators.
    map{ |e|((e.ri_output_frequency_in_mode_1.to_f +
              e.ri_output_frequency_in_mode_1.to_f +
              e.ri_output_frequency_in_mode_1.to_f +
              e.ri_output_frequency_in_mode_1.to_f) / 4 ).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1, ch_2)
  end

  def chart_27_0
    @title = 'По интервалам разностей КНУ (захв.), замеренным на оп.115 и 105'

    ch_1 = @resonators.
    map{ |e|((e.ri_output_frequency_in_mode_1.to_f +
              e.ri_output_frequency_in_mode_2.to_f +
              e.ri_output_frequency_in_mode_3.to_f +
              e.ri_output_frequency_in_mode_4.to_f) / 4 -
             (e.cr_output_frequency_in_mode_1.to_f +
              e.cr_output_frequency_in_mode_2.to_f +
              e.cr_output_frequency_in_mode_3.to_f +
              e.cr_output_frequency_in_mode_4.to_f) / 4).round params[:round].to_i }.
    group_by{ |e| e }.map{ |key, val| [key, val.count] }

    @chart_distribution =
    hash_charts(params[:x_in], params[:x_out], @resonators.count, ch_1)
  end

  private
  def array_for_input_fields
    # Список резонаторов
    @resonators_array = Resonator.all.map{ |e|  e.number }

    # Список изготовителей(поставщиков) катодов
    @manufacturer_array = Cathode.all.where.not(manufacturer_supplier: '').
    select(:manufacturer_supplier).distinct.map{ |e| e.manufacturer_supplier }

    # Список режимов окисления катодов
    @oxidation_array = Cathode.all.where.not(oxidation_mode: '')
    .select(:oxidation_mode).distinct.map{ |e| e.oxidation_mode }

    # Список катодов
    @cathodes_array = Cathode.all.map{ |e|  e.number}

    # Список ног
    @legs_array = Leg.all.map{ |e|  e.disk_number}

    # Список зеркал, Только годные
    @mirrors_array = Mirror.all.
    where("result_of_polishing = ? OR result_of_polishing = ?
          OR appearance = ?", nil, "Годен", "Годен").
    map{ |e|  e.number_of_substrate}
  end


  def resonators_select
    # Выборка резонаторов для анализа
    @resonators =
    resonators_collect( Resonator.all, params[:number],
                        params[:date_in], params[:date_out],
                        params[:manufacturer], params[:oxidation],
                        params[:cathodes], params[:legs], params[:mirrors] )
  end

  # Возвращает период с date_in до date_out
  def date_conversion_in_the_period date_in, date_out
    if date_in.length > 0 && date_out.length > 0
      year = date_in[0..3].to_i
      month = date_in[5..6].to_i
      date_in = Date.civil(year, month, 1)
      year = date_out[0..3].to_i
      month = date_out[5..6].to_i
      date_out = Date.civil(year, month, 1)

      date_in..date_out
    else
      Date.current - 10.year..Date.current
    end
  end

  # Возвращает список номеров резонаторов
  def resonators_in_the_list resonators
    resonators ? Resonator.where(number: resonators).select(:number) :
    Resonator.all.select(:number)
  end

  # Возвращает список резонаторов, по производителю катода
  def cathodes_to_the_list_by_manufacturer cathode_manufacturer
    cathode_manufacturer ?
    Cathode.where(manufacturer_supplier: cathode_manufacturer).ids :
    Cathode.all.ids
  end

  # Возвращает список резонаторов, по режиму окисления
  def list_of_cathodes_by_oxidation_regime oxidation
    oxidation ?
    Cathode.where(oxidation_mode: oxidation).ids : Cathode.all.ids
  end

  # Возвращает список резонаторов, по установленным катодам
  def cathodes_in_the_list cathodes
    cathodes ? Cathode.where(number: cathodes).ids : Cathode.all.ids
  end

  # Возвращает список резонаторов, по установленным ногам
  def legs_in_the_list legs
    legs ? Leg.where(disk_number: legs).ids : Leg.all.ids
  end

  # Возвращает список резонаторов, по установленным зеркалам
  def mirrors_in_the_list mirrors
    mirrors ? Mirror.where(number_of_substrate: mirrors).ids
    : Mirror.all.ids
  end

  # Вернет список резонаторов, по всем условиям запросов
  def resonators_collect( model_list, resonators = 0, date_in = 0, date_out = 0,
                          manufacturer_ids = 0, oxidation_ids = 0, cathodes = 0,
                          legs = 0, mirrors = 0 )
    if model_list.exists?
      date_interval = date_conversion_in_the_period date_in, date_out
      resonators_list = resonators_in_the_list resonators
      manufacturer_list_ids = cathodes_to_the_list_by_manufacturer manufacturer_ids
      oxidation_list_ids = list_of_cathodes_by_oxidation_regime oxidation_ids
      cathode_list = cathodes_in_the_list cathodes
      legs_list = legs_in_the_list legs
      mirrors_list = mirrors_in_the_list mirrors

      Resonator.all.
      where(al_date: date_interval).
      where(number: resonators_list).
      where(cathode_id: manufacturer_list_ids).
      where(cathode_id: oxidation_list_ids).
      where(cathode_id: cathode_list).
      where(cathode_id: legs_list).
      where(cathode_id: mirrors_list)
    end
  end

  # Возвращает хеш, в заданном интервале
  def axis_h(hsh_chart, from, to)
    if hsh_chart
      axis_i = from.length > 0 && to.length > 0 ? [from, to] : [-999999, 999999]
      hsh_chart.select{ |key, val| key >= axis_i.first.to_f && key <= axis_i.last.to_f }
    else nil
    end
  end

  def add_charts( chart1, chart2 = nil, chart3 = nil, chart4 = nil, chart5 = nil )
    temp  = chart1
    temp  = (chart1 - chart2) + (chart2 - chart1) if chart2
    temp += (chart1 - chart3) + (chart3 - chart1) if chart3
    temp += (chart1 - chart4) + (chart4 - chart1) if chart4
    temp += (chart1 - chart5) + (chart5 - chart1) if chart5
    temp
  end

  def add_zero( chart1, chart2 = nil, chart3 = nil, chart4 = nil, chart5 = nil )
    chart1_key = chart1.map{ |key, val| key }
    chart2_key = chart2.map{ |key, val| key } if chart2
    chart3_key = chart3.map{ |key, val| key } if chart3
    chart4_key = chart4.map{ |key, val| key } if chart4
    chart5_key = chart5.map{ |key, val| key } if chart5

    add_collect = []
    add_collect += add_charts(chart2_key, chart1_key) if chart2
    add_collect += add_charts(chart3_key, chart2_key, chart1_key) if chart3
    add_collect += add_charts(chart4_key, chart3_key, chart2_key, chart1_key) if chart4
    add_collect += add_charts(chart5_key, chart4_key, chart3_key, chart2_key, chart1_key) if chart5

    if chart2
      add_collect = add_collect.uniq
      chart1 = (chart1 + (add_collect - chart1_key).map{ |e| [e, 0] })
      chart2 = (chart2 + (add_collect - chart2_key).map{ |e| [e, 0] }).sort{ |x,y| x<=>y }
    end

    chart3 = (chart3 + (add_collect - chart3_key).map{ |e| [e, 0] }).sort{ |x,y| x<=>y } if chart3
    chart4 = (chart4 + (add_collect - chart4_key).map{ |e| [e, 0] }).sort{ |x,y| x<=>y } if chart4
    chart5 = (chart5 + (add_collect - chart5_key).map{ |e| [e, 0] }).sort{ |x,y| x<=>y } if chart5

    [chart1.sort{ |x,y| x<=>y }, chart2, chart3, chart4, chart5].compact
  end

  def hash_charts( from, to, count, chart1, chart2 = nil, chart3 = nil, chart4 = nil, chart5 = nil )
    chart1 = axis_h(chart1, from, to)
    chart2 = axis_h(chart2, from, to) if chart2
    chart3 = axis_h(chart3, from, to) if chart3
    chart4 = axis_h(chart4, from, to) if chart4
    chart5 = axis_h(chart5, from, to) if chart5

    corrected = add_zero(chart1, chart2, chart3, chart4, chart5)

    chart1_rel = to_relative(corrected.at(0), count)
    chart2_rel = to_relative(corrected.at(1), count) if chart2
    chart3_rel = to_relative(corrected.at(2), count) if chart3
    chart4_rel = to_relative(corrected.at(3), count) if chart4
    chart5_rel = to_relative(corrected.at(4), count) if chart5

    [corrected.at(0), corrected.at(1), corrected.at(2),
      corrected.at(3), corrected.at(4), chart1_rel, chart2_rel,
      chart3_rel, chart4_rel, chart5_rel, count
    ].compact
  end

  def to_relative(chart, count)
    return chart.map{ |key, val| [key, val * 100 / count] }
  end
end
