class BadResonatorsController < ApplicationController

	before_action :set_step,
	only: [ :show ]

	before_action :resonator_list_cols,
	only: [ :show ]

	before_action :returns_list_cols,
	only: [ :show ]

	before_action :element_list_cols,
	only: [ :show ]

	before_action :parameter_list_cols,
	only: [ :show ]

	def show
		@resonator = BadResonator.find(params[:id])

		@nitro3 = (@resonator.t3_nitrogen_level.to_i * 10 ** @resonator.t3_nitrogen_level_rate.to_i).to_f
	 	@nitro5 = (@resonator.cr_nitrogen_level.to_i * 10 ** @resonator.cr_nitrogen_level_rate.to_i).to_f
	 	@nitro9 = (@resonator.t9_nitrogen_level.to_i * 10 ** @resonator.t9_nitrogen_level_rate.to_i).to_f

		@histories = History.where(dev_id: @resonator.id)

	end

	def update
		@resonator = BadResonator.find(params[:id])
		redirect_to @resonator,
		notice: 'Нельзя редактировать брак'
	end

end
