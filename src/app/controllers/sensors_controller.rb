class SensorsController < ApplicationController

	before_action :authenticate_user!
	skip_before_action :authenticate_user!, only: [:index, :show,
		:show_coil_winding, :show_align_prisms_and_fpu,
		:show_assembly_and_wiring, :show_checking_the_build_quality_in_screens,
		:show_check_for_resistance_to_sinusoidal_vibration, :show_psi, :search ]

	before_action :set_step,
	only: [
		:set_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration,
		:set_psi ]

	before_action :sensor_list_cols,
	only: [
		:search, :ok_sensor_filter,
		:index, :new, :edit, :show,
		:set_align_prisms_and_fpu, :show_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens, :show_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration, :show_check_for_resistance_to_sinusoidal_vibration,
		:set_check_by_pm, :show_check_by_pm,
		:set_psi, :show_psi ]

	before_action :set_sensor,
	only: [
		:show, :edit, :update, :destroy,
		:set_align_prisms_and_fpu, :show_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens, :show_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration, :show_check_for_resistance_to_sinusoidal_vibration,
		:set_check_by_pm, :show_check_by_pm,
		:set_psi, :show_psi ]

	before_action :returns_list_cols,
	only: [
		:show,
		:search, :ok_sensor_filter,
		:set_align_prisms_and_fpu, :show_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens, :show_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration, :show_check_for_resistance_to_sinusoidal_vibration,
		:set_check_by_pm, :show_check_by_pm,
		:set_psi, :show_psi	]

	before_action :element_list_cols,
	only: [
		:search, :ok_sensor_filter,
		:set_align_prisms_and_fpu, :show_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens, :show_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration, :show_check_for_resistance_to_sinusoidal_vibration,
		:set_check_by_pm, :show_check_by_pm,
		:set_psi, :show_psi ]

	before_action :parameter_list_cols,
	only: [
		:search, :ok_sensor_filter,
		:set_align_prisms_and_fpu, :show_align_prisms_and_fpu,
		:set_checking_the_build_quality_in_screens, :show_checking_the_build_quality_in_screens,
		:set_check_for_resistance_to_sinusoidal_vibration, :show_check_for_resistance_to_sinusoidal_vibration,
		:set_check_by_pm, :show_check_by_pm,
		:set_psi, :show_psi ]

	# Возврат из не годных.
	# Пока убрал ибо валидация не пропускает создание записи без прикрепленного резонатора
	def roll_back_bad
		if BadSensor.where params[:id]
			@bad_sensor = BadSensor.find(params[:id])

			if @bad_sensor.ap_result == 'Возврат'
				@bad_sensor.update ap_result: nil
				@bad_sensor.update ap_return_date: nil
				@bad_sensor.update ap_reason_for_return: nil
				@bad_sensor.update ap_for_the_return_list: nil
				@bad_sensor.update ap_refund_description: nil
			end

			if @bad_sensor.cb_result == 'Возврат'
				@bad_sensor.update cb_result: nil
				@bad_sensor.update cb_return_date: nil
				@bad_sensor.update cb_reason_for_return: nil
				@bad_sensor.update cb_for_the_return_list: nil
				@bad_sensor.update cb_refund_description: nil
			end

			if @bad_sensor.cf_result == 'Возврат'
				@bad_sensor.update cf_result: nil
				@bad_sensor.update cf_return_date: nil
				@bad_sensor.update cf_reason_for_return: nil
				@bad_sensor.update cf_for_the_return_list: nil
				@bad_sensor.update cf_refund_description: nil
			end

			if @bad_sensor.cp_result == 'Возврат'
				@bad_sensor.update cp_result: nil
				@bad_sensor.update cp_return_date: nil
				@bad_sensor.update cp_reason_for_return: nil
				@bad_sensor.update cp_for_the_return_list: nil
				@bad_sensor.update cp_refund_description: nil
			end

			if @bad_sensor.ps_result == 'Возврат' or
				@bad_sensor.update ps_result: nil
				@bad_sensor.update ps_return_date: nil
				@bad_sensor.update ps_reason_for_return: nil
				@bad_sensor.update ps_for_the_return_list: nil
				@bad_sensor.update ps_refund_description: nil
			end

		end

		# Далее переместим запись из не годных в годные

		# Создаю запись в таблице нормальных датчиков
		@sensor = Sensor.create(@bad_sensor.attributes)
		@bad_sensor.destroy

		set_history("Возвращено из брака")

		redirect_to sensors_path

	end

	def search
		@search_val = params[:search_val]
		if Sensor.where("number LIKE ?", "%#{@search_val}%")
			@sensors = Sensor.where("number LIKE ?", "%#{@search_val}%")
		end

		if BadSensor.where("number LIKE ?", "%#{@search_val}%")
			@bad_sensors = BadSensor.where("number LIKE ?", "%#{@search_val}%")
		end
	end

	def ok_sensor_filter
		@date = Date.civil(params[:date][:year].to_i, params[:date][:month].to_i)
		@date_interval = @date .. @date + (1.month - 1.day)
		@sensors = Sensor.where(psi: 'Завершена').where(ps_date_of_psi: @date_interval).order(ps_date_of_psi: :desc).page(params[:page]).per_page(20)
	end

	def index
		@work_sensor = Sensor.where.not(suitable: true)
		@good_sensor = Sensor.where(suitable: true)
		@current_title = "В производстве"
		$rv_type = true

		case params[:view_type]
		when 'good'
			@sensors = @good_sensor.order(created_at: :desc).page(params[:page]).per_page(20)
			@current_title = "Годные"
			$rv_type = true

		when 'bad'
			@sensors = BadSensor.all.order(created_at: :desc).page(params[:page]).per_page(20)
			@current_title = "Не годные"
			$rv_type = false
		else
			@sensors = @work_sensor.order(created_at: :desc).page(params[:page]).per_page(999)
		end

		respond_to do |format|
			format.html
			#format.csv { send_data @sensors.to_csv }
			#format.xls # { send_data @sensors.to_csv(col_sep: "\t") }
		end
	end

	def new
		@sensor = Sensor.new
	end

	def create
		@sensor = Sensor.new(sensor_params)

		if @sensor.save
			end_count(@sensor)

			# Установить связь с резонатором
			if @sensor.resonator_id?
				set_sensor_in_resonator(@sensor)
			end


			set_history("Запись создана")

			redirect_to @sensor,
			notice: "Добавлена запись #{@sensor.number}"
		else
			redirect_to new_sensor_path,
			notice: @sensor.errors.first[1]
		end
	end

	def edit
	end

	def show
		@histories = History.where(dev_id: @sensor.id)
	end

	def update

		if @sensor.update(sensor_params)

			end_count(@sensor)

			if @sensor.resonator_id?
				set_sensor_in_resonator(@sensor)
			end

			check_the_status_of_marriage(@sensor)

			set_history("Сведения обновлены")

			redirect_to sensor_path,
            notice: "Запись #{@sensor.number} изменена"
		else
			redirect_to edit_sensor_path,
			notice: @sensor.errors.first[1]
		end
	end

	def destroy
		del_sensor_from_resonator(@sensor)

		# Удаляю историю
		if History.where dev_id: @sensor.id
			@history = History.where dev_id: @sensor.id

			@history.each do |h|
				h.destroy
			end
		end

		if @sensor.destroy
			redirect_to sensors_path,
            notice: "Удалена запись #{@sensor.number}"
		else
			redirect_to sensors_path,
            notice: "Не удалось удалить запись #{@sensor.number}"
		end
	end

	private

		def end_count(sensor)

			count = 0

			if sensor.align_prisms_and_fpu == 'Завершена'
			    count += 1
			end

			if sensor.checking_the_build_quality_in_screens == 'Завершена'
			    count += 1
			end

			if sensor.check_for_resistance_to_sinusoidal_vibration == 'Завершена'
			    count += 1
			end

			if sensor.check_by_pm == 'Завершена'
			    count += 1
			end

			if sensor.psi == 'Завершена'
			    count += 1
			end

			sensor.update end_count: count

			if count == 5
			    sensor.update suitable: true
			else
			    sensor.update suitable: false
			end

		end

		# Проверка стаутса брака
		def check_the_status_of_marriage(sensor)

			if 	sensor.ap_result == 'Возврат' or
        		sensor.cb_result == 'Возврат' or
        		sensor.cf_result == 'Возврат' or
        		sensor.cp_result == 'Возврат' or
        		sensor.ps_result == 'Возврат'

				if Resonator.find_by id: sensor.resonator_id

					# Убираем иформацию о датчике из резонатора
					resonator = Resonator.find(sensor.resonator_id)
					resonator.update sensor_id: nil

					# Убираем информацию о резонаторе из датчика
					sensor.update resonator_id: nil

				end

				# Создаю запись в таблице испорченных резонаторов
				bad_sensor = BadSensor.create(sensor.attributes)
				set_history("Направлено в брак")

				# Удаляю текущую запись из таблицы годных резонаторов
				sensor.destroy

				sensor = bad_sensor

			end
		end

		# Записать в историю
		def set_history(operation)
			History.create(dev: 'sensor', dev_id: @sensor.id, operation: operation, operator: 'noname')
		end

		def set_sensor
			if Sensor.find_by(id: params[:id])
				@sensor = Sensor.find(params[:id])
			elsif BadSensor.find_by(id: params[:id])
				@sensor = BadSensor.find(params[:id])
			else
				@sensor = Sensor.last
		end


		end

		def set_sensor_in_resonator(sensor)
			if Resonator.find_by id: sensor.resonator_id
				resonator = Resonator.find(sensor.resonator_id)
				resonator.update sensor_id: sensor.id
			end
		end


		# Удаление информации о датчике из резонатора
		def del_sensor_from_resonator(sensor)
			if Resonator.find_by id: sensor.resonator_id
				resonator = Resonator.find(sensor.resonator_id)
				resonator.update sensor_id: nil
			end
		end

		def sensor_params
			params.require(:sensor).permit(
			:number,
			:resonator_id,
			:date_of_manufacture_of_the_resonator,

			:align_prisms_and_fpu,
			:ap_date,
			:ap_the_combustion_voltage_in_2_sec,
			:ap_the_combustion_voltage_after_35_sec,
			:ap_the_combustion_voltage_after_10_min,
			:ap_the_transmission_coefficient_pb1,
			:ap_the_transmission_coefficient_pb2,
			:ap_result,
			:ap_return_date,
			:ap_reason_for_return,
			:ap_for_the_return_list,
			:ap_refund_description,

			:checking_the_build_quality_in_screens,
			:cb_date,
			:cb_the_combustion_voltage_in_2_sec,
			:cb_the_combustion_voltage_after_35_sec,
			:cb_the_combustion_voltage_after_10_min,
			:cb_the_amplitude_of_the_output_signal_1_in_the_mode_plus,
			:cb_the_amplitude_of_the_output_signal_2_in_the_mode_plus,
			:cb_the_amplitude_of_the_output_signal_1_in_the_mode_minus,
			:cb_the_amplitude_of_the_output_signal_2_in_the_mode_minus,
			:cb_the_phase_difference_of_the_output_signals,
			:cb_the_transmission_coefficient_of_the_modes_in_the_mode_plus,
			:cb_the_transmission_coefficient_of_the_modes_in_the_mode_minus,
			:cb_the_transmission_coefficient,
			:cb_the_amplitude_of_the_perimeter_signal_of_the_mode_plus,
			:cb_the_amplitude_of_the_perimeter_signal_of_the_mode_minus,
			:cb_result,
			:cb_return_date,
			:cb_reason_for_return,
			:cb_for_the_return_list,
			:cb_refund_description,

			:check_for_resistance_to_sinusoidal_vibration,
			:cf_date,
			:cf_result,
			:cf_return_date,
			:cf_reason_for_return,
			:cf_for_the_return_list,
			:cf_refund_description,

	      	:check_by_pm,
	      	:cp_date,
			:cp_result,
			:cp_return_date,
			:cp_reason_for_return,
			:cp_for_the_return_list,
			:cp_refund_description,

			:psi,
			:ps_date,
			:ps_the_amplitude_of_the_output_signal_2_in_the_operating_mode,
			:ps_the_amplitude_of_the_output_signal_1_in_the_working_mode,
			:ps_the_transmission_coefficient_of_the_ou_in_the_working_mode,
			:ps_combustion_voltage,
			:ps_result,
			:ps_return_date,
			:ps_reason_for_return,
			:ps_for_the_return_list,
			:ps_refund_description,
			:remark)
		end
end
