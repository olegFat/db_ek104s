class ResonatorsController < ApplicationController

	before_action :authenticate_user!

	skip_before_action :authenticate_user!, only:[ :index, :show,
		:show_alignment,
		:show_evo,
		:show_thermostabilization,
		:show_cold_resistance,
		:show_running_in,
		:show_sealing,
		:show_heat_treatment_in_an_argon_chamber,
		:show_varnish_coating,
		:show_metallization_of_mirrors,
		:show_tinning_mirrors,
		:show_test3,
		:show_test9,
		:show_control,
		:search,
		:graphics_loss,
		:graphics_capture,
		:graphics_from_the_burning_voltage_after_10_min,
		:graphics_burning_voltage_after_10_min,
		:bad_gist ]

	before_action :set_step,
	only: [
		:set_alignment,
		:set_evo,
		:set_thermostabilization,
		:set_cold_resistance,
		:set_running_in,
		:set_sealing,
		:set_heat_treatment_in_an_argon_chamber,
		:set_tinning_mirrors,
		:set_test3,
		:show_test3,
		:set_test9,
		:show_test9 ]

	before_action :resonator_list_cols,
	only: [
		:search,
		:index, :new, :show, :edit,
		:set_alignment, :show_alignment,
		:set_evo, :show_evo,
		:set_thermostabilization, :show_thermostabilization,
		:set_cold_resistance, :show_cold_resistance,
		:set_running_in, :show_running_in,
		:set_sealing, :show_sealing,
		:set_heat_treatment_in_an_argon_chamber, :show_heat_treatment_in_an_argon_chamber,
		:set_varnish_coating, :show_varnish_coating,
		:set_metallization_of_mirrors, :show_metallization_of_mirrors,
		:set_tinning_mirrors, :show_tinning_mirrors,
		:set_test3, :show_test3,
		:set_test9, :show_test9,
		:set_control, :show_control ]

	before_action :returns_list_cols,
	only: [
		:search, :show,
		:set_alignment, :show_alignment,
		:set_evo, :show_evo,
		:set_test3, :show_test3,
		:set_test9, :show_test9,
		:set_thermostabilization, :show_thermostabilization,
		:set_cold_resistance, :show_cold_resistance,
		:set_running_in, :show_running_in,
		:set_sealing, :show_sealing,
		:set_heat_treatment_in_an_argon_chamber, :show_heat_treatment_in_an_argon_chamber,
		:set_varnish_coating, :show_varnish_coating,
		:set_metallization_of_mirrors, :show_metallization_of_mirrors,
		:set_tinning_mirrors, :show_tinning_mirrors,
		:set_control, :show_control ]

	before_action :element_list_cols,
	only: [
		:search, :show,
		:set_alignment, :show_alignment,
		:set_evo, :show_evo,
		:set_thermostabilization, :show_thermostabilization,
		:set_cold_resistance, :show_cold_resistance,
		:set_running_in, :show_running_in,
		:set_sealing, :show_sealing,
		:set_heat_treatment_in_an_argon_chamber, :show_heat_treatment_in_an_argon_chamber,
		:set_varnish_coating, :show_varnish_coating,
		:set_metallization_of_mirrors, :show_metallization_of_mirrors,
		:set_tinning_mirrors, :show_tinning_mirrors ]

	before_action :parameter_list_cols,
	only: [
		:search, :show,
		:set_alignment, :show_alignment,
		:set_evo, :show_evo,
		:set_thermostabilization, :show_thermostabilization,
		:set_cold_resistance, :show_cold_resistance,
		:set_running_in, :show_running_in,
		:set_sealing, :show_sealing,
		:set_heat_treatment_in_an_argon_chamber, :show_heat_treatment_in_an_argon_chamber,
		:set_varnish_coating, :show_varnish_coating,
		:set_metallization_of_mirrors, :show_metallization_of_mirrors,
		:set_tinning_mirrors, :show_tinning_mirrors ]

	before_action :set_resonator,
	only: [
		:set_fmirror, :set_pmirror1, :set_pmirror2, :set_smirror,
		:show, :edit, :update, :destroy,
		:set_alignment, :show_alignment,
		:set_evo, :show_evo,
		:set_thermostabilization, :show_thermostabilization,
		:set_cold_resistance, :show_cold_resistance,
		:set_running_in, :show_running_in,
		:set_sealing, :show_sealing,
		:set_heat_treatment_in_an_argon_chamber, :show_heat_treatment_in_an_argon_chamber,
		:set_varnish_coating, :show_varnish_coating,
		:set_metallization_of_mirrors, :show_metallization_of_mirrors,
		:set_tinning_mirrors, :show_tinning_mirrors,
		:pull_out_cathode, :pull_out_leg, :pull_out_fmirror, :pull_out_pmirror_1, :pull_out_pmirror_2, :pull_out_smirror,
		:set_test3, :show_test3,
		:set_test9, :show_test9,
		:set_test9, :show_test9,
		:set_control, :show_control,
		:suitable
	]

	before_action :test_nitro,
	only: [:show, :set_test3, :show_test3, :set_cold_resistance, :show_cold_resistance, :set_test9, :show_test9]

	before_action :tech_pro_num,
	only: [:index, :show]


	# Возврат из не годных
	def roll_back_bad
		if BadResonator.where params[:id]
			@bad_resonator = BadResonator.find(params[:id])


			if @bad_resonator.al_result == 'Возврат'
				@bad_resonator.update al_result: nil
				@bad_resonator.update al_return_date: nil
				@bad_resonator.update al_reason_for_return: nil
				@bad_resonator.update al_for_the_return_list: nil
				@bad_resonator.update al_refund_description: nil
			end

			if @bad_resonator.ev_result == 'Возврат'
				@bad_resonator.update ev_result: nil
				@bad_resonator.update ev_return_date: nil
				@bad_resonator.update ev_reason_for_return: nil
				@bad_resonator.update ev_for_the_return_list: nil
				@bad_resonator.update ev_refund_description: nil
			end

			if @bad_resonator.t3_result == 'Возврат' or
				@bad_resonator.t3_im_fit == 'Нет'

				@bad_resonator.update t3_result: nil
				@bad_resonator.update t3_return_date: nil
				@bad_resonator.update t3_reason_for_return: nil
				@bad_resonator.update t3_for_the_return_list: nil
				@bad_resonator.update t3_refund_description: nil
				@bad_resonator.update t3_im_fit: nil

			end

			if @bad_resonator.ts_result == 'Возврат'
				@bad_resonator.update ts_result: nil
				@bad_resonator.update ts_return_date: nil
				@bad_resonator.update ts_reason_for_return: nil
				@bad_resonator.update ts_for_the_return_list: nil
				@bad_resonator.update ts_refund_description: nil
			end

			if @bad_resonator.cr_result == 'Возврат' or
				@bad_resonator.cr_im_fit == 'Нет'
				@bad_resonator.update cr_result: nil
				@bad_resonator.update cr_return_date: nil
				@bad_resonator.update cr_reason_for_return: nil
				@bad_resonator.update cr_for_the_return_list: nil
				@bad_resonator.update cr_refund_description: nil
				@bad_resonator.update cr_im_fit: nil
			end

			if @bad_resonator.ri_result == 'Возврат'
				@bad_resonator.update ri_result: nil
				@bad_resonator.update ri_return_date: nil
				@bad_resonator.update ri_reason_for_return: nil
				@bad_resonator.update ri_for_the_return_list: nil
				@bad_resonator.update ri_refund_description: nil
			end

			if @bad_resonator.se_result == 'Возврат' or
				@bad_resonator.se_im_fit == 'Нет'

				@bad_resonator.update se_result: nil
				@bad_resonator.update se_return_date: nil
				@bad_resonator.update se_reason_for_return: nil
				@bad_resonator.update se_for_the_return_list: nil
				@bad_resonator.update se_refund_description: nil
				@bad_resonator.update se_im_fit: nil
			end

			if @bad_resonator.ht_result == 'Возврат'
				@bad_resonator.update ht_result: nil
				@bad_resonator.update ht_return_date: nil
				@bad_resonator.update ht_reason_for_return: nil
				@bad_resonator.update ht_for_the_return_list: nil
				@bad_resonator.update ht_refund_description: nil
			end

			if 	@bad_resonator.t9_result == 'Возврат' or
				@bad_resonator.t9_im_fit == 'Нет'

				@bad_resonator.update t9_result: nil
				@bad_resonator.update t9_return_date: nil
				@bad_resonator.update t9_reason_for_return: nil
				@bad_resonator.update t9_for_the_return_list: nil
				@bad_resonator.update t9_refund_description: nil
				@bad_resonator.update t9_im_fit: nil
			end

			if @bad_resonator.cn_result=='Возврат'
				@bad_resonator.update cn_result: nil
				@bad_resonator.update cn_return_date: nil
				@bad_resonator.update cn_reason_for_return: nil
				@bad_resonator.update cn_for_the_return_list: nil
				@bad_resonator.update cn_refund_description: nil
			end
		end

		# Далее переместим запись из негодных в годные

		# Создаю запись в таблице нормальных резонаторов
		@resonator = Resonator.create(@bad_resonator.attributes)
		@bad_resonator.destroy

		set_history("Возвращено из брака")

		redirect_to resonators_path

	end

	def search

		@search_val = params[:search_val]

		if Resonator.where("number LIKE ?", "%#{@search_val}%")
			@resonators = Resonator.where("number LIKE ?", "%#{@search_val}%")
		end

		if BadResonator.where("number LIKE ?", "%#{@search_val}%")
			@bad_resonators = BadResonator.where("number LIKE ?", "%#{@search_val}%")
		end

	end

	def index

		# Посчитаем давления азота
		@work_resonator = Resonator.where(created_at: Date.new(2017, 9, 1)..Date.current + 1.day).where.not(suitable: true)
		@good_resonator = Resonator.where(created_at: Date.new(2017, 1, 1)..Date.new(2017, 9, 1)).or(Resonator.where(suitable: true))

		@current_title = "В производстве"
		$rv_type = true

		case params[:view_type]
		when 'good'
			@resonators = @good_resonator.order(created_at: :desc).page(params[:page]).per_page(20)
			@current_title = "Годные"
			$rv_type = true

		when 'bad'
			@resonators = BadResonator.all.order(created_at: :desc).page(params[:page]).per_page(20)
			@current_title = "Не годные"
			$rv_type = false

		else
			@resonators = @work_resonator.order(created_at: :desc).page(params[:page]).per_page(999)
		end

		respond_to do |format|
			format.html
			format.csv { send_data @resonators.to_csv }
			format.xls # { send_data @resonators.to_csv(col_sep: "\t") }
		end

	end


	def new
		@resonator = Resonator.new
	end

	def create
		@resonator = Resonator.new(resonator_params)

		if @resonator.save

			end_count(@resonator)

			set_history("Запись создана")

			redirect_to @resonator,
			notice: "Добавлена запись #{@resonator.number}"
		else
			redirect_to new_resonator_path,
			notice: @resonator.errors.first[1]
		end
	end

	def edit
	end

	def show
	 	@histories = History.where(dev_id: @resonator.id)
	end

	def update

		uresonator_params = resonator_params

		# Изменение напряжения горения, ΔU гор
		uresonator_params[:cr_combustion_voltage_delta] =
		uresonator_params[:cr_burning_voltage_after_10_min].to_f -
		uresonator_params[:cr_burning_voltage_after_2_sec].to_f

		# Среднее по модам захвата , Σ(Захват на моде N)/4
		uresonator_params[:cr_capture_in_fashion_delta] = (
		uresonator_params[:cr_capture_in_fashion_1].to_f +
		uresonator_params[:cr_capture_in_fashion_2].to_f +
		uresonator_params[:cr_capture_in_fashion_3].to_f +
		uresonator_params[:cr_capture_in_fashion_4].to_f) / 4

		# Средняя частота выходного сигнала на модах
		uresonator_params[:cr_output_frequency_delta] = (
		uresonator_params[:cr_output_frequency_in_mode_1].to_f +
		uresonator_params[:cr_output_frequency_in_mode_2].to_f +
		uresonator_params[:cr_output_frequency_in_mode_3].to_f +
		uresonator_params[:cr_output_frequency_in_mode_4].to_f) / 4


		# Приработка. Среднее по модам захвата , Σ(Захват на моде N)/4
		uresonator_params[:ri_capture_in_fashion_delta] = (
		uresonator_params[:ri_capture_in_fashion_1].to_f +
		uresonator_params[:ri_capture_in_fashion_2].to_f +
		uresonator_params[:ri_capture_in_fashion_3].to_f +
		uresonator_params[:ri_capture_in_fashion_4].to_f) / 4

		# Средняя частота выходного сигнала на модах
		uresonator_params[:ri_output_frequency_delta] = (
		uresonator_params[:ri_output_frequency_in_mode_1].to_f +
		uresonator_params[:ri_output_frequency_in_mode_2].to_f +
		uresonator_params[:ri_output_frequency_in_mode_3].to_f +
		uresonator_params[:ri_output_frequency_in_mode_4].to_f) / 4

		# Если ввели ногу, которой нет в базе - нужно её создать, запарево по поводу id,
		# в select вводится номер ноги, select возвращает id, но если мы вводим значение которого
		# нет в списке - select вернёт введённый номер, в этом случае нужно создать новую запись в ногах
		# и резонатору присвоить id этой записи!!

		if uresonator_params[:leg_id]

			# Если введённая нога отсутствует в базе (Посмотреть, могут быть проблемы, когда id станут по количеству цифр такие же как disk_number)
			if !Leg.find_by id: uresonator_params[:leg_id]
				new_leg = Leg.new(disk_number: "#{uresonator_params[:leg_id]}",
					ds_date_of_manufacture: Time.now, resonator_id: @resonator.id,
					date_of_installation_in_the_resonator: Time.now)

				if new_leg.save
					uresonator_params[:leg_id] = new_leg.id
				end
			end
		end

		if @resonator.update(uresonator_params)

		end_count(@resonator)

			if @resonator.leg_id?
				set_leg(@resonator.leg_id)
				if uresonator_params[:leg_id]
					set_history("Добавлена ножка #{Leg.find(@resonator.leg_id).disk_number}")
				end
			end

			if @resonator.cathode_id?
				set_cathode(@resonator.cathode_id)
				if uresonator_params[:cathode_id]
					set_history("Добавлен катод #{Cathode.find(@resonator.cathode_id).number}")
				end
			end

			if @resonator.fmirror_id?
				set_mirror(@resonator.fmirror_id)
				if uresonator_params[:fmirror_id]
					set_history("Добавлено плоское зеркало #{Mirror.find(@resonator.fmirror_id).number_of_substrate}")
				end
			end

			if @resonator.pmirror_1_id?
				set_mirror(@resonator.pmirror_1_id)
				if uresonator_params[:pmirror_1_id]
					set_history("Добавлено пьезо зеркало #{Mirror.find(@resonator.pmirror_1_id).number_of_substrate}")
				end
			end

			if @resonator.pmirror_2_id?
				set_mirror(@resonator.pmirror_2_id)
				if uresonator_params[:pmirror_2_id]
					set_history("Добавлено пьезо зеркало #{Mirror.find(@resonator.pmirror_2_id).number_of_substrate}")
				end
			end

			if @resonator.smirror_id?
				set_mirror(@resonator.smirror_id)
				if uresonator_params[:smirror_id]
					set_history("Добавлено сферическое зеркало #{Mirror.find(@resonator.smirror_id).number_of_substrate}")
				end
			end

			check_the_status_of_marriage(@resonator)

			set_history("Сведения обновлены")

			redirect_to resonator_url,
            notice: "#{@resonator.number} изменен"
		else

			if @resonator.errors.first.first.to_s == 'piezo_same'
				redirect_to set_alignment_resonators_path(@resonator.id),
				notice: @resonator.errors.first.last
			else
				redirect_to resonator_url,
				notice: @resonator.errors.first.last
			end
		end

	end

	def destroy

		# Удаляю все связи с другими таблицами
		del_from_sensor()
		del_from_fmirror()
		del_from_smirror()
		del_from_pmirror()
		del_from_leg()
		del_from_cathode()

		# Удаляю историю
		if History.where dev_id: @resonator.id
			@history = History.where dev_id: @resonator.id

			@history.each do |h|
				h.destroy
			end
		end

		if @resonator.destroy
			redirect_to resonators_path,
            notice: "Удален #{@resonator.number}"
		else
			redirect_to edit_resonator_path,
            notice: "Не удалален #{@resonator.number}"
		end
	end

	# Удаление связей резонатора с комплектующими
	def pull_out_cathode
		cathode = Cathode.find(@resonator.cathode_id)
		cathode.update(resonator_id: nil)
		cathode.update(date_of_installation_in_the_resonator: nil)
		@resonator.update(cathode_id: nil)

		set_history("Катод #{cathode.number} удален из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end

	def pull_out_leg
		leg = Leg.find(@resonator.leg_id)
		leg.update(resonator_id: nil)
		leg.update(date_of_installation_in_the_resonator: nil)
		@resonator.update(leg_id: nil)
		@resonator.update(al_leg_clamp: nil)

		set_history("Ножка #{leg.disk_number} удалена из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end

	def pull_out_fmirror
		fmirror = Mirror.find(@resonator.fmirror_id)
		fmirror.update(resonator_id: nil)
		fmirror.update(date_of_installation_in_the_resonator: nil)
		@resonator.update(fmirror_id: nil)

		set_history("Плоское зеркало #{fmirror.number_of_substrate} удаленo из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end

	def pull_out_pmirror_1
		pmirror_1 = Mirror.find(@resonator.pmirror_1_id)
		pmirror_1.update(resonator_id: nil)
		pmirror_1.update(date_of_installation_in_the_resonator: nil)
		@resonator.update(pmirror_1_id: nil)

		set_history("Пьезо зеркало #{pmirror_1.number_of_substrate} удаленo из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end

	def pull_out_pmirror_2
		pmirror_2 = Mirror.find(@resonator.pmirror_2_id)
		pmirror_2.update(resonator_id: nil)
		pmirror_2.update(date_of_installation_in_the_resonator: nil)
		@resonator.update(pmirror_2_id: nil)

		set_history("Пьезо зеркало #{pmirror_2.number_of_substrate} удаленo из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end

	def pull_out_smirror
		smirror = Mirror.find(@resonator.smirror_id)
		smirror.update(resonator_id: nil)
		@resonator.update(smirror_id: nil)

		set_history("Сферическое зеркало #{smirror.number_of_substrate} удаленo из резонатора")

		redirect_to set_alignment_resonators_path(id: @resonator.id)
	end


	def set_resonator

		if Resonator.find_by(id: params[:id])
			@resonator = Resonator.find(params[:id])
		elsif BadResonator.find_by(id: params[:id])
			@resonator = BadResonator.find(params[:id])
		else
			@resonator = Resonator.last
		end

	end


	private

		# Названия операций по техпроцессу
		def tech_pro_num
			@t3_title = "оп. 075, 080 согласно ЖГДК.203625.004"
			@ts_title = "оп. 085, 090 согласно ЖГДК.203625.004"
			@cr_title = "оп. 095, 100, 105 согласно ЖГДК.203625.004"
			@ri_title = "оп. 110, 115, 120 согласно ЖГДК.203625.004"
			@se_title = "оп. 125, 130, 135 согласно ЖГДК.203625.004"
			@ht_title = "оп. 140, 145, 150, 155 согласно ЖГДК.203625.004"
			@t9_title = "оп. 175, 180 согласно техпроцесса ЖГДК.203625.004"
		end

 		# Значения азота
 		def test_nitro
			@nitro3 = (@resonator.t3_nitrogen_level.to_i * 10 ** @resonator.t3_nitrogen_level_rate.to_i).to_f
		 	@nitro5 = (@resonator.cr_nitrogen_level.to_i * 10 ** @resonator.cr_nitrogen_level_rate.to_i).to_f
		 	@nitro9 = (@resonator.t9_nitrogen_level.to_i * 10 ** @resonator.t9_nitrogen_level_rate.to_i).to_f
	 	end

		# Записать в историю
		def set_history(operation)
			History.new(dev: 'resonator', dev_id: @resonator.id, operation: operation, operator: 'noname').save
		end


		def set_mirror(id)
			mirror = Mirror.find(id)
			mirror.update resonator_id: @resonator.id
			mirror.update date_of_installation_in_the_resonator: Time.now
		end

		def set_leg(id)
			leg = Leg.find(id)
			leg.update resonator_id: @resonator.id
			leg.update date_of_installation_in_the_resonator: Time.now
		end

		def set_cathode(id)
			cathode = Cathode.find(id)
			cathode.update resonator_id: @resonator.id
			cathode.update date_of_installation_in_the_resonator: Time.now
		end

		def del_from_sensor
			if sensor_id = @resonator.sensor_id
				sensor = Sensor.find(sensor_id)
				sensor.update resonator_id: nil
			end
		end

		def del_from_smirror
			if smirror_id = @resonator.smirror_id
				smirror = Mirror.find(smirror_id)
				smirror.update resonator_id: nil
			end
		end

		def del_from_fmirror
			if fmirror_id = @resonator.fmirror_id
				fmirror = Mirror.find(fmirror_id)
				fmirror.update resonator_id: nil
			end
		end

		def del_from_pmirror
			if pmirror_id = @resonator.pmirror_1_id
				pmirror = Mirror.find(pmirror_id)
				pmirror.update resonator_id: nil
			end

			if pmirror_id = @resonator.pmirror_2_id
				pmirror = Mirror.find(pmirror_id)
				pmirror.update resonator_id: nil
			end
		end

		def del_from_leg
			if leg_id = @resonator.leg_id
				leg = Leg.find(leg_id)
				leg.update resonator_id: nil
			end
		end

		def del_from_cathode
			if cathode_id = @resonator.cathode_id
				cathode = Cathode.find(cathode_id)
				cathode.update resonator_id: nil
			end
		end


		# Проверка стаутса брака
		def check_the_status_of_marriage(resonator)

			if 	resonator.al_result == 'Возврат' or
				resonator.ev_result == 'Возврат' or
				resonator.t3_result == 'Возврат' or
				resonator.t3_im_fit == 'Нет' 	  or
				resonator.ts_result == 'Возврат' or
				resonator.cr_result == 'Возврат' or
				resonator.cr_im_fit == 'Нет' 	  or
				resonator.ri_result == 'Возврат' or
				resonator.se_result == 'Возврат' or
				resonator.se_im_fit == 'Нет' 	  or
				resonator.ht_result == 'Возврат' or
				resonator.t9_result == 'Возврат' or
				resonator.t9_im_fit == 'Нет' 	  or
				resonator.cn_result == 'Возврат'

				# Удаляю информацию из комплектующих
				del_from_sensor()
				del_from_smirror()
				del_from_fmirror()
				del_from_pmirror()
				del_from_leg()
				del_from_cathode()

				# Вытаскиваю все из резонатора
				if 	resonator.sensor_id
					resonator.update sensor_id: nil
				end

				if resonator.smirror_id
					set_history("Сферическое зеркало #{Mirror.find(resonator.smirror_id).number_of_substrate} удаленo из резонатора")
					resonator.update smirror_id: nil
				end

				if resonator.fmirror_id
					set_history("Плоское зеркало #{Mirror.find(resonator.fmirror_id).number_of_substrate} удаленo из резонатора")
					resonator.update fmirror_id: nil
				end

				if resonator.pmirror_1_id
					set_history("Пьезо зеркало #{Mirror.find(resonator.pmirror_1_id).number_of_substrate} удаленo из резонатора")
					resonator.update pmirror_1_id: nil
				end

				if resonator.pmirror_2_id
					set_history("Пьезо зеркало #{Mirror.find(resonator.pmirror_2_id).number_of_substrate} удаленo из резонатора")
					resonator.update pmirror_2_id: nil
				end

				if resonator.leg_id
					set_history("Ножка #{Leg.find(resonator.leg_id).disk_number} удалена из резонатора")
					resonator.update leg_id: nil
				end

				if resonator.cathode_id
					set_history("Катод #{Cathode.find(resonator.cathode_id).number} удален из резонатора")
					resonator.update cathode_id: nil
				end

				# Далее переместим запись из годных в не годные

				# Создаю запись в таблице испорченных резонаторов
				bad_resonator = BadResonator.create(resonator.attributes)
				set_history("Направлено в брак")

				# Удаляю текущую запись из таблицы годных резонаторов
				resonator.destroy

				resonator = bad_resonator

			end
		end


		def end_count(resonator)

			count = 0

			if resonator.alignment == 'Завершена'
				count += 1
			end

			if resonator.evo == 'Завершена'
				count += 1
			end

			if resonator.test_3 == 'Завершена'
				count += 1
			end

			if resonator.thermostabilization == 'Завершена'
				count += 1
			end

			if resonator.cold_resistance == 'Завершена'
				count += 1
			end

			if resonator.running_in == 'Завершена'
				count += 1
			end

			if resonator.sealing == 'Завершена'
				count += 1
			end

			if resonator.heat_treatment_in_an_argon_chamber == 'Завершена'
				count += 1
			end

			if resonator.test_9 == 'Завершена'
				count += 1
			end

			if resonator.control == 'Завершена'
				count += 1
			end

			resonator.update end_count: count

			if count == 10
				resonator.update suitable: true
			else
				resonator.update suitable: false
			end

		end

		def resonator_params
			params.require(:resonator).permit(
				:number,
				:sensor_id,
				:remark,

				# 1.Юстировка
				:alignment,
				:al_date,
				:cathode_id,
				:al_cathode_date_of_manufacture,
				:leg_id,
				:al_leg_date_of_manufacture,
				:al_leg_clamp,
				:fmirror_id,
				:al_fmirror_date_of_manufacture,
				:pmirror_1_id,
				:al_piezo_mirrors_1_date_of_manufacture,
				:pmirror_2_id,
				:al_piezo_mirrors_2_date_of_manufacture,
				:smirror_id,
				:al_smirror_date_of_manufacture,
				:al_TEM00_loss,
				:al_TEM01_loss,
				:al_selectivity,
				:al_optical_contact_descripttion,
				:al_result,
				:al_return_date,
				:al_reason_for_return,
				:al_for_the_return_list,
				:al_refund_description,

				# 2.ЭВО
				:evo,
				:ev_date,
				:ev_vacuum_post_number,
				:ev_threshold_current_at_the_beginning_of_the_workout,
				:ev_burning_stress_at_the_beginning_of_the_workout,
				:ev_burning_voltage_after_5_hours_of_training,
				:ev_combustion_voltage_after_refilling,
				:ev_the_burning_voltage_after_10_hours_of_training,
				:ev_threshold_current_at_the_end_of_the_workout,
				:ev_the_combustion_voltage_at_the_beginning_of_stabilization,
				:ev_the_combustion_voltage_after_1_hour_of_stabilization,
				:ev_the_combustion_voltage_after_2_hours_of_stabilization,
				:ev_the_combustion_voltage_after_3_hours_of_stabilization,
				:ev_threshold_current_at_the_end_of_stabilization,
				:ev_optical_contact_monitoring,
				:ev_description_of_optical_contact,
				:ev_result,
				:ev_return_date,
				:ev_reason_for_return,
				:ev_for_the_return_list,
				:ev_refund_description,

	  			# 3.Проверочная
	  			:test_3,
	  			:t3_appearance,
				:t3_optical_contact,
				:t3_selectivity,
				:t3_losses,
				:t3_im_date,
				:t3_im_fit,
				:t3_nitrogen_level,
				:t3_nitrogen_level_rate,
	      		:t3_result,
		      	:t3_return_date,
		      	:t3_reason_for_return,
		      	:t3_for_the_return_list,
		      	:t3_refund_description,

				# 4.Стабилизация
				:thermostabilization,
				:ts_date,
				:ts_verification_operation_after_ts,
				:ts_optical_contact_monitoring,
				:ts_description_of_optical_contact,
				:ts_measurement_of_combustion_voltage,
				:ts_date_of_preheating_of_getter,
				:ts_result,
				:ts_return_date,
				:ts_reason_for_return,
				:ts_for_the_return_list,
				:ts_refund_description,

				# 5.Испытания климатические
				:cold_resistance,
				:cr_date,
				:cr_verification_operation_after_ts,
				:cr_optical_contact_monitoring,
				:cr_description_of_optical_contact,
				:cr_burning_voltage_after_2_sec,
				:cr_burning_voltage_after_35_sec,
				:cr_burning_voltage_after_10_min,
				:cr_threshold_current_after_XY,
				:cr_output_frequency_in_mode_1,
				:cr_capture_in_fashion_1,
				:cr_output_frequency_in_mode_2,
				:cr_capture_on_fashion_2,
				:cr_output_frequency_in_mode_3,
				:cr_capture_in_fashion_3,
				:cr_output_frequency_in_mode_4,
				:cr_capture_on_fashion_4,
				:cr_im_date,
				:cr_im_fit,
				:cr_appearance,
				:cr_combustion_voltage,
				:cr_losses,
				:cr_selectivity,
				:cr_nitrogen_level,
				:cr_nitrogen_level_rate,
				:cr_result,
				:cr_return_date,
				:cr_reason_for_return,
				:cr_for_the_return_list,
				:cr_refund_description,

				# 6.Приработка
				:running_in,
				:ri_date,
				:ri_verification_operation_after_ts,
				:ri_optical_contact_monitoring,
				:ri_description_of_optical_contact,
				:ri_threshold_current_after_XY,
				:ri_output_frequency_in_mode_1,
				:ri_capture_in_fashion_1,
				:ri_output_frequency_in_mode_2,
				:ri_capture_on_fashion_2,
				:ri_output_frequency_in_mode_3,
				:ri_capture_in_fashion_3,
				:ri_output_frequency_in_mode_4,
				:ri_capture_on_fashion_4,
				:ri_appearance,
				:ri_combustion_voltage_2s,
				:ri_combustion_voltage_35s,
				:ri_combustion_voltage_10m,
				:ri_result,
				:ri_return_date,
				:ri_reason_for_return,
				:ri_for_the_return_list,
				:ri_refund_description,

				# 7.Вибрация
				:sealing,
				:se_date,
				:se_verification_operation_after_ts,
				:se_burning_voltage_after_2_sec,
				:se_burning_voltage_after_35_sec,
				:se_burning_voltage_after_10_min,
				:se_threshold_voltage_1GRP,
				:se_threshold_voltage_2GRP,
				:se_appearance,
				:se_optical_contact,
				:se_im_date,
				:se_im_fit,
				:se_result,
				:se_return_date,
				:se_reason_for_return,
				:se_for_the_return_list,
				:se_refund_description,

				# 8.Термоциклическая обработка
				:heat_treatment_in_an_argon_chamber,
				:ht_date,
				:ht_verification_operation_after_ts,
				:ht_optical_contact_monitoring,
				:ht_description_of_optical_contact,
				:ht_burning_voltage_after_2_sec,
				:ht_burning_voltage_after_35_sec,
				:ht_burning_voltage_after_10_min,
				:ht_threshold_current_after_XY,
				:ht_the_change_in_the_argon_pressure_in_the_resonator,
				:ht_result,
				:ht_return_date,
				:ht_reason_for_return,
				:ht_for_the_return_list,
				:ht_refund_description,

				# 9. Проверочная
				:test_9,
				:t9_appearance,
				:t9_optical_contact,
				:t9_im_date,
				:t9_im_fit,
				:t9_combustion_voltage_2s,
				:t9_combustion_voltage_35s,
				:t9_combustion_voltage_10m,
				:t9_combustion_voltage_delta,
				:t9_threshold_current,
				:t9_omega,
				:t9_nitrogen_level,
				:t9_nitrogen_level_rate,

				:t9_losses,
        		:t9_selectivity,

				:t9_result,
      			:t9_return_date,
      			:t9_reason_for_return,
      			:t9_for_the_return_list,
  				:t9_refund_description,

				# 10.Контрольная
				:control,
				:cn_date,
		      	:cn_result,
      			:cn_return_date,
      			:cn_reason_for_return,
      			:cn_for_the_return_list,
  				:cn_refund_description,

				:cr_combustion_voltage_delta,
				:cr_capture_in_fashion_delta,
				:cr_output_frequency_delta,
				:ri_capture_in_fashion_delta,
				:ri_output_frequency_delta,

				:end_count)
	end

end
