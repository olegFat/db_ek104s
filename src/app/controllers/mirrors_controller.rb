class MirrorsController < ApplicationController

	before_action :authenticate_user!
	skip_before_action :authenticate_user!, only:[:index, :search]

	before_action :set_step,
	only: [:new, :edit]

	before_action :mirror_list_cols, 
	only: [ :index, :new, :edit, :show, :search ]

	before_action :returns_list_cols,
	only: [ :index, :new, :edit, :search ]

	before_action :element_list_cols,
	only: [ :index, :new, :edit, :search ]

	before_action :parameter_list_cols,
	only: [ :index, :new, :edit, :search]

	before_action :set_mirror,
	only: [ :edit, :update, :destroy, :show]

    before_action :mode_list_cols,
    only: [ :new, :edit, :search]

    def search
		@search_val = params[:search_val]

		if @search_val
			case $mv_type
			when 'smirror'
				@mirrors = Mirror.where(mirror_type: "сферическое").where("number_of_substrate LIKE ?", "%#{@search_val}%")
			when 'fmirror'
				@mirrors = Mirror.where(mirror_type: "плоское").where("number_of_substrate LIKE ?", "%#{@search_val}%")
			when 'pmirror'
				@mirrors = Mirror.where(mirror_type: "пьезо").where("number_of_substrate LIKE ?", "%#{@search_val}%")
			when 'all'
				@mirrors = Mirror.where("number_of_substrate LIKE ?", "%#{@search_val}%")
			end
			@mirrors = @mirrors.page(params[:page]).per_page(20)
		end
		
    end

	def index
		# Текущий вид
		if $mv_type == nil
			$mv_type = 'all'
		end

		# Текущая страница
		if $mc_page == nil
			$mc_page = 1
		end

		if params[:page]
			$mc_page = params[:page]
		end

		if params[:view_type]
			case params[:view_type]
			when 'smirror'
				$mv_type = 'smirror'

			when 'fmirror'
				$mv_type = 'fmirror'

			when 'pmirror'
				$mv_type = 'pmirror'

			when 'all'
				$mv_type = 'all'
			end
		else
			$mv_type = 'all'
		end

		case $mv_type
		when 'smirror'
			@mirrors = Mirror.where mirror_type: 'сферическое'

		when 'fmirror'
			@mirrors = Mirror.where mirror_type: 'плоское'

		when 'pmirror'
			@mirrors = Mirror.where mirror_type: 'пьезо'

		else 'all'
			@mirrors = Mirror.all
		end

				

		@mirrors = @mirrors.order(created_at: :desc).page($mc_page).per_page(25)
				


	end

	def new
		@mirror = Mirror.new
	end

	def create
		@mirror = Mirror.new(mirror_params)
		if @mirror.save
			redirect_to mirrors_path,
			notice: "Добавлена запись #{@mirror.number_of_substrate}"
		else
			redirect_to new_mirror_path,
			notice: @mirror.errors.first[1]
		end
	end

	def edit
	end

	def update
		if @mirror.update(mirror_params)
			redirect_to mirrors_path,
            notice: "Запись #{@mirror.number_of_substrate} изменена"
		else
			redirect_to edit_mirror_path,
			notice: @mirror.errors.first[1]
		end
	end

	def destroy
		if @mirror.mirror_type == "Плоское"
			del_fmirror_from_resonator(@mirror.resonator_id)
		end

		if @mirror.mirror_type == "Пьезо"
			del_pmirror_from_resonator(@mirror.resonator_id)
		end

		if @mirror.mirror_type == "Сферическое"
			del_smirror_from_resonator(@mirror.resonator_id)
		end

		if @mirror.destroy
			redirect_to mirrors_path,
            notice: "Удалена запись #{@mirror.number_of_substrate}"
		else
			redirect_to mirrors_path,
            notice: "Не удалось удалить запись #{@mirror.number_of_substrate}"
		end
	end

	private

		def set_mirror
			@mirror = Mirror.find(params[:id])
		end

		def del_fmirror_from_resonator(id)
			if resonator_id = @mirror.resonator_id
				resonator = Resonator.find(resonator_id)
				resonator.update fmirror_id: nil
			end
		end


		def del_pmirror_from_resonator(id)
			if resonator_id = @mirror.resonator_id
				resonator = Resonator.find(resonator_id)
				if resonator.pmirror_1_id == @mirror.id
					resonator.update pmirror_1_id: nil
				else
					resonator.update pmirror_2_id: nil
				end
			end
		end


		def del_smirror_from_resonator(id)
			if resonator_id = @mirror.resonator_id
				resonator = Resonator.find(resonator_id)
				resonator.update smirror_id: nil
			end
		end

		def mirror_params
			params.require(:mirror).permit(
				
				:number_of_substrate,
				:mirror_type,
				:date_of_manufacture_of_the_substrate,
				:name_of_polisher,
				:roughness_of_the_substrate,
				:radius_of_curvature_is_nominal,
				:radius_of_curvature_is_actual,
				:date_of_measurement_of_the_radius_of_curvature,
				:result_of_polishing,
				:description_of_appearance_substrate,
				:manufacturer,
				:date_of_deposition,
				:spray_installation,
				:appearance,
				:description_of_appearance,
				:date_control,
				:tl_total_losses,
				:tl_measurement,
				:tl_date_of_measurement,
				:ci_coefficient_of_integral_scattering,
				:ci_measurement,
				:ci_date_of_measurement,
				:pa_phase_anisotropy,
				:pa_ci_date_of_measurement,
				:tr_transmittance,
				:tr_date_of_measurement,
				:date_of_transfer_to_the_assembly_in_the_resonator,
				:return,
				:return_date,
				:reason_for_return,
				:for_the_return_list,
				:deviations,
				:number_of_assemblies,
				:resonator_id,
				:date_of_installation_in_the_resonator)
		end
end




