class Cathode < ApplicationRecord
	resourcify
	# presence - пустое
	# uniqueness - уникально
	validates :number, presence: {message: "Введите номер катода!"}
	validates :date_of_manufacture, presence: {message: "Введите дату изготовления!"}

	# уникально ли сочетание :number и :date_of_manufacture ?
	validates :number, uniqueness: { scope: :date_of_manufacture, message: "Такая запись уже существует!"}

end
