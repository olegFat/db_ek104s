class Resonator < ApplicationRecord

	resourcify

	validate :piezo_can_not_be_the_same,
	:new_leg_exists

	validates :number,
	presence: {message: "Введите номер резонатора!"}

	validates :number,
	uniqueness: {message: "Резонатор с таким номером уже существует!"}

	# В резонатор нельзя вставить два пьезо зеркала с одинаковыми номерами
	def piezo_can_not_be_the_same
		if pmirror_1_id? or pmirror_2_id?
			if pmirror_1_id == pmirror_2_id
				errors.add(:alignment, "Указано одно и тоже пьезо зеркало!")
			end
		end
	end

	# Проверим новую ногу на совпадение со списком ног
	def new_leg_exists
		if Leg.where(disk_number: leg_id).exists?
			errors.add(:alignment, "Ножка с таким номером уже существует!")
		end
	end

	# Экспорт в CSV
	def self.to_csv(options = {})
		CSV.generate(options) do |csv|

	    	csv << column_names

	    	all.each do |resonator|
	      		csv << resonator.attributes.values_at(*column_names)
			end

		end
	end

end
