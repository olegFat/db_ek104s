class Mirror < ApplicationRecord
	resourcify
	# presence - пустое
	# uniqueness - уникально
	validates :number_of_substrate, presence: {message: "Введите номер подложки!"}
	validates :date_of_manufacture_of_the_substrate, presence: {message: "Введите дату изготовления!"}
	#validates :date_of_deposition, presence: {message: "Введите дату напыления!"}
	#validates :pa_phase_anisotropy, presence: {message: "Введите информацию о фазовой анизотропии!"}

	# уникально ли сочетание :number_of_substrate, :mirror_type, :date_of_manufacture_of_the_substrate ?
	validates :number_of_substrate, uniqueness: { scope: [:mirror_type, :date_of_manufacture_of_the_substrate], message: "Такая запись уже существует!"}

end
