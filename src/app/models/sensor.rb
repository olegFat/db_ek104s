class Sensor < ApplicationRecord
	resourcify
	# presence - пустое
	# uniqueness - уникально
	validates :number, presence: {message: "Введите номер датчика!"}
	validates :number, uniqueness: {message: "Датчик с таким номером уже внесен в базу!"}
	validates :resonator_id, presence: {message: "Введите номер резонатора!"}
end
