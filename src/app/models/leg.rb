class Leg < ApplicationRecord
	resourcify
	# presence - пустое
	# uniqueness - уникально
	validates :disk_number, presence: {message: "Введите номер диска!"}
	validates :ds_date_of_manufacture, presence: {message: "Введите дату изготовления!"}

	# уникально ли сочетание :disk_number и :ds_date_of_manufacture ?
	validates :disk_number, uniqueness: { scope: :ds_date_of_manufacture, message: "Такая запись уже существует!"}

	mount_uploader :xray_file, XRayFileUploader
end
