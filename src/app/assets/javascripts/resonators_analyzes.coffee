$(document).on 'turbolinks:load', ->

    $('#date_clear_button').click ->
      $('input#date_in').val("")
      $('input#date_out').val("")

    $('#x_interval_clear_button').click ->
      $('input#x_in').val("")
      $('input#x_out').val("")

    $('#round_val_clear_button').click ->
      $('input#round').val("")

    $('.slider').slider
      min: 0
      max: 5
