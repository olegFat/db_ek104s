return_field_show = () ->
    $(".return_fields").css("display", "block")
    $('#return_date_field').prop("disabled", false)
    $('select#reason_for_return_field').prop("disabled", false)
    $("#return_description_field").prop("disabled", false)

return_field_hide = () ->
    $(".return_fields").css("display", "none")
    $('#return_date_field').prop("disabled", true)
    $('select#reason_for_return_field').prop("disabled", true)
    $("#return_description_field").prop("disabled", true)

option_group_0_hide = () ->
    $("select#option-group-0-field").prop("disabled", true)
    $(".option-group-0").css("display","none")#none
option_group_0_show = () ->
    $("select#option-group-0-field").prop("disabled", false)
    $(".option-group-0").css("display","block")

option_group_1_hide = () ->
    $("select#option-group-1-field").prop("disabled", true)
    $(".option-group-1").css("display","none")#none
option_group_1_show = () ->
    $("select#option-group-1-field").prop("disabled", false)
    $(".option-group-1").css("display","block")

option_group_2_hide = () ->
    $("select#option-group-2-field").prop("disabled", true)
    $(".option-group-2").css("display","none")#none
option_group_2_show = () ->
    $("select#option-group-2-field").prop("disabled", false)
    $(".option-group-2").css("display","block")

option_item_other_hide = () ->
    $("#option-item-other-field").prop("disabled", true)
    $(".option-item-other").css("display","none")#none
option_item_other_show = () ->
    $("#option-item-other-field").prop("disabled", false)
    $(".option-item-other").css("display","block")

option_other_hide = () ->
    $("#option-other-field").prop("disabled", true)
    $(".option-other").css("display","none") #none
option_other_show = () ->
    $("#option-other-field").prop("disabled", false)
    $(".option-other").css("display","block")

option_hide_all = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_0_show = () ->
    option_other_hide()
    option_group_0_show()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_1_show = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_show()
    option_group_2_hide()
    option_item_other_hide()

group_2_show = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_show()
    option_item_other_hide()

group_other_show = () ->
    option_other_show()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_item_other_show = () ->
    option_other_hide()
    option_group_0_show()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_show()

# Выпадающие списки причин возврата
current_return = (current_value) ->
    switch current_value
        when 'group_1'
            group_1_show()
            if $("select#option-group-1-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_1_show()
        when 'group_2'
            group_2_show()
            if $("select#option-group-2-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_2_show()
        when 'other'
            group_other_show()
        else
            if $("select#option-group-0-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_0_show()
            break

# Выпадающий список результата
current_item_return_field = () ->
    if $('select#result_field').val() == "Возврат"
        return_field_show()
        if $("select#option-group-0-field").val() == 'in_group_other'
            group_item_other_show()
        else
            current_return($('select#reason_for_return_field').val())
    else
        return_field_hide()
        option_hide_all()

# Цвет panel
status_container = (container, nos_val, nos_rate, shamrock, result, status) ->
    if shamrock == 'Нет' or result == 'Возврат'
        container.addClass("panel-danger")
    else
        if nos_val != null and nos_rate != null and nos_val * Math.pow(10, nos_rate) > Math.pow(10,-5)
            container.addClass("panel-warning")
        else
            if status == 'Завершена'
                container.addClass("panel-info")
            else
                container.addClass("panel-default")

$(document).on 'turbolinks:load',->

    # 1.Юстировка призмы и ФПУ
    align_prism_and_fpu_result = $('.align_prisms_and_fpu_container').data('return')
    align_prism_and_fpu_status = $('.align_prisms_and_fpu_container').data('status')
    status_container($('.align_prisms_and_fpu_container'), null, null, null, align_prism_and_fpu_result, align_prism_and_fpu_status)

    # 2.Проверка параметров после монтажа и сборки в экраны
    checking_the_build_quality_in_screens_result = $('.checking_the_build_quality_in_screens_container').data('return')
    checking_the_build_quality_in_screens_status = $('.checking_the_build_quality_in_screens_container').data('status')
    status_container($('.checking_the_build_quality_in_screens_container'), null, null, null, checking_the_build_quality_in_screens_result, checking_the_build_quality_in_screens_status)

    # 3.Проверка на устойчивость к синусоидальной вибрации
    check_for_resistance_to_sinusoidal_vibration_result = $('.check_for_resistance_to_sinusoidal_vibration_container').data('return')
    check_for_resistance_to_sinusoidal_vibration_status = $('.check_for_resistance_to_sinusoidal_vibration_container').data('status')
    status_container($('.check_for_resistance_to_sinusoidal_vibration_container'), null, null, null, check_for_resistance_to_sinusoidal_vibration_result, check_for_resistance_to_sinusoidal_vibration_status)

    # 4.Проверка по ПМ
    check_by_pm_result = $('.check_by_pm_container').data('return')
    check_by_pm_status = $('.check_by_pm_container').data('status')
    status_container($('.check_by_pm_container'), null, null, null, check_by_pm_result, check_by_pm_status)

    # 5.ПСИ
    psi_result = $('.psi_container').data('return')
    psi_status = $('.psi_container').data('status')
    status_container($('.psi_container'), null, null, null, psi_result, psi_status)
