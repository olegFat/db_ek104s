return_field_show = () ->
    $(".return_fields").css("display", "block")
    $('#return_date_field').prop("disabled", false)
    $('select#reason_for_return_field').prop("disabled", false)
    $("#return_description_field").prop("disabled", false)

return_field_hide = () ->
    $(".return_fields").css("display", "none")
    $('#return_date_field').prop("disabled", true)
    $('select#reason_for_return_field').prop("disabled", true)
    $("#return_description_field").prop("disabled", true)

option_group_0_hide = () ->
    $("select#option-group-0-field").prop("disabled", true)
    $(".option-group-0").css("display","none")#none
option_group_0_show = () ->
    $("select#option-group-0-field").prop("disabled", false)
    $(".option-group-0").css("display","block")

option_group_1_hide = () ->
    $("select#option-group-1-field").prop("disabled", true)
    $(".option-group-1").css("display","none")#none
option_group_1_show = () ->
    $("select#option-group-1-field").prop("disabled", false)
    $(".option-group-1").css("display","block")

option_group_2_hide = () ->
    $("select#option-group-2-field").prop("disabled", true)
    $(".option-group-2").css("display","none")#none
option_group_2_show = () ->
    $("select#option-group-2-field").prop("disabled", false)
    $(".option-group-2").css("display","block")

option_item_other_hide = () ->
    $("#option-item-other-field").prop("disabled", true)
    $(".option-item-other").css("display","none")#none
option_item_other_show = () ->
    $("#option-item-other-field").prop("disabled", false)
    $(".option-item-other").css("display","block")

option_other_hide = () ->
    $("#option-other-field").prop("disabled", true)
    $(".option-other").css("display","none") #none
option_other_show = () ->
    $("#option-other-field").prop("disabled", false)
    $(".option-other").css("display","block")

option_hide_all = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_0_show = () ->
    option_other_hide()
    option_group_0_show()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_1_show = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_show()
    option_group_2_hide()
    option_item_other_hide()

group_2_show = () ->
    option_other_hide()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_show()
    option_item_other_hide()

group_other_show = () ->
    option_other_show()
    option_group_0_hide()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_hide()

group_item_other_show = () ->
    option_other_hide()
    option_group_0_show()
    option_group_1_hide()
    option_group_2_hide()
    option_item_other_show()

# Выпадающие списки причин возврата
current_return = (current_value) ->
    switch current_value
        when 'group_1'
            group_1_show()
            if $("select#option-group-1-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_1_show()
        when 'group_2'
            group_2_show()
            if $("select#option-group-2-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_2_show()
        when 'other'
            group_other_show()
        else
            if $("select#option-group-0-field").val() == 'in_group_other'
                group_item_other_show()
            else
                group_0_show()
            break

# Выпадающий список результата
current_item_return_field = () ->
    if $('select#result_field').val() == "Возврат"
        return_field_show()
        if $("select#option-group-0-field").val() == 'in_group_other'
            group_item_other_show()
        else
            current_return($('select#reason_for_return_field').val())
    else
        return_field_hide()
        option_hide_all()

# Цвет panel
status_container = (container, nos_val, nos_rate, shamrock, result, status) ->
    if shamrock == 'Нет' or result == 'Возврат'
        container.addClass("panel-danger")
    else
        if nos_val != null and nos_rate != null and nos_val * Math.pow(10, nos_rate) > Math.pow(10,-5)
            container.addClass("panel-warning")
        else
            if status == 'Завершена'
                container.addClass("panel-info")
            else
                container.addClass("panel-default")


# Видимость поля 'Усилие прижима ножки, кг' в 1.Юстировка
leg_clamp_field = () ->
    if $('select#resonator_leg_id').val() != ""
        $(".leg_clamp").css("display", "block")
        $('#leg_clamp_field').prop("disabled", false)
    else
        $(".leg_clamp").css("display", "none")
        $('#leg_clamp_field').prop("disabled", true)

# Видимость полей ввода 2.ЭВО при внесенном номере поста
evo_fields = () ->
    if $('#resonator_ev_vacuum_post_number').val() != ""
        $('.evo_fields').css("display", "block")
    else
        $('.evo_fields').css("display", "none")

$(document).on 'turbolinks:load', ->

    $(window).scroll ->
        if $(this).scrollTop() > 1
            $('header').addClass("sticky")
        else
            $('header').removeClass("sticky")

    # Какого цвета поля в resonator_show у всех полей
    # 1.Юстировка
    alignment_result = $('.alignment_container').data('return')
    alignment_status = $('.alignment_container').data('status')
    status_container($('.alignment_container'), null, null, null, alignment_result, alignment_status)

    # 2.ЭВО
    evo_result = $('.evo_container').data('return')
    evo_status = $('.evo_container').data('status')
    status_container($('.evo_container'), null, null, null, evo_result, evo_status)

    # 3.Проверочная
    test_3_nos_val = $('.test_3_container').data('nos')
    test_3_nos_rate = $('.test_3_container').data('nos-rate')
    test_3_shamrock = $('.test_3_container').data('shamrock')
    test_3_result = $('.test_3_container').data('return')
    test_3_status = $('.test_3_container').data('status')
    status_container($('.test_3_container'), test_3_nos_val, test_3_nos_rate, test_3_shamrock, test_3_result, test_3_status)

    # 4.Стабилизация
    thermostabilization_result = $('.thermostabilization_container').data('return')
    thermostabilization_status = $('.thermostabilization_container').data('status')
    status_container($('.thermostabilization_container'), null, null, null, thermostabilization_result, thermostabilization_status)

    # 5.Испытания климатические
    cold_resistance_nos_val = $('.cold_resistance_container').data('nos')
    cold_resistance_nos_rate = $('.cold_resistance_container').data('nos-rate')
    cold_resistance_shamrock = $('.cold_resistance_container').data('shamrock')
    cold_resistance_result = $('.cold_resistance_container').data('return')
    cold_resistance_status = $('.cold_resistance_container').data('status')
    status_container($('.cold_resistance_container'), cold_resistance_nos_val, cold_resistance_nos_rate, cold_resistance_shamrock, cold_resistance_result, cold_resistance_status)

    # 6.Приработка
    running_in_result = $('.running_in_container').data('return')
    running_in_status = $('.running_in_container').data('status')
    status_container($('.running_in_container'), null, null, null, running_in_result, running_in_status)

    # 7.Вибрация
    sealing_shamrock = $('.sealing_container').data('shamrock')
    sealing_result = $('.sealing_container').data('return')
    sealing_status = $('.sealing_container').data('status')
    status_container($('.sealing_container'), null, null, sealing_shamrock, sealing_result, sealing_status)

    # 8.Термоциклическая обработка
    heat_treatment_in_an_argon_chamber_result =  $('.heat_treatment_in_an_argon_chamber_container').data('return')
    heat_treatment_in_an_argon_chamber_status =  $('.heat_treatment_in_an_argon_chamber_container').data('status')
    status_container($('.heat_treatment_in_an_argon_chamber_container'), null, null, null, heat_treatment_in_an_argon_chamber_result, heat_treatment_in_an_argon_chamber_status)

    # 9.Проверочная
    test_9_nos_val = $('.test_9_container').data('nos')
    test_9_resistance_nos_rate =  $('.test_9_container').data('nos-rate')
    test_9_shamrock = $('.test_9_container').data('shamrock')
    test_9_resistance_result =  $('.test_9_container').data('return')
    test_9_resistance_status =  $('.test_9_container').data('status')
    status_container($('.test_9_container'), test_9_nos_val, test_9_resistance_nos_rate, test_9_shamrock, test_9_resistance_result, test_9_resistance_status)

    # 10.Контрольная
    control_result = $('.control_container').data('return')
    control_status = $('.control_container').data('status')
    status_container($('.control_container'), null, null, null, control_result, control_status)



    # 1.Юстировка, видимость поля усиление прижима ножки, кг
    leg_clamp_field()
    $('select#resonator_leg_id').change ->
        leg_clamp_field()

    # 2.ЭВО, видимость полей в зависимости
    # от заполнения номера вакуумного поста
    evo_fields()
    $('#resonator_ev_vacuum_post_number').keyup ->
        evo_fields()


    # Реакция на 'Результат'
    # При первом запуске страницы
    current_item_return_field()
    # При смене пункта меню "Результат"
    $('select#result_field').change ->
        current_item_return_field()

    # При смене пункта меню "Причина возврата"
    $('select#reason_for_return_field').change ->
        current_return($('select#reason_for_return_field').val())

    # При смене пункта меню option-group-0-field
    $("select#option-group-0-field").change ->
        if $(this).val() == 'in_group_other'
            group_item_other_show()
        else
            current_return($('select#reason_for_return_field').val())

    # При смене пункта меню option-group-1-field
    $("select#option-group-1-field").change ->
        if $(this).val() == 'in_group_other'
            group_item_other_show()
        else
            current_return($('select#reason_for_return_field').val())

    # При смене пункта меню option-group-2-field
    $("select#option-group-2-field").change ->
        if $(this).val() == 'in_group_other'
            group_item_other_show()
        else
            current_return($('select#reason_for_return_field').val())


    # Выбор даты
    $('.datepicker').datepicker
        language: 'ru',
        format: 'yyyy-mm-dd'

    # tooltip
    $('[data-toggle="tooltip"]').tooltip()

    # select2
    $('.select2').select2
        theme: 'bootstrap'

    # select2_multi
    $('.select2_multi').select2
        theme: 'bootstrap'
        placeholder: "Выберите.."

    # select2 без строки поиск
    $('.select2_nosearch').select2
        theme: 'bootstrap'
        minimumResultsForSearch: Infinity   #без строки поиск

    # select2 со своими значениями
    $('.select2_tags').select2
        theme: 'bootstrap'
        placeholder: "Выберите или ввидите"
        tags: true

    # select2 с пустой строкой / во вьюхе в select должна быть пустая option
    $('.select2_empty').select2
        theme: 'bootstrap'
        placeholder: "Выберите.."

    # select2 с пустой строкой / во вьюхе в select должна быть пустая option
    $('.select2_empty_rate').select2
        theme: 'bootstrap'
        placeholder: "В степени"
        allowClear: true


    # Назад, просто назад как в браузере
    $('#back').click ->
        javascript:history.back()

    # Modal
    $('.search').on 'show.bs.modal', (event) ->
        return

