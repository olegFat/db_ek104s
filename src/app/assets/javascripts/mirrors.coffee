hide_element_list = () ->
    $("select#element_list_field").prop("disabled", true)
    $(".element_list").css("display","none")#none
show_element_list = () ->
    $("select#element_list_field").prop("disabled", false)
    $(".element_list").css("display","block")

hide_parameter_list = () ->
    $("select#parameter_list_field").prop("disabled", true)
    $(".parameter_list").css("display","none")#none
show_parameter_list = () ->
    $("select#parameter_list_field").prop("disabled", false)
    $(".parameter_list").css("display","block")

hide_other_list = () ->
    $("#other_list_field").prop("disabled", true)
    $(".other_list").css("display","none") #none
show_other_list = () ->
    $("#other_list_field").prop("disabled", false)
    $(".other_list").css("display","block")

current_return = (current_value) ->
    switch current_value
        when 'Другое'
            show_other_list()
            hide_parameter_list()
            hide_element_list()

        when 'Параметр меньше нормы' 
            hide_other_list()
            show_parameter_list()
            hide_element_list()

        when 'Параметр больше нормы'
            hide_other_list()
            show_parameter_list()
            hide_element_list()

        else
            hide_other_list()
            hide_parameter_list()
            show_element_list()
            break


$(document).on 'turbolinks:load',->

    # При изменении поля Возврат
    $('select#return_field').change ->
        if $(this).val() == "Да"
            $('select#return_date_field').prop("disabled", false)
            $('select#reason_for_return_field').prop("disabled", false)
            current_return($('select#reason_for_return_field').val())
        else
            $('select#return_date_field').prop("disabled", true)
            $('select#reason_for_return_field').prop("disabled", true)
            hide_other_list()
            hide_parameter_list()
            hide_element_list()

    # При загрузке страницы 
    if $('select#return_field').val() == "Да"
        $('select#return_date_field').prop("disabled", false)
        $('select#reason_for_return_field').prop("disabled", false)
        current_return($('select#reason_for_return_field').val())
    else
        $('select#return_date_field').prop("disabled", true)
        $('select#reason_for_return_field').prop("disabled", true)
        hide_other_list()
        hide_parameter_list()
        hide_element_list()

    # При изменении поля Причина возврата
    $('select#reason_for_return_field').change ->
        current_return($(this).val())

