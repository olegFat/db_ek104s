# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on 'turbolinks:load', ->
    # Выбор даты
    $('.datepicker_month').datepicker
        language: 'ru',
        format: 'yyyy-mm'
        startView: "months"
        minViewMode: "months"

    $('.input-daterange input').each ->
        $(this).datepicker
            language: 'ru',
            format: 'yyyy-mm'
            startView: "months"
            minViewMode: "months"



