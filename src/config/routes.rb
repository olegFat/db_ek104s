Rails.application.routes.draw do

    # Анализ основных параметров резонатора
    resources :resonators_analyzes do

        get :chart_1_1, on: :collection
        get :chart_1_2, on: :collection
        get :chart_1_3, on: :collection
        get :chart_1_4, on: :collection
        get :chart_1_5, on: :collection

        get :chart_3_1, on: :collection
        get :chart_3_2, on: :collection
        get :chart_3_3, on: :collection
        get :chart_3_4, on: :collection
        get :chart_3_5, on: :collection

        get :chart_5_0, on: :collection
        get :chart_7_0, on: :collection
        get :chart_9_0, on: :collection
        get :chart_11_0, on: :collection

        get :chart_13_0, on: :collection
        get :chart_15_0, on: :collection
        get :chart_17_0, on: :collection
        get :chart_19_0, on: :collection
        get :chart_21_0, on: :collection
        get :chart_23_0, on: :collection
        get :chart_25_0, on: :collection
        get :chart_27_0, on: :collection

    end


    # Анализ брака и отказов
    resources :bad_analyzes do

        # Общая статистика по браку
        get :bad_total, on: :collection

        # Брак на посту ЭВО
        get :bad_evo, on: :collection

        # Брак после ЭВО
        get :bad_after_evo, on: :collection

        # Брак по стадиям производства
        get :bad_stage, on: :collection

        # Общая статистика по отказам
        get :return_total, on: :collection

        # Отказы на посту ЭВО
        get :return_evo, on: :collection

        # Отказы после ЭВО
        get :return_after_evo, on: :collection

        # Отказы по стадиям производства
        get :return_stage, on: :collection

    end

    resources :resonators do

        # Извлечение комплектующих
        get :pull_out_cathode, on: :collection
        get :pull_out_leg, on: :collection
        get :pull_out_fmirror, on: :collection
        get :pull_out_pmirror_1, on: :collection
        get :pull_out_pmirror_2, on: :collection
        get :pull_out_smirror, on: :collection

		get :set_alignment, on: :collection
		get :show_alignment, on: :collection

        get :set_evo, on: :collection
        get :show_evo, on: :collection

        get :set_thermostabilization, on: :collection
        get :show_thermostabilization, on: :collection

        get :set_cold_resistance, on: :collection
        get :show_cold_resistance, on: :collection

        get :set_running_in, on: :collection
        get :show_running_in, on: :collection

        get :set_sealing, on: :collection
        get :show_sealing, on: :collection

        get :set_heat_treatment_in_an_argon_chamber, on: :collection
        get :show_heat_treatment_in_an_argon_chamber, on: :collection

        get :set_varnish_coating, on: :collection
        get :show_varnish_coating, on: :collection

        get :set_metallization_of_mirrors, on: :collection
        get :show_metallization_of_mirrors, on: :collection

        get :set_tinning_mirrors, on: :collection
        get :show_tinning_mirrors, on: :collection

        get :set_test3, on: :collection
        get :show_test3, on: :collection

        get :set_test9, on: :collection
        get :show_test9, on: :collection

        get :set_test10, on: :collection
        get :show_test10, on: :collection

        get :set_control, on: :collection
        get :show_control, on: :collection

        get :search, on: :collection

        # Возврат из не годных
        get :roll_back_bad, on: :collection

    end

    resources :bad_resonators

    resources :sensors do

        # 1
        get :set_align_prisms_and_fpu, on: :collection
        get :show_align_prisms_and_fpu, on: :collection

        # 2
        get :set_checking_the_build_quality_in_screens, on: :collection
        get :show_checking_the_build_quality_in_screens, on: :collection

        # 3
        get :set_check_for_resistance_to_sinusoidal_vibration, on: :collection
        get :show_check_for_resistance_to_sinusoidal_vibration, on: :collection

        # 4
        get :set_check_by_pm, on: :collection
        get :show_check_by_pm, on: :collection

        # 5
        get :set_psi, on: :collection
        get :show_psi, on: :collection

        get :search, on: :collection

        get :ok_sensor_filter, on: :collection

        # Возврат из не годных
        get :roll_back_bad, on: :collection
    end

    resources :bad_sensors

    resources :mirrors do
        get :search, on: :collection
    end

    resources :legs do
        get :search, on: :collection
    end

    resources :cathodes do
        get :search, on: :collection
    end

    devise_for :users

    root "resonators#index"
end
