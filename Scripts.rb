#Resonator.first(1).each do |resonator|
#    _string = resonator.al_date.to_s
#    _year = _string[0..3]
#    _month = _string[5..6]
#    _day = _string[8..9]
#    _new_date = Date.new(_year, _month, _day)
#end


# Парсим таблицу resonators на наличие не годных резонаторов
# Делаем свободными все комплектующие вставленные в эти резонаторы
# Переносим все испорченные резонаторы в таблицу bad_resonators и удаляем их из
# таблицы resonators
Resonator.all.each do |resonator|

    if  resonator.al_result == 'Возврат' or
        resonator.ev_result == 'Возврат' or
        resonator.t3_result == 'Возврат' or
        resonator.t3_im_fit == 'Нет'     or
        resonator.ts_result == 'Возврат' or
        resonator.cr_result == 'Возврат' or
        resonator.cr_im_fit == 'Нет'     or
        resonator.ri_result == 'Возврат' or
        resonator.se_result == 'Возврат' or
        resonator.se_im_fit == 'Нет'     or
        resonator.ht_result == 'Возврат' or
        resonator.t9_result == 'Возврат' or
        resonator.t9_im_fit == 'Нет'     or
        resonator.cn_result == 'Возврат'

        # Вытаскиваю все из резонатора
        if resonator.cathode_id?
            cathode = Cathode.find(resonator.cathode_id)
            cathode.update(resonator_id: nil)
            cathode.update(date_of_installation_in_the_resonator: nil)
            resonator.update(cathode_id: nil)
        end
        if resonator.leg_id?
            leg = Leg.find(resonator.leg_id)
            leg.update(resonator_id: nil)
            leg.update(date_of_installation_in_the_resonator: nil)
            resonator.update(leg_id: nil)
        end
        if resonator.fmirror_id?
            fmirror = Mirror.find(resonator.fmirror_id)
            fmirror.update(resonator_id: nil)
            fmirror.update(date_of_installation_in_the_resonator: nil)
            resonator.update(fmirror_id: nil)
        end
        if resonator.pmirror_1_id?
            pmirror_1 = Mirror.find(resonator.pmirror_1_id)
            pmirror_1.update(resonator_id: nil)
            pmirror_1.update(date_of_installation_in_the_resonator: nil)
            resonator.update(pmirror_1_id: nil)
        end
        if resonator.pmirror_2_id
            pmirror_2 = Mirror.find(resonator.pmirror_2_id)
            pmirror_2.update(resonator_id: nil)
            pmirror_2.update(date_of_installation_in_the_resonator: nil)
            resonator.update(pmirror_2_id: nil)
        end
        if resonator.smirror_id
            smirror = Mirror.find(resonator.smirror_id)
            smirror.update(resonator_id: nil)
            smirror.update(date_of_installation_in_the_resonator: nil)
            resonator.update(smirror_id: nil)
        end

        # Создаю запись в таблице испорченных резонаторов
        bad_resonator = BadResonator.new(resonator.attributes)
        bad_resonator.save()

        # Удаляю текущую запись
        resonator.destroy
        resonator.save()

    end
end
render text: "Resonator count = #{Resonator.all.count}; BadResonator count = #{BadResonator.all.count};"


# Парсим таблицу resonators и bad_resonators
# Считаем количество законченных этапов каждого резонатора, результат записывает в :end_count
def work_e

    Resonator.all.each do |resonator|

        count_e = 0

        if resonator.alignment=="Завершена"
            count_e = count_e +  1
        end

        if resonator.evo=="Завершена"
            count_e = count_e +  1
        end

        if resonator.test_3=="Завершена"
            count_e = count_e +  1
        end

        if resonator.thermostabilization=="Завершена"
            count_e = count_e +  1
        end

        if resonator.cold_resistance=="Завершена"
            count_e = count_e +  1
        end

        if resonator.running_in=="Завершена"
            count_e = count_e +  1
        end

        if resonator.sealing=="Завершена"
            count_e = count_e +  1
        end

        if resonator.heat_treatment_in_an_argon_chamber=="Завершена"
            count_e = count_e +  1
        end

        if resonator.test_9=="Завершена"
            count_e = count_e +  1
        end

        if resonator.control=="Завершена"
            count_e = count_e +  1
        end

        if count_e
            resonator.update end_count: count_e
            resonator.save
        end

    end

    BadResonator.all.each do |resonator|

        count_e = 0

        if resonator.alignment=="Завершена"
            count_e = count_e +  1
        end

        if resonator.evo=="Завершена"
            count_e = count_e +  1
        end

        if resonator.test_3=="Завершена"
            count_e = count_e +  1
        end

        if resonator.thermostabilization=="Завершена"
            count_e = count_e +  1
        end

        if resonator.cold_resistance=="Завершена"
            count_e = count_e +  1
        end

        if resonator.running_in=="Завершена"
            count_e = count_e +  1
        end

        if resonator.sealing=="Завершена"
            count_e = count_e +  1
        end

        if resonator.heat_treatment_in_an_argon_chamber=="Завершена"
            count_e = count_e +  1
        end

        if resonator.test_9=="Завершена"
            count_e = count_e +  1
        end

        if resonator.test_9=="Завершена"
            count_e = count_e +  1
        end

        if resonator.control=="Завершена"
            count_e = count_e +  1
        end

        if count_e
            resonator.update end_count: count_e
            resonator.save
        end

        resonator.update end_count: count_e
        resonator.save

    end

    render text: 'Done'

end

# Парсим резонаторы на наличие не правильно указанных ножек
i = 0
Resonator.all.each do |resonator|
    if !Leg.find_by id: resonator.leg_id
        resonator.update leg_id: nil
        resonator.save
        i = i.next
    end
end
puts "Исправлена информаци в Done #{i} резонаторах."


# Для всех посчитать изменение напряжения горения в 5.Испытания климатические
Resonator.all.each do |r|

    # Изменение напряжения горения, ΔU гор
    cr_combustion_voltage_delta = r.cr_burning_voltage_after_10_min.to_f - r.cr_burning_voltage_after_2_sec.to_f
    r.update(cr_combustion_voltage_delta: cr_combustion_voltage_delta)

    # Испытания климатические. Среднее по модам захвата , Σ(Захват на моде N)/4
    cr_capture_in_fashion_delta = (
    r.cr_capture_in_fashion_1.to_f +
    r.cr_capture_on_fashion_2.to_f +
    r.cr_capture_in_fashion_3.to_f +
    r.cr_capture_on_fashion_4.to_f ) / 4
    r.update(cr_capture_in_fashion_delta: cr_capture_in_fashion_delta)

    # Испытания климатические. Средняя частота выходного сигнала на модах
    cr_output_frequency_delta = (
    r.cr_output_frequency_in_mode_1.to_f +
    r.cr_output_frequency_in_mode_2.to_f +
    r.cr_output_frequency_in_mode_3.to_f +
    r.cr_output_frequency_in_mode_4.to_f ) / 4
    r.update(cr_output_frequency_delta: cr_output_frequency_delta)

    # Приработка. Среднее по модам захвата , Σ(Захват на моде N)/4
    ri_capture_in_fashion_delta = (
    r.ri_capture_in_fashion_1.to_f +
    r.ri_capture_on_fashion_2.to_f +
    r.ri_capture_in_fashion_3.to_f +
    r.ri_capture_on_fashion_4.to_f ) / 4
    r.update(ri_capture_in_fashion_delta: ri_capture_in_fashion_delta)

    # Приработка. Средняя частота выходного сигнала на модах
    ri_output_frequency_delta = (
    r.ri_output_frequency_in_mode_1.to_f +
    r.ri_output_frequency_in_mode_2.to_f +
    r.ri_output_frequency_in_mode_3.to_f +
    r.ri_output_frequency_in_mode_4.to_f ) / 4
    r.update(ri_output_frequency_delta: ri_output_frequency_delta)

end

render text: 'Done'

# Считаем количество выполненных операций в Sensor.all,
# результат записываем в :end_count
Sensor.all.each do |sensor|
    count = 0
    if sensor.align_prisms_and_fpu == 'Завершена'
        count = count + 1
    end
    if sensor.checking_the_build_quality_in_screens == 'Завершена'
        count = count + 1
    end
    if sensor.check_for_resistance_to_sinusoidal_vibration == 'Завершена'
        count = count + 1
    end
    if sensor.check_by_pm == 'Завершена'
        count = count + 1
    end
    if sensor.psi == 'Завершена'
        count = count + 1
    end
    sensor.update end_count: count

    if count >= 5
        sensor.update suitable: true
    else
        sensor.update suitable: false
    end

    puts "#{sensor.number}_#{count}"
end
puts 'done'


Sensor.all.each do |sensor|
    if  sensor.ap_result == 'Возврат' or
        sensor.cb_result == 'Возврат' or
        sensor.cf_result == 'Возврат' or
        sensor.cp_result == 'Возврат' or
        sensor.ps_result == 'Возврат'
end


